import { Injectable } from '@angular/core';
import { Http, Headers  } from '@angular/http';
import { BehaviorSubject } from 'rxjs';
import 'rxjs/add/operator/map';

//let apiUrl    = 'http://www.omedia.web.id/api-haldin-agent/api/';
// let apiUrl    = 'http://localhost/api-haldin-agent/api/';
// let apiUrl    = 'https://www.haldinfoods.com/api-haldin-agent/api/';
let apiUrl    = 'https://apps.haldinfoods.com/api-haldin-agent/api/';

@Injectable()
export class AuthService {
	
  private cart = [];
  private cartItemCount = new BehaviorSubject(0);
  
  id: number;
  name: string;
  price: number;
  amount: number;

  constructor(public http: Http) {}

   login(data) {
     return new Promise((resolve, reject) => {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
		
		if(data.username == "" && data.password == "") {
          reject("Username/Password harus diisi");
        } 
        else { 
			this.http.post(apiUrl+'login', JSON.stringify(data), {headers: headers})
			  .subscribe(res => {
				resolve(res.json());
				//console.log(res.json());
			  }, (err) => {
				reject(err);
			});
		}  
    })
  } 
  
  forgotPassword(data) {
     return new Promise((resolve, reject) => {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
		
		if(data.email == "") {
          reject("Email harus diisi");
        } 
        else { 
			this.http.post(apiUrl+'forgot_password', JSON.stringify(data), {headers: headers})
			  .subscribe(res => {
				resolve(res.json());
				// console.log(res.json());
			  }, (err) => {
				reject(err);
			});
			
		}  
    })
  } 
  
    ubah_password(data) {
    return new Promise((resolve, reject) => {
        let headers = new Headers();
		//let re = '/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/';
        headers.append('Content-Type', 'application/json');
		
		if(data.password == ""){
			reject("password harus diisi");
		}
		
		else if (data.password2 == "") {
            reject("Konfirmasi Password harus diisi");
        }
		
		else if (data.password != data.password2) {
            reject("Konfirmasi Password Tidak Sesuai");
        }
		
		else {
			this.http.post(apiUrl+'ubah_password_user', JSON.stringify(data), {headers: headers})
			  .subscribe(res => {
				resolve(res.json());
				//console.log(res.json());
			}, (err) => {
				reject(err);
				//console.log(err);
			})
		}
    });
  }

  register(data) {
    return new Promise((resolve, reject) => {
        let headers = new Headers();
		//let re = '/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/';
        headers.append('Content-Type', 'application/json');
		
		if(data.gelar == ""){
			reject("Gelar harus diisi");
		}
		
		else if (data.nama == ""){  
			reject("Nama harus diisi");
        }
		
		else if(data.jenis_kelamin == ""){
			reject("Jenis kelamin harus diisi");
		}

        else if(data.email == ""){
			reject("Email harus diisi");
		}
		
		else if(data.telepon == ""){
			reject("Telepon harus diisi");
		}
		
		else if(data.nik == ""){
			reject("Nik harus diisi");
		}
		
		// else if(data.imageOriNpwp == ""){
		// 	reject("Foto NPWP harus diupload");
		// }
		
		else if(data.npwp == ""){
			reject("NPWP harus diisi");
		}
		
		else if(data.alamat == ""){
			reject("Alamat harus diisi");
		}
		
		else if(data.nama_bank == ""){
			reject("Nama bank harus diisi");
		}
		
		else if(data.kantor_cabang == ""){
			reject("Kantor cabang harus diisi");
		}
		
		else if(data.nomor_rekening == ""){
			reject("Kantor cabang harus diisi");
		}
		
		else if(data.nama_pemilik == ""){
			reject("Nama pemilik harus diisi");
		}
		
		// else if(data.imageOriRek == ""){
		// 	reject("Foto Buku Rekening Harus Disi");
		// }
		
		else if(data.kode_agent == ""){
			reject("Kode agent harus diisi");
		}
		
		else if(data.kode_sales == ""){
			reject("Kode Sales harus diisi");
		}
		
		else if(data.provinsiSelected == ""){
			reject("Provinsi harus diisi");
		}
		
		else if(data.citySelected == ""){
			reject("Kota harus diisi");
		}
		
		else if(data.kecamatanSelected == ""){
			reject("Kecamatan harus diisi");
		}
		
		else if(data.kelurahanSelected == ""){
			reject("Kelurahan harus diisi");
		}
		
		else if(data.kode_pos == ""){
			reject("Kode pos harus diisi");
		}
		
		else if (data.nama_pemilik != data.nama) {
            reject("Nama pemilik rekening harus sama dengan nama lengkap");
        }
		
		else if(data.password == ""){
			reject("password harus diisi");
		}
		
		else if (data.password != data.password2) {
            reject("Konfirmasi Password Tidak Sesuai");
        }
		
		else {
			this.http.post(apiUrl+'register_user', JSON.stringify(data), {headers: headers})
			  .subscribe(res => {
				resolve(res.json());
				//console.log(res.json());
			}, (err) => {
				reject(err);
				//console.log(err);
			})
		}
    });
  }
  
  register_survey(data) {
    return new Promise((resolve, reject) => {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
		
		if (data.kategoriSelected != ""){			
			if(data.kategoriSelected == 1){
				if (data.gelar == "") {
				  reject("Gelar harus diisi");
				} 
				else if(data.jenis_kelamin == ""){
					reject("Jenis kelamin harus diisi");
				}
				else if (data.nama == "") {
				  reject("Nama pemilik harus diisi");
				} 
				/*else if (data.email == "") {
				  reject("Email harus diisi");
				}*/ 
				else if (data.telepon == "") {
				  reject("No Telepon harus diisi");
				} 
				/*else if (data.nik == "") {
				  reject("NIK harus diisi");
				}*/ 
				else if(data.alamat_outlet == ""){
					reject("Alamat harus diisi");
				}
				else if(data.provinsiSelected == ""){
					reject("Provinsi harus diisi");
				}
				else if(data.citySelected == ""){
					reject("Kota harus diisi");
				}
				else if(data.kode_pos == ""){
					reject("Kode Pos harus diisi");
				}
				else if(data.password == ""){
					reject("password harus diisi");
				}
				else if (data.password != data.password2) {
					reject("Konfirmasi Password Tidak Sesuai");
				}
				 else {
					this.http.post(apiUrl+'register_survey', JSON.stringify(data), {headers: headers})
					  .subscribe(res => {
						resolve(res.json());
						//console.log(res.json());
					}, (err) => {
						reject(err);
						//console.log(err);
					})
				}
			} 
			
			if(data.kategoriSelected == 2){
				if (data.nama_outlet == "") {
				  reject("Nama outlet harus diisi");
				} 
				else if (data.gelar == "") {
				  reject("Gelar harus diisi");
				} 
				else if(data.jenis_kelamin == ""){
					reject("Jenis kelamin harus diisi");
				}
				else if (data.nama == "") {
				  reject("Nama pemilik harus diisi");
				} 
				else if (data.email == "") {
				  reject("Email harus diisi");
				} 
				else if (data.telepon == "") {
				  reject("No Telepon harus diisi");
				} 
				else if (data.nik == "") {
				  reject("NIK harus diisi");
				} 
				else if (data.npwp == "") {
				  reject("NPWP harus diisi");
				} 
				else if (data.nib == "") {
				  reject("NIB harus diisi");
				}
				else if(data.alamat_outlet == ""){
					reject("Alamat harus diisi");
				}
				else if(data.provinsiSelected == ""){
					reject("Provinsi harus diisi");
				}
				else if(data.citySelected == ""){
					reject("Kota harus diisi");
				}
				else if(data.kode_pos == ""){
					reject("Kode Pos harus diisi");
				}
				else if(data.password == ""){
					reject("password harus diisi");
				}
				else if (data.password != data.password2) {
					reject("Konfirmasi Password Tidak Sesuai");
				}
				else {
					this.http.post(apiUrl+'register_survey', JSON.stringify(data), {headers: headers})
					  .subscribe(res => {
						resolve(res.json());
						//console.log(res.json());
					}, (err) => {
						reject(err);
						//console.log(err);
					})
				}
			}
			
			if(data.kategoriSelected == 3){
				if (data.gelar == "") {
				  reject("Gelar harus diisi");
				} 
				else if (data.nomor_induk_kr == "") {
				  reject("Nomor Induk Karyawan harus diisi");
				} 
				else if(data.jenis_kelamin == ""){
					reject("Jenis kelamin harus diisi");
				}
				else if (data.nama == "") {
				  reject("Nama pemilik harus diisi");
				} 
				else if (data.email == "") {
				  reject("Email harus diisi");
				} 
				else if (data.telepon == "") {
				  reject("No Telepon harus diisi");
				} 
				else if (data.nik == "") {
				  reject("NIK harus diisi");
				} 
				else if (data.npwp == "") {
				  reject("NPWP harus diisi");
				} 
				else if(data.alamat_outlet == ""){
					reject("Alamat harus diisi");
				}
				else if(data.provinsiSelected == ""){
					reject("Provinsi harus diisi");
				}
				else if(data.citySelected == ""){
					reject("Kota harus diisi");
				}
				else if(data.kode_pos == ""){
					reject("Kode Pos harus diisi");
				}
				else if(data.password == ""){
					reject("password harus diisi");
				}
				else if (data.password != data.password2) {
					reject("Konfirmasi Password Tidak Sesuai");
				}
				else {
					this.http.post(apiUrl+'register_survey', JSON.stringify(data), {headers: headers})
					  .subscribe(res => {
						resolve(res.json());
						//console.log(res.json());
					}, (err) => {
						reject(err);
						//console.log(err);
					})
				}
			}
		} 
		else {
			reject("Silahkan pilih kategori");
		}
		
		
    });
  }

  logout(){
    return new Promise((resolve, reject) => {
        let headers = new Headers();
        headers.append('X-Auth-Token', localStorage.getItem('token'));

        this.http.post(apiUrl+'logout', {}, {headers: headers})
          .subscribe(res => {
            localStorage.clear();
          }, (err) => {
            reject(err);
          });
    });
  }
  
  aktivasi(data) {
    return new Promise((resolve, reject) => {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
		
		if (data.kode == "") {
          reject("Field harus diisi");
        } 
		else {
			this.http.post(apiUrl+'aktivasi_user', JSON.stringify(data), {headers: headers})
			  .subscribe(res => {
				resolve(res.json());
				//console.log(res.json());
			}, (err) => {
				reject(err);
				//console.log(err);
			})
		}
    });
  }
  
  getDashDetailTransaksi(todo){
	   return new Promise((resolve, reject) => {			
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', localStorage.getItem('token'));
		
        if (todo.order_id == "") {
			reject("Id Belum Dipilih");
        } 
		else{
		  
		  this.http.get(apiUrl+'get_dashboard_detail_transaksi?order_id='+todo.order_id, {headers: headers})
          .subscribe(res => {
			  resolve(res.json());
			  //console.log(todo.userId);
			}, (err) => {
			  //reject("Data Tidak Ditemukan");
			  reject(err);
		  });
        } 
    })
  }
  
  getDashTotalTransaksiPending(todo){
	  return new Promise((resolve, reject) => {			
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', localStorage.getItem('token'));
		
        if (todo.userId == "") {
			reject("Id Belum Dipilih");
        } 
		else{
		  
		  this.http.get(apiUrl+'get_dashboard_total_transaksi_pending?agent_id='+todo.userId, {headers: headers})
          .subscribe(res => {
			  resolve(res.json());
			  //console.log(todo.userId);
			}, (err) => {
			  //reject("Data Tidak Ditemukan");
			  reject(err);
		  });
        } 
    })
  }
  
  getLoadProfileAdsress(todo){
	   return new Promise((resolve, reject) => {			
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', localStorage.getItem('token'));
		
		let providUser = localStorage.getItem('provider_user');;
				
        if (todo.userId == "") {
			reject("Id Belum Dipilih");
        } 
		else{
		  
		  this.http.get(apiUrl+'get_load_profile_address?agent_id='+todo.userId+'&provider_user='+providUser, {headers: headers})
          .subscribe(res => {
			  resolve(res.json());
			  //console.log(todo.userId);
			}, (err) => {
			  //reject("Data Tidak Ditemukan");
			  reject(err);
		  });
        } 
    }) 
  }
  
  getLoadProfile(todo){
	  return new Promise((resolve, reject) => {			
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', localStorage.getItem('token'));
		
        if (todo.userId == "") {
			reject("Id Belum Dipilih");
        } 
		else{
		  
		  this.http.get(apiUrl+'get_load_profile?agent_id='+todo.userId, {headers: headers})
          .subscribe(res => {
			  resolve(res.json());
			  //console.log(todo.userId);
			}, (err) => {
			  //reject("Data Tidak Ditemukan");
			  reject(err);
		  });
        } 
    }) 
  }
  
  getDashTotalTransaksiComplete(todo){
	  return new Promise((resolve, reject) => {			
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', localStorage.getItem('token'));
		
        if (todo.userId == "") {
			reject("Id Belum Dipilih");
        } 
		else{
		  
		  this.http.get(apiUrl+'get_dashboard_total_transaksi_complete?agent_id='+todo.userId, {headers: headers})
          .subscribe(res => {
			  resolve(res.json());
			  //console.log(todo.userId);
			}, (err) => {
			  //reject("Data Tidak Ditemukan");
			  reject(err);
		  });
        } 
    }) 
  }
  
  getDashTotalTransaksiKirim(todo){
	    return new Promise((resolve, reject) => {			
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', localStorage.getItem('token'));
		
        if (todo.userId == "") {
			reject("Id Belum Dipilih");
        } 
		else{
		  
		  this.http.get(apiUrl+'get_dashboard_total_transaksi_receive?agent_id='+todo.userId, {headers: headers})
          .subscribe(res => {
			  resolve(res.json());
			  //console.log(todo.userId);
			}, (err) => {
			  //reject("Data Tidak Ditemukan");
			  reject(err);
		  });
        } 
    })
  }
  
  getDashTotalTransaksiSent(todo){
	  return new Promise((resolve, reject) => {			
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', localStorage.getItem('token'));
		
        if (todo.userId == "") {
			reject("Id Belum Dipilih");
        } 
		else{
		  
		  this.http.get(apiUrl+'get_dashboard_total_transaksi_sent?agent_id='+todo.userId, {headers: headers})
          .subscribe(res => {
			  resolve(res.json());
			  //console.log(todo.userId);
			}, (err) => {
			  //reject("Data Tidak Ditemukan");
			  reject(err);
		  });
        } 
    })
  }
  
  getDataStatusByTipe(todo){
	  return new Promise((resolve, reject) => {			
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', localStorage.getItem('token'));
		
        if (todo.userId == "") {
			reject("Id Belum Dipilih");
        } 
		else{
		  
		  this.http.get(apiUrl+'get_data_transaksi_by_tipe?agent_id='+todo.userId+'&tipe_status='+todo.tipe_status, {headers: headers})
          .subscribe(res => {
			  resolve(res.json());
			  //console.log(todo.userId);
			}, (err) => {
			  //reject("Data Tidak Ditemukan");
			  reject(err);
		  });
        } 
    })
  }
  
  getDashTransaksi(todo){
	   return new Promise((resolve, reject) => {			
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', localStorage.getItem('token'));
		
        if (todo.userId == "") {
			reject("Id Belum Dipilih");
        } 
		else{
		  
		  this.http.get(apiUrl+'get_dashboard_transaksi?agent_id='+todo.userId, {headers: headers})
          .subscribe(res => {
			  resolve(res.json());
			  //console.log(todo.userId);
			}, (err) => {
			  //reject("Data Tidak Ditemukan");
			  reject(err);
		  });
        } 
    })
  }

  getCostShipping(todo){
	return new Promise((resolve, reject) => {			
	  let headers = new Headers();
	  headers.append('Content-Type', 'application/json');
	  headers.append('Authorization', localStorage.getItem('token'));
	  
	  if (todo.kurirId == "") {
		  reject("Loading cost....");
	  } 
	  else if(todo.originset === undefined){
		  reject("Silahkan pilih stock point pengiriman product");
	  }
	  else{
		
		this.http.get(apiUrl+'getCost?kurirId='+todo.kurirId+'&cityId='+todo.cityId+'&totalWeight='+todo.totalWeight+'&originset='+todo.originset+'&provider_user='+todo.providerUser, {headers: headers})
		.subscribe(res => {
			resolve(res.json());
			//console.log(todo.surveyId);
		  }, (err) => {
			//reject("Data Tidak Ditemukan");
			reject(err);
		});
	  } 
  })
}
  
  getListPerforma(todo){
	  return new Promise((resolve, reject) => {			
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', localStorage.getItem('token'));
		
        if (todo.userId == "") {
			reject("Id Belum Dipilih");
        } else {
		  this.http.get(apiUrl+'get_list_performa?agent_id='+todo.userId+'&provider_user='+todo.providerUser, {headers: headers})
          .subscribe(res => {
			  resolve(res.json());
			  //console.log(todo.userId);
			}, (err) => {
			  //reject("Data Tidak Ditemukan");
			  reject(err);
		  });
        } 
    })
  }
  
  getKomisiAgent(params){
	  return new Promise((resolve, reject) => {			
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', localStorage.getItem('token'));
		
        if (params.kode_agent_set == "") {
			reject("Id Belum Dipilih");
        } 
		else{
		  
		  this.http.get(apiUrl+'get_list_komisi?kode_agent='+params.kode_agent_set+'&tahun='+params.tahun_set+'&bulan='+params.bulan_set, {headers: headers})
          .subscribe(res => {
			  resolve(res.json());
			  //console.log(todo.userId);
			}, (err) => {
			  //reject("Data Tidak Ditemukan");
			  reject(err);
		  });
        } 
    })
  }
  
    getBankAgent(params){
	  return new Promise((resolve, reject) => {			
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', localStorage.getItem('token'));
		
        if (params.agent_id == "") {
			reject("Id Belum Dipilih");
        } 
		else{
		  
		  this.http.get(apiUrl+'get_bank_agent?agent_id='+params.agent_id, {headers: headers})
          .subscribe(res => {
			  resolve(res.json());
			  //console.log(todo.userId);
			}, (err) => {
			  //reject("Data Tidak Ditemukan");
			  reject(err);
		  });
        } 
    })
  }
  
  getDetailKomisiAgent(params){
	  return new Promise((resolve, reject) => {			
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', localStorage.getItem('token'));
		
        if (params.kode_agent_set == "") {
			reject("Id Belum Dipilih");
        } 
		else{
		  
		  this.http.get(apiUrl+'get_list_detail_komisi?kode_agent='+params.kode_agent_set+'&tgl='+params.tgl_set, {headers: headers})
          .subscribe(res => {
			  resolve(res.json());
			  //console.log(todo.userId);
			}, (err) => {
			  //reject("Data Tidak Ditemukan");
			  reject(err);
		  });
        } 
    })
  }
  
  getListSurvey(todo){
	  return new Promise((resolve, reject) => {			
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', localStorage.getItem('token'));
		
        if (todo.userId == "") {
			reject("Id Belum Dipilih");
        } 
		else{
		  
		  this.http.get(apiUrl+'get_list_survey?agent_id='+todo.userId, {headers: headers})
          .subscribe(res => {
			  resolve(res.json());
			  //console.log(todo.userId);
			}, (err) => {
			  //reject("Data Tidak Ditemukan");
			  reject(err);
		  });
        } 
    })
  }
  
  getDetailPerforma(todo){
	  return new Promise((resolve, reject) => {			
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', localStorage.getItem('token'));
		
        if (todo.detailId == "") {
			reject("Id Belum Dipilih");
        } 
		else{
		  
		  this.http.get(apiUrl+'get_detail_performa?detail_id='+todo.detailId, {headers: headers})
          .subscribe(res => {
			  resolve(res.json());
			  //console.log(todo.surveyId);
			}, (err) => {
			  //reject("Data Tidak Ditemukan");
			  reject(err);
		  });
        } 
    })
  } 
  
  getDetailLead(todo){
	  return new Promise((resolve, reject) => {			
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', localStorage.getItem('token'));
		
        if (todo.leadId == "") {
			reject("Id Belum Dipilih");
        } 
		else{
		  
		  this.http.get(apiUrl+'get_detail_lead?lead_id='+todo.leadId, {headers: headers})
          .subscribe(res => {
			  resolve(res.json());
			  //console.log(todo.surveyId);
			}, (err) => {
			  //reject("Data Tidak Ditemukan");
			  reject(err);
		  });
        } 
    })
  } 
  
  getDetailAgenda(todo){
	  return new Promise((resolve, reject) => {			
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', localStorage.getItem('token'));
		
        if (todo.agendaId == "") {
			reject("Id Belum Dipilih");
        } 
		else{
		  
		  this.http.get(apiUrl+'get_detail_agenda?agenda_id='+todo.agendaId, {headers: headers})
          .subscribe(res => {
			  resolve(res.json());
			  //console.log(todo.surveyId);
			}, (err) => {
			  //reject("Data Tidak Ditemukan");
			  reject(err);
		  });
        } 
    })
  } 
  
  getDetailSurvey(todo){
	  return new Promise((resolve, reject) => {			
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', localStorage.getItem('token'));
		
        if (todo.surveyId == "") {
			reject("Id Belum Dipilih");
        } 
		else{
		  
		  this.http.get(apiUrl+'get_detail_survey?survey_id='+todo.surveyId, {headers: headers})
          .subscribe(res => {
			  resolve(res.json());
			  //console.log(todo.surveyId);
			}, (err) => {
			  //reject("Data Tidak Ditemukan");
			  reject(err);
		  });
        } 
    })
  } 
  
  getListLead(todo){
	  return new Promise((resolve, reject) => {			
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', localStorage.getItem('token'));
		
        if (todo.userId == "") {
			reject("Id Belum Dipilih");
        } 
		else{
		  
		  this.http.get(apiUrl+'get_list_lead?agent_id='+todo.userId, {headers: headers})
          .subscribe(res => {
			  resolve(res.json());
			  //console.log(todo.userId);
			}, (err) => {
			  //reject("Data Tidak Ditemukan");
			  reject(err);
		  });
        } 
    })
  }
  
  getListAgendaDefault(todo){
	  return new Promise((resolve, reject) => {			
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', localStorage.getItem('token'));
		
        if (todo.userId == "") {
			reject("Id Belum Dipilih");
        } 
		else{
		  
		  this.http.get(apiUrl+'get_list_agenda_default?tgl='+todo.tgl_agenda+'&id='+todo.userId, {headers: headers})
          .subscribe(res => {
			  resolve(res.json());
			}, (err) => {
			  reject(err);
		  });
        }  
    })
  }
  
  getListAgenda(todo){
	  return new Promise((resolve, reject) => {			
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', localStorage.getItem('token'));
		
        if (todo.userId == "") {
			reject("Id Belum Dipilih");
        } 
		else{
		  
		  this.http.get(apiUrl+'get_list_agenda?tgl='+todo.tgl_agenda+'&id='+todo.userId, {headers: headers})
          .subscribe(res => {
			  resolve(res.json());
			}, (err) => {
			  reject(err);
		  });
        }  
    })
  }
  
  getListProduct(todo){
	  return new Promise((resolve, reject) => {			
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', localStorage.getItem('token'));
		  
		this.http.get(apiUrl+'get_list_product?price_user='+todo.price_type_user, {headers: headers})
        .subscribe(res => {
			  resolve(res.json());
			//   console.log(res);
		   }, (err) => {
			  //reject("Data Tidak Ditemukan");
			  reject(err);
		});
        
    })
  }
  
  getListProductSample(todo){
	  return new Promise((resolve, reject) => {			
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', localStorage.getItem('token'));
		  
		this.http.get(apiUrl+'get_list_product_sample', {headers: headers})
        .subscribe(res => {
			  resolve(res.json());
			  //console.log(todo.userId);
		   }, (err) => {
			  //reject("Data Tidak Ditemukan");
			  reject(err);
		});
        
    })
  }
  
  getDetailProduct(todo){
	  return new Promise((resolve, reject) => {			
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', localStorage.getItem('token'));
		
        if (todo.productId == "") {
			reject("Id Belum Dipilih");
        } 
		else{
		  
		  this.http.get(apiUrl+'get_detail_product?product_id='+todo.productId, {headers: headers})
          .subscribe(res => {
			  resolve(res.json());
			  //console.log(todo.surveyId);
			}, (err) => {
			  //reject("Data Tidak Ditemukan");
			  reject(err);
		  });
        } 
    })
  } 
  
   getListApproval(todo){
	  return new Promise((resolve, reject) => {			
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', localStorage.getItem('token'));
		  
		this.http.get(apiUrl+'get_list_approval?user_id='+todo.userId, {headers: headers})
        .subscribe(res => {
			  resolve(res.json());
			  //console.log(todo.userId);
		   }, (err) => {
			  //reject("Data Tidak Ditemukan");
			  reject(err);
		});
        
    })
  }
  
  getListAktifasiUser(todo){
	  return new Promise((resolve, reject) => {			
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', localStorage.getItem('token'));
		  
		this.http.get(apiUrl+'get_list_aktifasi_customer?user_id='+todo.userId, {headers: headers})
        .subscribe(res => {
			  resolve(res.json());
			  //console.log(todo.userId);
		   }, (err) => {
			  //reject("Data Tidak Ditemukan");
			  reject(err);
		});
        
    })
  }
  
  getAktifasiUserCode(code){
	   return new Promise((resolve, reject) => {			
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', localStorage.getItem('token'));
		  
		this.http.get(apiUrl+'get_aktifasi_customer_code?user_code='+code, {headers: headers})
        .subscribe(res => {
			  resolve(res.json());
			  //console.log(todo.userId);
		   }, (err) => {
			  //reject("Data Tidak Ditemukan");
			  reject(err);
		});
        
    })
  }
  
  getTotalApproval(todo){
	  return new Promise((resolve, reject) => {			
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', localStorage.getItem('token'));
		  
		  this.http.get(apiUrl+'getTotalApproval?user_id='+todo.userId, {headers: headers})
          .subscribe(res => {
			  resolve(res.json());
			  //console.log(todo.cutomer_id);
			}, (err) => {
			  //reject("Data Tidak Ditemukan");
			  reject(err);
		  });
         
    })
  }
  
   getDetailApproval(todo){
	  return new Promise((resolve, reject) => {			
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', localStorage.getItem('token'));
		
        if (todo.approval_id == "") {
			reject("Id Belum Dipilih");
        } 
		else{
		  
		  this.http.get(apiUrl+'get_detail_approval?approval_id='+todo.approval_id, {headers: headers})
          .subscribe(res => {
			  resolve(res.json());
			  //console.log(todo.surveyId);
			}, (err) => {
			  //reject("Data Tidak Ditemukan");
			  reject(err);
		  });
        } 
    })
  } 
  
  approvedHsp(id) {
     return new Promise((resolve, reject) => {			
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', localStorage.getItem('token'));
		  
		  this.http.get(apiUrl+'approvedhsp?app_id='+id, {headers: headers})
          .subscribe(res => {
			  resolve(res.json());
			  //console.log(todo.cutomer_id);
			}, (err) => {
			  //reject("Data Tidak Ditemukan");
			  reject(err);
		  });
         
    })
  }
  
  rejectedHsp(id) {
     return new Promise((resolve, reject) => {			
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', localStorage.getItem('token'));
		  
		  this.http.get(apiUrl+'rejectedhsp?app_id='+id, {headers: headers})
          .subscribe(res => {
			  resolve(res.json());
			  //console.log(todo.cutomer_id);
			}, (err) => {
			  //reject("Data Tidak Ditemukan");
			  reject(err);
		  });
         
    })
  }
  
  getCustomer(){
	  return new Promise((resolve, reject) => {			
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', localStorage.getItem('token'));
		let id_agent  = localStorage.getItem('agent_id');
		let prov_user = localStorage.getItem('provider_user');
		let kode_agent = localStorage.getItem('kode_agent');
		let kode_sales = localStorage.getItem('kode_sales');
		  
		  this.http.get(apiUrl+'getCustomerAll?agent_id='+id_agent+'&provider_user='+prov_user+'&kode_agent='+kode_agent+'&kode_sales='+kode_sales, {headers: headers})
          .subscribe(res => {
			  resolve(res.json());
			 //console.log(res.json());
			}, (err) => {
			  //reject("Data Tidak Ditemukan");
			  reject(err);
		  });
         
    })
  } 
  
  getCustomerAddress(){
	  return new Promise((resolve, reject) => {			
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', localStorage.getItem('token'));
		let id_agent  = localStorage.getItem('agent_id');
		  
		  this.http.get(apiUrl+'getCustomerAddress?agent_id='+id_agent, {headers: headers})
          .subscribe(res => {
			  resolve(res.json());
			 //console.log(res.json());
			}, (err) => {
			  //reject("Data Tidak Ditemukan");
			  reject(err);
		  });
         
    })
  } 
  
  getPhoneNumber(todo){
	  return new Promise((resolve, reject) => {			
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', localStorage.getItem('token'));
		  
		  this.http.get(apiUrl+'getPhoneNumber?customer_id='+todo.customer_id, {headers: headers})
          .subscribe(res => {
			  resolve(res.json());
			  //console.log(todo.cutomer_id);
			}, (err) => {
			  //reject("Data Tidak Ditemukan");
			  reject(err);
		  });
         
    })
  }

  getDefaultAddress(todo){
	return new Promise((resolve, reject) => {			
	  let headers = new Headers();
	  headers.append('Content-Type', 'application/json');
	  headers.append('Authorization', localStorage.getItem('token'));
		
		this.http.get(apiUrl+'getCusDefault?customer_id='+todo.customer_id, {headers: headers})
		.subscribe(res => {
			resolve(res.json());
			//console.log(todo.cutomer_id);
		  }, (err) => {
			//reject("Data Tidak Ditemukan");
			reject(err);
		});
	   
  })
}
  
  getTotalSurvey(){
	  return new Promise((resolve, reject) => {			
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', localStorage.getItem('token'));
		let id_agent  = localStorage.getItem('agent_id');
		  
		  this.http.get(apiUrl+'getTotalSurvey?id_agent='+id_agent, {headers: headers})
          .subscribe(res => {
			  resolve(res.json());
			  //console.log(todo.cutomer_id);
			}, (err) => {
			  //reject("Data Tidak Ditemukan");
			  reject(err);
		  });
         
    })
  }
  
   getTotalLead(){
	  return new Promise((resolve, reject) => {			
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', localStorage.getItem('token'));
		let id_agent  = localStorage.getItem('agent_id');
		  
		  this.http.get(apiUrl+'getTotalLead?id_agent='+id_agent, {headers: headers})
          .subscribe(res => {
			  resolve(res.json());
			  //console.log(todo.cutomer_id);
			}, (err) => {
			  //reject("Data Tidak Ditemukan");
			  reject(err);
		  });
         
    })
  }
  
   getTotalTransaksi(){
	  return new Promise((resolve, reject) => {			
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', localStorage.getItem('token'));
		let id_agent  = localStorage.getItem('agent_id');
		  
		  this.http.get(apiUrl+'getTotalTransaksi?id_agent='+id_agent, {headers: headers})
          .subscribe(res => {
			  resolve(res.json());
			  //console.log(todo.cutomer_id);
			}, (err) => {
			  //reject("Data Tidak Ditemukan");
			  reject(err);
		  });
         
    })
  }

  getTotalAgendaOpen(){
	return new Promise((resolve, reject) => {			
	  let headers = new Headers();
	  headers.append('Content-Type', 'application/json');
	  headers.append('Authorization', localStorage.getItem('token'));
	  let id_agent  = localStorage.getItem('agent_id');
		
		this.http.get(apiUrl+'getTotalAgendaOpen?id_agent='+id_agent, {headers: headers})
		.subscribe(res => {
			resolve(res.json());
			//console.log(todo.cutomer_id);
		  }, (err) => {
			//reject("Data Tidak Ditemukan");
			reject(err);
		});
	   
  })
}

getTotalLeadOpen(){
	return new Promise((resolve, reject) => {			
	  let headers = new Headers();
	  headers.append('Content-Type', 'application/json');
	  headers.append('Authorization', localStorage.getItem('token'));
	  let id_agent  = localStorage.getItem('agent_id');
		
		this.http.get(apiUrl+'getTotalLeadOpen?id_agent='+id_agent, {headers: headers})
		.subscribe(res => {
			resolve(res.json());
			//console.log(todo.cutomer_id);
		  }, (err) => {
			//reject("Data Tidak Ditemukan");
			reject(err);
		});
	   
  })
}
  
  getTotalKunjungan(){
	   return new Promise((resolve, reject) => {			
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', localStorage.getItem('token'));
		let id_agent  = localStorage.getItem('agent_id');
		  
		  this.http.get(apiUrl+'getTotalKujungan?id_agent='+id_agent, {headers: headers})
          .subscribe(res => {
			  resolve(res.json());
			  //console.log(todo.cutomer_id);
			}, (err) => {
			  //reject("Data Tidak Ditemukan");
			  reject(err);
		  });
         
    })
  }
  
  getOriginPoint(){
	  return new Promise((resolve, reject) => {			
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', localStorage.getItem('token'));
		  
		  this.http.get(apiUrl+'getOrigin', {headers: headers})
          .subscribe(res => {
			  resolve(res.json());
			 //console.log(todo.cutomer_id);
			}, (err) => {
			  //reject("Data Tidak Ditemukan");
			  reject(err);
		  });
         
    })
  } 
  
   getProvince(){
	  return new Promise((resolve, reject) => {			
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', localStorage.getItem('token'));
		  
		  this.http.get(apiUrl+'getProvince', {headers: headers})
          .subscribe(res => {
			  resolve(res.json());
			//  console.log(todo.cutomer_id);
			}, (err) => {
			  //reject("Data Tidak Ditemukan");
			  reject(err);
		  });
         
    })
  } 
  
  getProvinceRegister(){
	  return new Promise((resolve, reject) => {			
		  this.http.get(apiUrl+'getProvinceRegister')
          .subscribe(res => {
			  resolve(res.json());
			 //console.log(todo.cutomer_id);
			}, (err) => {
			  //reject("Data Tidak Ditemukan");
			  reject(err);
		  });
         
    })
  } 

  kecamatanLead(){
	return new Promise((resolve, reject) => {			
		this.http.get(apiUrl+'kecamatanLead')
		.subscribe(res => {
			// console.log(res);
			resolve(res.json());
		  }, (err) => {
			//reject("Data Tidak Ditemukan");
			reject(err);
		});
	   
  })
} 
  
   getCityRegister(provinceId){
	  return new Promise((resolve, reject) => {			
		 this.http.get(apiUrl+'getCityRegister?provinceId='+provinceId)
          .subscribe(res => {
			  resolve(res.json());
			 //console.log(todo.cutomer_id);
			}, (err) => {
			  //reject("Data Tidak Ditemukan");
			  reject(err);
		  });
         
    })
  } 
  
   getKecamatanRegister(kotaId){
	  return new Promise((resolve, reject) => {			
		 this.http.get(apiUrl+'getKecamatanRegister?kotaId='+kotaId)
          .subscribe(res => {
			  resolve(res.json());
			 //console.log(todo.cutomer_id);
			}, (err) => {
			  //reject("Data Tidak Ditemukan");
			  reject(err);
		  });
         
    })
  } 
  
  getKelurahanRegister(kecamatanId){
	  return new Promise((resolve, reject) => {			
		 this.http.get(apiUrl+'getKelurahanRegister?kecamatanId='+kecamatanId)
          .subscribe(res => {
			  resolve(res.json());
			 //console.log(todo.cutomer_id);
			}, (err) => {
			  //reject("Data Tidak Ditemukan");
			  reject(err);
		  });
         
    })
  } 
  
  getCity(provinceId){
	  return new Promise((resolve, reject) => {			
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', localStorage.getItem('token'));
		
        if (provinceId == "") {
			reject("Id Belum Dipilih");
        } 
		else{
		  
		  this.http.get(apiUrl+'getCity?provinceId='+provinceId, {headers: headers})
          .subscribe(res => {
			  resolve(res.json());
			  //console.log(todo.surveyId);
			}, (err) => {
			  //reject("Data Tidak Ditemukan");
			  reject(err);
		  });
        } 
    })
  } 
  
  getKurir(){
	  return new Promise((resolve, reject) => {			
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', localStorage.getItem('token'));
		  
		  this.http.get(apiUrl+'getCourir', {headers: headers})
          .subscribe(res => {
			  resolve(res.json());
			  //console.log(todo.surveyId);
			}, (err) => {
			  //reject("Data Tidak Ditemukan");
			  reject(err);
		  });
    })
  }
  

  
  getKodeAgentGenerate(todo){
	return new Promise((resolve, reject) => {			
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		//headers.append('Authorization', localStorage.getItem('token'));
		
		//console.log(todo);
		  
		this.http.get(apiUrl+'kode_agent?kode_daftar='+todo.kode_set_pack+'&kode_kota='+todo.kotaid, {headers: headers})
		  .subscribe(res => {
			  resolve(res.json());
			}, (err) => {
			  reject(err);
		}); 
        
    })
  }
  
  getKodeCustomerGenerate(todo){
	return new Promise((resolve, reject) => {			
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		//headers.append('Authorization', localStorage.getItem('token'));
		
		//console.log(todo);
		  
		this.http.get(apiUrl+'kode_customer?kode_daftar='+todo.kode_daftar_customer+'&kode_kota='+todo.kotaid, {headers: headers})
		  .subscribe(res => {
			  resolve(res.json());
			}, (err) => {
			  reject(err);
		}); 
        
    })
  }
  
  getDataKunjungan(todo){
	  return new Promise((resolve, reject) => {			
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', localStorage.getItem('token'));
		
        if (todo.userId == "") {
			reject("Loading cost....");
        } 
		else{
		  
		  this.http.get(apiUrl+'getDataKunjungan?userId='+todo.userId, {headers: headers})
          .subscribe(res => {
			  resolve(res.json());
			  //console.log(todo.surveyId);
			}, (err) => {
			  //reject("Data Tidak Ditemukan");
			  reject(err);
		  });
        } 
    })
  }
  
  uploadFotoProfile(regData){
	return new Promise((resolve, reject) => {
        let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', localStorage.getItem('token'));
		
		this.http.post(apiUrl+'save_upload_profile', JSON.stringify(regData), {headers: headers})
		  .subscribe(res => {
			resolve(res.json());
			//console.log(todo);
		}, (err) => {
			reject(err);
			//console.log(err);
		})
		
    }); 
	
  }
  
  selesaikanPesanan(todo){
	return new Promise((resolve, reject) => {
        let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', localStorage.getItem('token'));
		
		this.http.post(apiUrl+'selesaikan_pesanan', JSON.stringify(todo), {headers: headers})
		  .subscribe(res => {
			resolve(res.json());
			//console.log(todo);
		}, (err) => {
			reject(err);
			//console.log(err);
		})
		
    }); 
	
  }
  
  saveTandaiKunjungan(todo){
	return new Promise((resolve, reject) => {
        let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', localStorage.getItem('token'));
		
		this.http.post(apiUrl+'saveTandaiKunjungan', JSON.stringify(todo), {headers: headers})
		  .subscribe(res => {
			resolve(res.json());
			//console.log(todo);
		}, (err) => {
			reject(err);
			//console.log(err);
		})
		
    });
	
  }
  
  save_data_lead(todo) {
    return new Promise((resolve, reject) => {
        let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', localStorage.getItem('token'));
		todo.userId   = localStorage.getItem('agent_id');
		
		if(todo.provinsiSelected == ""){
			reject("Provinsi harus diisi");
		} else if(todo.sumber_data == "") {
			reject("Sumber Data harus diisi");
		} else if(todo.citySelected == "") {
			reject("Kota harus diisi");
		} else if(todo.kecamatanSelected == "") {
			reject("Kecamatan harus diisi");
		} else if(todo.kategori_outlet == "") {
			reject("Kategori Outlet harus diisi");
		} else if(todo.nama_outlet == "") {
			reject("Nama Outlet harus diisi");
		} else if(todo.nama_kontak == "") {
			reject("Nama Kontak 1 harus diisi");
		} else if(todo.alamat == "") {
			reject("Alamat harus diisi");
		} else if(todo.sosial_media == "") {
			reject("Sosial Media harus diisi");
		} else if(todo.jumlah_outlet == "") {
			reject("Jumlah Outlet harus diisi");
		} else {
			this.http.post(apiUrl+'save_lead', JSON.stringify(todo), {headers: headers})
			  .subscribe(res => {
				resolve(res.json());
				//console.log(res.json());
			}, (err) => {
				reject(err);
				//console.log(err);
			})
		}
    });
  }
  
  save_detail_lead(regData) {
    return new Promise((resolve, reject) => {
        let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', localStorage.getItem('token'));
		
		this.http.post(apiUrl+'save_lead_detail', JSON.stringify(regData), {headers: headers})
		  .subscribe(res => {
			resolve(res.json());
			//console.log(res.json());
		}, (err) => {
			reject(err);
			//console.log(err);
		})
    });
  }
  
  save_detail_agenda(regData) {
    return new Promise((resolve, reject) => {
        let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', localStorage.getItem('token'));
		
		this.http.post(apiUrl+'save_agenda_detail', JSON.stringify(regData), {headers: headers})
		  .subscribe(res => {
			resolve(res.json());
			//console.log(res.json());
		}, (err) => {
			reject(err);
			//console.log(err);
		})
    });
  }
  
  saveLeadBaru(todo) {
    return new Promise((resolve, reject) => {
        let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', localStorage.getItem('token'));
		todo.userId   = localStorage.getItem('agent_id');
		
		this.http.post(apiUrl+'save_lead_baru', JSON.stringify(todo), {headers: headers})
		  .subscribe(res => {
			resolve(res.json());
			//console.log(res.json());
		}, (err) => {
			reject(err);
			//console.log(err);
		})
    });
  }
  
   save_data_agenda(todo) {
    return new Promise((resolve, reject) => {
        let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', localStorage.getItem('token'));
		todo.userId   = localStorage.getItem('agent_id');
		
		if (todo.title == "" || todo.message == "" || todo.startDate == "" || todo.jam == "") {
          reject("Field harus diisi");
        } 
		else {
			this.http.post(apiUrl+'save_agenda', JSON.stringify(todo), {headers: headers})
			  .subscribe(res => {
				resolve(res.json());
				//console.log(res.json());
			}, (err) => {
				reject(err);
				//console.log(err);
			})
		}
    });
  }

  saveTransaksiUser(params) {
    return new Promise((resolve, reject) => {
        let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', localStorage.getItem('token'));
		
		if(params.customer_id !== undefined){
			if(params.cus_alamat =="" || params.cus_alamat == null){ 
				reject("Alamat tujuan harus diisi");
			} 
			else if(params.cus_provinsi == null){
				reject("Provinsi tujuan harus diisi");
			}
			else if(params.cus_city ==null){
				reject("Kota tujuan harus diisi");
			}
			else if(params.cus_kurir == null){
				reject("Pengiriman harus diisi");
			}
			else if(params.cus_paymethod == null){
				reject("Bank Pembayaran harus diisi");
			}
			else if(params.cus_stock_point === undefined){
				reject("Silahkan pilih stock point pengambilan product");
			}		
			else if(params.cus_paket == null){
				reject("Silahkan pilih paket pengiriman");
			}		
			else {
				this.http.post(apiUrl+'saveTransaksiUser', JSON.stringify(params), {headers: headers})
				  .subscribe(res => {
					resolve(res.json());
				}, (err) => {
					reject(err);
				})
				//console.log(params.cus_paymethod);
			}
		} 
		else {
			reject("Field harus diisi");
			//console.log(params);
		}
    });
	
	
  } 
  
  saveTransaksiSampleUser(params) {
    return new Promise((resolve, reject) => {
        let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', localStorage.getItem('token'));
		
		if(params.customer_id !== undefined){
			if(params.cus_alamat =="" || params.cus_alamat == null){ 
				reject("Alamat tujuan harus diisi");
			} 
			else if(params.cus_provinsi =="0-null"){
				reject("Provinsi tujuan harus diisi");
			}
			else if(params.cus_city =="0-null-null-null"){
				reject("Kota tujuan harus diisi");
			}
			else if(params.cus_kurir == null){
				reject("Pengiriman harus diisi");
			}
			else if(params.cus_stock_point === undefined){
				reject("Silahkan pilih stock point pengambilan product");
			}
			else {
				this.http.post(apiUrl+'saveTransaksiSampleUser', JSON.stringify(params), {headers: headers})
				  .subscribe(res => {
					resolve(res.json());
				}, (err) => {
					reject(err);
				})
				//console.log(params.cus_paymethod);
			}
		} 
		else {
			reject("Field harus diisi");
			//console.log(params);
		}
    });
	
	
  } 

  getOriginStock(params) {
    return new Promise((resolve, reject) => {
        let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', localStorage.getItem('token'));
			
		this.http.post(apiUrl+'getOriginStock', JSON.stringify(params), {headers: headers})
		  .subscribe(res => {
			resolve(res.json());
		}, (err) => {
			reject(err);
		})			
    });
  }   
  
  getCart() {
    return this.cart;
  }
 
  getCartItemCount() {
    return this.cartItemCount;
  }
 
  addProduct(product) {
    let added = false;
	let tipe = localStorage.getItem('price_type_user');
    for (let p of this.cart) {
      	if (p.id === product.id) {
			let date = new Date().toISOString();
			console.log(p.amount);
			if(product.id == 4 && tipe == '2' && date.substring(0, 10) == '2021-06-25'){
				p.price = 110909;
				p.amount += 1;
				added = true;
				break;
			} else if(product.id == 4 && tipe == '2' && date.substring(0, 10) == '2021-06-26'){
				p.price = 110909;
				p.amount += 1;
				added = true;
				break;
			} else if(product.id == 4 && tipe == '2' && date.substring(0, 10) == '2021-06-27'){
				p.price = 110909;
				p.amount += 1;
				added = true;
				break;
			} else if(product.id == 4 && tipe == '2' && date.substring(0, 10) == '2021-06-28'){
				p.price = 110909;
				p.amount += 1;
				added = true;
				break;
			} else if(product.id == 4 && p.amount >= 12 && p.amount <= 179 && tipe == '3') {
				p.price = 128000;
				p.amount += 1;
				added = true;
				break;
		  	} else if(product.id == 4 && p.amount >= 179 && tipe == '3') {
				p.price = 122000;
				p.amount += 1;
				added = true;
				break;
		  	} else if(product.id == 4 && p.amount <= 11 && tipe == '3'){
			  	p.price = 138000;
			  	p.amount += 1;
			  	added = true;
			  	break;
			} else if(product.id == 4 && p.amount >= 12 && p.amount <= 179 && tipe == '2'){
				p.price = 116364;
				p.amount += 1;
				added = true;
				break;
			} else if(product.id == 4 && p.amount <= 11 && tipe == '2') {
				p.price = 125455;
				p.amount += 1;
				added = true;
				break;
		  	} else if(product.id == 4 && p.amount >= 179 && tipe == '2') {
				p.price = 110909;
				p.amount += 1;
				added = true;
				break;
			} else if(product.id == 4 && p.amount <= 4 && tipe == '1') {
				p.price = 140000;
				p.amount += 1;
				added = true;
				break;
		  	} else if(product.id == 4 && p.amount >= 5 && p.amount <= 34 && tipe == '1'){
				p.price = 138000;
				p.amount += 1;
				added = true;
				break;
			} else if(product.id == 4 && p.amount >= 35 && tipe == '1'){
				p.price = 135000;
				p.amount += 1;
				added = true;
				break;
			} else if(product.id == 7 && p.amount <= 4 && tipe == '1'){
				p.price = 89000;
				p.amount += 1;
				added = true;
				break;
			} else if(product.id == 7 && p.amount >= 5  && p.amount <= 34 && tipe == '1'){
				p.price = 87000;
				p.amount += 1;
				added = true;
				break;
			} else if(product.id == 7 && p.amount >= 35  && tipe == '1'){
				p.price = 85000;
				p.amount += 1;
				added = true;
				break;
			} else if(product.id == 7 && p.amount >= 35  && tipe == '1'){
				p.price = 85000;
				p.amount += 1;
				added = true;
				break;
			} else if(product.id == 63 && p.amount <= 34  && tipe == '1'){
				p.price = 85000;
				p.amount += 1;
				added = true;
				break;
			} else if(product.id == 63 && p.amount >= 35  && p.amount <= 59 && tipe == '1'){
				p.price = 87000;
				p.amount += 1;
				added = true;
				break;
			} else if(product.id == 63 && p.amount >= 60  && tipe == '1'){
				p.price = 85000;
				p.amount += 1;
				added = true;
				break;
			} else if(product.id == 66 && p.amount >= 4 && p.amount <= 28 && tipe == '1'){
				p.price = 72000;
				p.amount += 1;
				added = true;
				break;
			} else if(product.id == 66 && p.amount >= 29 && tipe == '1'){
				p.price = 70000;
				p.amount += 1;
				added = true;
				break;
			} else if(product.id == 66 && p.amount <= 3 && tipe == '1'){
				p.price = 75000;
				p.amount += 1;
				added = true;
				break;
			} else if(product.id == 70 && p.amount >= 4 && p.amount <= 38 && tipe == '1'){
				p.price = 77000;
				p.amount += 1;
				added = true;
				break;
			} else if(product.id == 70 && p.amount <= 3 && tipe == '1'){
				p.price = 80000;
				p.amount += 1;
				added = true;
				break;
			} else if(product.id == 70 && p.amount >= 39 && tipe == '1'){
				p.price = 75000;
				p.amount += 1;
				added = true;
				break;
			} else if(product.id == 72 && p.amount >= 4 && p.amount >= 38 && tipe == '1'){
				p.price = 97000;
				p.amount += 1;
				added = true;
				break;
			} else if(product.id == 72 && p.amount <= 3 && tipe == '1'){
				p.price = 100000;
				p.amount += 1;
				added = true;
				break;
			} else if(product.id == 72 && p.amount >= 39 && tipe == '1'){
				p.price = 95000;
				p.amount += 1;
				added = true;
				break;
			} else if(product.id == 77 && p.amount >= 39 && tipe == '1'){
				p.price = 155000;
				p.amount += 1;
				added = true;
				break;
			} else if(product.id == 91 && p.amount >= 39 && tipe == '1'){
				p.price = 155000;
				p.amount += 1;
				added = true;
				break;
			} else if(product.id == 83 && p.amount >= 39 && tipe == '1'){
				p.price = 155000;
				p.amount += 1;
				added = true;
				break;
			} else if(product.id == 78 && p.amount >= 39 && tipe == '1'){
				p.price = 155000;
				p.amount += 1;
				added = true;
				break;
			} else if(product.id == 79 && p.amount >= 39 && tipe == '1'){
				p.price = 155000;
				p.amount += 1;
				added = true;
				break;
			} else if(product.id == 71 && p.amount >= 4 && p.amount >=38 && tipe == '1'){
				p.price = 87000;
				p.amount += 1;
				added = true;
				break;
			} else if(product.id == 71 && p.amount >= 39 && tipe == '1'){
				p.price = 85000;
				p.amount += 1;
				added = true;
				break;
			} else if(product.id == 71 && p.amount <= 3 && tipe == '1'){
				p.price = 90000;
				p.amount += 1;
				added = true;
				break;
			} else if(product.id == 35 && p.amount >= 4 && p.amount <= 28 && tipe == '1'){
				p.price = 68000;
				p.amount += 1;
				added = true;
				break;
			} else if(product.id == 35 && p.amount <= 3 && tipe == '1'){
				p.price = 70000;
				p.amount += 1;
				added = true;
				break;
			} else if(product.id == 35 && p.amount >= 29 && tipe == '1'){
				p.price = 65000;
				p.amount += 1;
				added = true;
				break;
			} else if(product.id == 92 && p.amount >= 39 && tipe == '1'){
				p.price = 60000;
				p.amount += 1;
				added = true;
				break;
			} else if(product.id == 75 && p.amount >= 49 && tipe == '1'){
				p.price = 135000;
				p.amount += 1;
				added = true;
				break;
			} else if(product.id == 75 && p.amount >= 29  && p.amount >= 48 && tipe == '1'){
				p.price = 140000;
				p.amount += 1;
				added = true;
				break;
			} else if(product.id == 75 && p.amount <= 28 && tipe == '1'){
				p.price = 145000;
				p.amount += 1;
				added = true;
				break;
			} else if(product.id == 93 && p.amount >= 39 && tipe == '1'){
				p.price = 115000;
				p.amount += 1;
				added = true;
				break;
			} else if(product.id == 94 && p.amount >= 31 && tipe == '1'){
				p.price = 155000;
				p.amount += 1;
				added = true;
				break;
			} else if(product.id == 95 && p.amount >= 119 && tipe == '1'){
				p.price = 65000;
				p.amount += 1;
				added = true;
				break;
			} else if(product.id == 96 && p.amount >= 89  && p.amount <= 148 && tipe == '1'){
				p.price = 120000;
				p.amount += 1;
				added = true;
				break;
			} else if(product.id == 96 && p.amount <= 88  && tipe == '1'){
				p.price = 125000;
				p.amount += 1;
				added = true;
				break;
			} else if(product.id == 96 && p.amount >= 149 && tipe == '1'){
				p.price = 115000;
				p.amount += 1;
				added = true;
				break;
			} else if(product.id == 97 && p.amount >= 119 && tipe == '1'){
				p.price = 165000;
				p.amount += 1;
				added = true;
				break;
			} else if(product.id == 98 && p.amount >= 119 && tipe == '1'){
				p.price = 125000;
				p.amount += 1;
				added = true;
				break;
			} else if(product.id == 63 && p.amount >= 35  && p.amount <= 58 && tipe == '1'){
				p.price = 145000;
				p.amount += 1;
				added = true;
				break;
			} else if(product.id == 63 && p.amount >= 59 && tipe == '1'){
				p.price = 140000;
				p.amount += 1;
				added = true;
				break;
			} else if(product.id == 63 && p.amount <= 34 && tipe == '1'){
				p.price = 150000;
				p.amount += 1;
				added = true;
				break;
			} else if(product.id == 86 && p.amount >= 35 && tipe == '1'){
				p.price = 210000;
				p.amount += 1;
				added = true;
				break;
			} else if(product.id == 87 && p.amount >= 35 && p.amount <= 58 && tipe == '1'){
				p.price = 145000;
				p.amount += 1;
				added = true;
				break;
			} else if(product.id == 87 && p.amount >= 59 && tipe == '1'){
				p.price = 140000;
				p.amount += 1;
				added = true;
				break;
			} else if(product.id == 87 && p.amount <= 34 && tipe == '1'){
				p.price = 150000;
				p.amount += 1;
				added = true;
				break;
			} else if(product.id == 57 && p.amount >= 19 && p.amount <= 38 && tipe == '1'){
				p.price = 125000;
				p.amount += 1;
				added = true;
				break;
			} else if(product.id == 57 && p.amount >= 18 && tipe == '1'){
				p.price = 130000;
				p.amount += 1;
				added = true;
				break;
			} else if(product.id == 57 && p.amount >= 39 && tipe == '1'){
				p.price = 120000;
				p.amount += 1;
				added = true;
				break;
			} else if(product.id == 88 && p.amount >= 191 && tipe == '1'){
				p.price = 55000;
				p.amount += 1;
				added = true;
				break;
			} else if(product.id == 100 && p.amount >= 39 && tipe == '1'){
				p.price = 155000;
				p.amount += 1;
				added = true;
				break;
			} else if(product.id == 89 && p.amount >= 191 && tipe == '1'){
				p.price = 55000;
				p.amount += 1;
				added = true;
				break;
			} else if(product.id == 90 && p.amount >= 191 && tipe == '1'){
				p.price = 55000;
				p.amount += 1;
				added = true;
				break;
			} else if(product.id == 8 && p.amount >= 4 && p.amount <= 28 && tipe == '1'){
				p.price = 88000;
				p.amount += 1;
				added = true;
				break;
			} else if(product.id == 8 && p.amount <= 3 && tipe == '1'){
				p.price = 90000;
				p.amount += 1;
				added = true;
				break;
			} else if(product.id == 8 && p.amount >= 29 && tipe == '1'){
				p.price = 85000;
				p.amount += 1;
				added = true;
				break;
			} else {
				p.amount += 1;
				added = true;
				break;
			}
		}
			//this.cartItemCount.next(this.cartItemCount.value + 1); 

    }
    if (!added) {
       product.amount = 1;
      this.cart.push(product); 
	
	  this.cartItemCount.next(this.cartItemCount.value + 1); 
    }
    
	
  }
 
  decreaseProduct(product) {
	let tipe = localStorage.getItem('price_type_user');
   	for (let [index, p] of this.cart.entries()) {
      if (p.id === product.id) {
		  	let date = new Date().toISOString();
			console.log(p.amount);
			if(product.id == 4 && tipe == '2' && date.substring(0, 10) == '2021-06-25'){
				p.price = 110909;
				p.amount -= 1;
			} else if(product.id == 4 && tipe == '2' && date.substring(0, 10) == '2021-06-26'){
				p.price = 110909;
				p.amount -= 1;
			} else if(product.id == 4 && tipe == '2' && date.substring(0, 10) == '2021-06-27'){
				p.price = 110909;
				p.amount -= 1;
			} else if(product.id == 4 && tipe == '2' && date.substring(0, 10) == '2021-06-28'){
				p.price = 110909;
				p.amount -= 1;
			} else if(p.id == 4 && p.amount >= 14 && p.amount <= 181 && tipe == '3') {
				p.price = 128000;
				p.amount -= 1;
			} else if(product.id == 4 && p.amount >= 182 && tipe == '3') {
				p.price = 122000;
				p.amount -= 1;
			} else if(product.id == 4 && p.amount <= 13 && tipe == '3'){
				p.price = 138000;
				p.amount -= 1;
			} else if(product.id == 4 && p.amount <= 13 && tipe == '2'){
				p.price = 125455;
				p.amount -= 1;
			} else if(p.id == 4 && p.amount >= 14 && p.amount <= 181 && tipe == '2') {
				p.price = 116364;
				p.amount -= 1;
			} else if(product.id == 4 && p.amount >= 182 && tipe == '2') {
				p.price = 110909;
				p.amount -= 1;
			} else if(product.id == 7 && p.amount <= 6 && tipe == '1'){
				p.price = 89000;
				p.amount -= 1;
			
				break;
			} else if(product.id == 7 && p.amount >= 7  && p.amount <= 36 && tipe == '1'){
				p.price = 87000;
				p.amount -= 1;
			
				break;
			} else if(product.id == 7 && p.amount >= 37  && tipe == '1'){
				p.price = 85000;
				p.amount -= 1;
			
				break;
			}  else if(product.id == 63 && p.amount <= 36  && tipe == '1'){
				p.price = 85000;
				p.amount -= 1;
			
				break;
			} else if(product.id == 63 && p.amount >= 37  && p.amount <= 59 && tipe == '1'){
				p.price = 87000;
				p.amount -= 1;
			
				break;
			} else if(product.id == 63 && p.amount >= 62  && tipe == '1'){
				p.price = 85000;
				p.amount -= 1;
			
				break;
			} else if(product.id == 66 && p.amount >= 6 && p.amount <= 30 && tipe == '1'){
				p.price = 72000;
				p.amount -= 1;
			
				break;
			} else if(product.id == 66 && p.amount >= 31 && tipe == '1'){
				p.price = 70000;
				p.amount -= 1;
			
				break;
			} else if(product.id == 66 && p.amount <= 5 && tipe == '1'){
				p.price = 75000;
				p.amount -= 1;
			
				break;
			} else if(product.id == 70 && p.amount >= 6 && p.amount <= 40 && tipe == '1'){
				p.price = 77000;
				p.amount -= 1;
			
				break;
			} else if(product.id == 70 && p.amount <= 5 && tipe == '1'){
				p.price = 80000;
				p.amount -= 1;
			
				break;
			} else if(product.id == 70 && p.amount >= 41 && tipe == '1'){
				p.price = 75000;
				p.amount -= 1;
			
				break;
			} else if(product.id == 72 && p.amount >= 6 && p.amount >= 40 && tipe == '1'){
				p.price = 97000;
				p.amount -= 1;
			
				break;
			} else if(product.id == 72 && p.amount <= 5 && tipe == '1'){
				p.price = 100000;
				p.amount -= 1;
			
				break;
			} else if(product.id == 72 && p.amount >= 41 && tipe == '1'){
				p.price = 95000;
				p.amount -= 1;
			
				break;
			} else if(product.id == 77 && p.amount >= 41 && tipe == '1'){
				p.price = 155000;
				p.amount -= 1;
			
				break;
			} else if(product.id == 91 && p.amount >= 41 && tipe == '1'){
				p.price = 155000;
				p.amount -= 1;
			
				break;
			} else if(product.id == 83 && p.amount >= 41 && tipe == '1'){
				p.price = 155000;
				p.amount -= 1;
			
				break;
			} else if(product.id == 78 && p.amount >= 41 && tipe == '1'){
				p.price = 155000;
				p.amount -= 1;
			
				break;
			} else if(product.id == 79 && p.amount >= 41 && tipe == '1'){
				p.price = 155000;
				p.amount -= 1;
			
				break;
			} else if(product.id == 71 && p.amount >= 6 && p.amount >=40 && tipe == '1'){
				p.price = 87000;
				p.amount -= 1;
			
				break;
			} else if(product.id == 71 && p.amount >= 41 && tipe == '1'){
				p.price = 85000;
				p.amount -= 1;
			
				break;
			} else if(product.id == 71 && p.amount <= 5 && tipe == '1'){
				p.price = 90000;
				p.amount -= 1;
			
				break;
			} else if(product.id == 35 && p.amount >= 6 && p.amount <= 30 && tipe == '1'){
				p.price = 68000;
				p.amount -= 1;
			
				break;
			} else if(product.id == 35 && p.amount <= 5 && tipe == '1'){
				p.price = 70000;
				p.amount -= 1;
			
				break;
			} else if(product.id == 35 && p.amount >= 31 && tipe == '1'){
				p.price = 65000;
				p.amount -= 1;
			
				break;
			} else if(product.id == 92 && p.amount >= 41 && tipe == '1'){
				p.price = 60000;
				p.amount -= 1;
			
				break;
			} else if(product.id == 75 && p.amount >= 51 && tipe == '1'){
				p.price = 135000;
				p.amount -= 1;
			
				break;
			} else if(product.id == 75 && p.amount >= 31  && p.amount >= 48 && tipe == '1'){
				p.price = 140000;
				p.amount -= 1;
			
				break;
			} else if(product.id == 75 && p.amount <= 31 && tipe == '1'){
				p.price = 145000;
				p.amount -= 1;
			
				break;
			} else if(product.id == 93 && p.amount >= 41 && tipe == '1'){
				p.price = 115000;
				p.amount -= 1;
			
				break;
			} else if(product.id == 94 && p.amount >= 33 && tipe == '1'){
				p.price = 155000;
				p.amount -= 1;
			
				break;
			} else if(product.id == 95 && p.amount >= 121 && tipe == '1'){
				p.price = 65000;
				p.amount -= 1;
			
				break;
			} else if(product.id == 96 && p.amount >= 91  && p.amount <= 150 && tipe == '1'){
				p.price = 120000;
				p.amount -= 1;
			
				break;
			} else if(product.id == 96 && p.amount <= 90  && tipe == '1'){
				p.price = 125000;
				p.amount -= 1;
			
				break;
			} else if(product.id == 96 && p.amount >= 151 && tipe == '1'){
				p.price = 115000;
				p.amount -= 1;
			
				break;
			} else if(product.id == 97 && p.amount >= 121 && tipe == '1'){
				p.price = 165000;
				p.amount -= 1;
			
				break;
			} else if(product.id == 98 && p.amount >= 121 && tipe == '1'){
				p.price = 125000;
				p.amount -= 1;
			
				break;
			} else if(product.id == 63 && p.amount >= 37  && p.amount <= 60 && tipe == '1'){
				p.price = 145000;
				p.amount -= 1;
			
				break;
			} else if(product.id == 63 && p.amount >= 61 && tipe == '1'){
				p.price = 140000;
				p.amount -= 1;
			
				break;
			} else if(product.id == 63 && p.amount <= 36 && tipe == '1'){
				p.price = 150000;
				p.amount -= 1;
			
				break;
			} else if(product.id == 86 && p.amount >= 37 && tipe == '1'){
				p.price = 210000;
				p.amount -= 1;
			
				break;
			} else if(product.id == 87 && p.amount >= 37 && p.amount <= 58 && tipe == '1'){
				p.price = 145000;
				p.amount -= 1;
			
				break;
			} else if(product.id == 87 && p.amount >= 61 && tipe == '1'){
				p.price = 140000;
				p.amount -= 1;
			
				break;
			} else if(product.id == 87 && p.amount <= 36 && tipe == '1'){
				p.price = 150000;
				p.amount -= 1;
			
				break;
			} else if(product.id == 57 && p.amount >= 21 && p.amount <= 40 && tipe == '1'){
				p.price = 125000;
				p.amount -= 1;
			
				break;
			} else if(product.id == 57 && p.amount >= 20 && tipe == '1'){
				p.price = 130000;
				p.amount -= 1;
			
				break;
			} else if(product.id == 57 && p.amount >= 41 && tipe == '1'){
				p.price = 120000;
				p.amount -= 1;
			
				break;
			} else if(product.id == 88 && p.amount >= 193 && tipe == '1'){
				p.price = 55000;
				p.amount -= 1;
			
				break;
			} else if(product.id == 100 && p.amount >= 41 && tipe == '1'){
				p.price = 155000;
				p.amount -= 1;
			
				break;
			} else if(product.id == 89 && p.amount >= 193 && tipe == '1'){
				p.price = 55000;
				p.amount -= 1;
			
				break;
			} else if(product.id == 90 && p.amount >= 193 && tipe == '1'){
				p.price = 55000;
				p.amount -= 1;
			
				break;
			} else if(product.id == 8 && p.amount >= 6 && p.amount <= 30 && tipe == '1'){
				p.price = 88000;
				p.amount -= 1;
			
				break;
			} else if(product.id == 8 && p.amount <= 5 && tipe == '1'){
				p.price = 90000;
				p.amount -= 1;
			
				break;
			} else if(product.id == 8 && p.amount >= 31 && tipe == '1'){
				p.price = 85000;
				p.amount -= 1;
				break;
			} else {
			    p.amount -= 1;
			}
        	if (p.amount == 0) {
        	  this.cart.splice(index, 1);
			  this.cartItemCount.next(this.cartItemCount.value - 1);
        	}
     	}
    }
   
	//console.log(this.getCart());
	
  }
 
  removeProduct(product) {
    for (let [index, p] of this.cart.entries()) {
      	if (p.id === product.id) {
        	//this.cartItemCount.next(this.cartItemCount.value - p.amount);
			this.cartItemCount.next(this.cartItemCount.value - 1);
        	this.cart.splice(index, 1);
      	}
    }
  }
  
  removeAllCartProduct(total, panjang){
	 this.cartItemCount.next(this.cartItemCount.value - panjang);
	 this.cart.splice(total, panjang);
  }
  
  getListPerluKirim(todo){
	  return new Promise((resolve, reject) => {			
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', localStorage.getItem('token'));
		
        if (todo.kode_sales == "") {
			reject("Id Belum Dipilih");
        } 
		else{
		  
		  this.http.get(apiUrl+'get_list_perlukirim?sales_kode='+todo.kode_sales, {headers: headers})
          .subscribe(res => {
			  resolve(res.json());
			  //console.log(todo.userId);
			}, (err) => {
			  //reject("Data Tidak Ditemukan");
			  reject(err);
		  });
        } 
    })
  }
  
  saveKirimPaket(id){
	  return new Promise((resolve, reject) => {			
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', localStorage.getItem('token'));
		
        if (id == "") {
			reject("Id Belum Dipilih");
        } 
		else{
		  
		  this.http.get(apiUrl+'set_perlukirim?id='+id, {headers: headers})
          .subscribe(res => {
			  resolve(res.json());
			  //console.log(todo.userId);
			}, (err) => {
			  //reject("Data Tidak Ditemukan");
			  reject(err);
		  });
        } 
    })
  }
  
    saveTokenFirebase(params){
	return new Promise((resolve, reject) => {
        let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', localStorage.getItem('token'));
		
		this.http.post(apiUrl+'saveTokenFirebase', JSON.stringify(params), {headers: headers})
		  .subscribe(res => {
			resolve(res.json());
			//console.log(todo);
		}, (err) => {
			reject(err);
			//console.log(err);
		})
		
    });
	
  }


}
