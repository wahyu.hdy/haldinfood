import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController, ModalController } from 'ionic-angular';
import { OrdersPage } from '../orders/orders';
import { AuthService } from '../../providers/auth-service/auth-service';
import { CartSamplePage } from '../cart-sample/cart-sample';
import { BehaviorSubject } from 'rxjs';
//import { DetailProductPage } from '../../pages/detail-product/detail-product';
import { HomePage } from '../../pages/home/home';

/**
 * Generated class for the SampleProductPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-sample-product',
  templateUrl: 'sample-product.html',
})
export class SampleProductPage {
	
  public data_item: any;  
  
  todo:any = {};
  data:any = {};
  loading: any;
  
  cart = [];
  products = [];
  cartItemCount: BehaviorSubject<number>;
 
  @ViewChild('cart', {read: ElementRef})fab: ElementRef;

  constructor(public navCtrl: NavController, public navParams: NavParams, public authService: AuthService, public loadingCtrl: LoadingController, private toastCtrl: ToastController, public modalCtrl: ModalController) {
	   this.todo.userId = localStorage.getItem('agent_id');
	   this.todo.price_type_user = localStorage.getItem('price_type_user');
	   this.dataProductSample();
  }
  
    ngOnInit() {
		this.cart = this.authService.getCart();
		this.cartItemCount = this.authService.getCartItemCount();
	}
  
   dataProductSample() {	
		this.showLoader();
		this.authService.getListProductSample(this.todo).then((result) => {
			this.data = result;
			let cek_respon = this.data.result_status;
			//console.log(cek_respon);
			if(cek_respon != '0'){
				//console.log(JSON.stringify(this.data));
				this.data_item = this.data.result_data;
				
			} 
			this.loading.dismiss();
             
        }).catch((err) => {
			this.loading.dismiss();
            let toast = this.toastCtrl.create({
                message: err,
                duration: 2000,
                position: 'bottom'
            });
            toast.present();
        })
	}
	
	finishOrder() {
		this.navCtrl.push(OrdersPage);
	}
	
	addToCartSample(product) {
		this.authService.addProduct(product);
		//this.animateCSS('tada');
		
		//console.log(product);
	}
	 
	async openCartSample() {
		this.navCtrl.push(CartSamplePage);
	}
	
	showLoader(){
		this.loading = this.loadingCtrl.create({
			content: 'Loading...'
		});

		this.loading.present();
	}

    presentToast(msg) {
		let toast = this.toastCtrl.create({
			  message: msg,
			  duration: 3000,
			  position: 'bottom',
			  dismissOnPageChange: true
		});

		toast.onDidDismiss(() => {
		  console.log('Dismissed toast');
		});

		toast.present();
    }
	
	goBack(){
		 this.navCtrl.push(HomePage);
	}
	
	doRefresh(refresher) {
		//console.log('Begin async operation', refresher);
        this.navCtrl.setRoot(this.navCtrl.getActive().component);
		setTimeout(() => {
		  //console.log('Async operation has ended');
		  refresher.complete();
		}, 2000);
	  }


}
