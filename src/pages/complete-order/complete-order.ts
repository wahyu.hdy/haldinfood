import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { HomePage } from '../../pages/home/home';
import { AuthService } from '../../providers/auth-service/auth-service';
import { InAppBrowser, InAppBrowserOptions  } from '@ionic-native/in-app-browser/ngx';

/**
 * Generated class for the CompleteOrderPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-complete-order',
  templateUrl: 'complete-order.html',
})
export class CompleteOrderPage {
  todo:any = {};
  payment_nicepay = [];  
  cart = [];
   loading: any;
   data:any = {};
   
   regData = { timeStamp: "", tXid: "", merchantToken: "", callBackUrl: ""};	

  options : InAppBrowserOptions = {
    location : 'yes',//Or 'no' 
    hidden : 'no', //Or  'yes'
    zoom : 'yes',//Android only ,shows browser zoom controls 
    hideurlbar:'yes',//Or 'no'
	hidenavigationbuttons:'no',

};

  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, private toastCtrl: ToastController,  public authService: AuthService, private iab: InAppBrowser) {
	  this.payment_nicepay = navParams.get('completePayment');
	  this.todo.tanggal_set = navParams.get('tanggal_order');
	  this.todo.jam_set     = navParams.get('jam_order');
	  this.todo.tanggal_pay = navParams.get('tanggal_pay');
	  this.todo.pay_methode = navParams.get('pay_methode');
	  this.todo.card_metode = navParams.get('card_metode');
	  this.todo.providerUser   = localStorage.getItem('provider_user');
	  this.todo.amt     = navParams.get('ov_amt');
	  this.todo.tixId   = navParams.get('ov_tixId');
	  this.todo.mctoken = navParams.get('ov_mctoken');
	  this.todo.tmtime = navParams.get('ov_time');
  }

   /* ionViewDidLoad() {
   this.navCtrl.push(HomePage);
  }  */
  
  closePage(){
	  this.showLoader();
	  this.cart = this.authService.getCart();
	  this.authService.removeAllCartProduct(0, this.cart.length);
	  this.navCtrl.push(HomePage);
	  this.loading.dismiss();
  }
  
  
  showLoader(){
		this.loading = this.loadingCtrl.create({
			content: 'Loading...'
		});

		this.loading.present();
  }

  presentToast(msg) {
		let toast = this.toastCtrl.create({
			  message: msg,
			  duration: 3000,
			  position: 'bottom',
			  dismissOnPageChange: true
		});

		toast.onDidDismiss(() => {
		  console.log('Dismissed toast');
		});

		toast.present();
   }
 
    save_complete_ovo(){
	   /*  $http.post("https://api.nicepay.co.id/nicepay/direct/v2/payment",{timeStamp: this.todo.tmtime, tXid: this.todo.tixId, merchantToken: this.todo.mctoken, callBackUrl: 'http://www.omedia.web.id/api-haldin-agent/api/getCallBackUrl'}).then(function(res){
			this.data = res
			console.log(this.data.resultMsg); // it will print result from url
		})  */
		
		//console.log(this.todo.tmtime);
		//console.log(this.todo.tixId);
		//console.log(this.todo.mctoken);
		
		
		/* $http.get("http://www.omedia.web.id/api-haldin-agent/api/getCallBackUrl").then(function(res){
			this.data = res
			console.log(this.data.resultMsg); // it will print result from url
		}) */
	}
	
	openWebpage(time,tixId,mcToken,card_metode) {
	   //console.log(this.resiData.noresi);
	    let url_data = 'https://apps.haldinfoods.com/api-haldin-agent/api/callPaymentUrl?timeStamp='+time+'&tXid='+tixId+'&merchantToken='+mcToken+'&cardCode='+card_metode;
       /*  const options: InAppBrowserOptions = {
          zoom: 'no'
        } */

        // Opening a URL and returning an InAppBrowserObject
        this.iab.create(url_data, '_blank', this.options); 

       // Inject scripts, css and more with browser.X
    }
	
	copyInputMessage(inputElement){
		inputElement.select();
		document.execCommand('copy');
		inputElement.setSelectionRange(0, 0);
		alert("Berhasil Dicopy");
	}

}
