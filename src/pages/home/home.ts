import { Component } from '@angular/core';
import { NavController, AlertController, ToastController } from 'ionic-angular';
import { PopoverController } from 'ionic-angular';
import { PopoverPage } from '../components/popover/popover';
import { ProductPage } from '../product/product';
import { SurveyPage } from '../survey/survey';
import { AuthService } from '../../providers/auth-service/auth-service';
import { LeadPage } from '../lead/lead';
import { AgendaPage } from '../agenda/agenda';
import { DsTransaksiPage } from '../ds-transaksi/ds-transaksi';
import { StTransaksiPage } from '../st-transaksi/st-transaksi';
import { ProfileUserPage } from '../profile-user/profile-user';
import { BantuanPage } from '../bantuan/bantuan';
import { ApprovalAgentPage } from '../approval-agent/approval-agent';
import { PerformaPage } from '../performa/performa';
import { BelanjaOcPage } from '../belanja-oc/belanja-oc';
import { SalesCustomerPage } from '../sales-customer/sales-customer';
import { SalesAktifasiUserPage } from '../sales-aktifasi-user/sales-aktifasi-user';
import { KomisiUserPage } from '../komisi-user/komisi-user';
import { PerluKirimPage } from '../perlu-kirim/perlu-kirim';
import { SampleProductPage } from '../sample-product/sample-product';
import { Firebase } from '@ionic-native/firebase';
import { NotificationsPage } from '../notifications/notifications';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  
  public total_agenda_open: string;
  public total_lead_open: string;
  public total_survey: string;
  public total_transaksi: string;
  public greet: string;
  
  tab1Root = SurveyPage;
  tab2Root = ProfileUserPage;
  tab3Root = BantuanPage;
  
  todo:any = {};
  data:any = {};

  constructor(public navCtrl: NavController,public popoverCtrl: PopoverController, private alertCtrl: AlertController, public authService: AuthService, private toastCtrl: ToastController, firebase: Firebase) {
	   this.todo.userId         = localStorage.getItem('agent_id');
	   this.todo.providerUser   = localStorage.getItem('provider_user');
	   this.todo.nama_user      = localStorage.getItem('nama_user');
     this.todo.kode_sales      = localStorage.getItem('kode_sales');
     this.todo.menu_open_id   = localStorage.getItem('menu_open_id');
	   //console.log(this.todo.providerUser );
	   this.totalSurveyData();
     this.totalTransaksiData();
     this.totalAgendaOpen();
     this.totalLeadOpen();
	   
	   /*firebase.getToken()
       .then(token => this.register_token_firebase(this.todo.userId, token) ) 
       .catch(error => console.error('Error getting token', error));*/
  }
  
 register_token_firebase(user_id, token){
		let params = { 
		  user_agent_id  : user_id,
		  token_firebase : token
		};
	    
	    this.authService.saveTokenFirebase(params).then((result) => {}).catch(err=> console.log(err));
  } 
  
  aktifasiCustomer(){
	  this.navCtrl.push(SalesAktifasiUserPage);
  }
  
  addCustomer(){
	  this.navCtrl.push(SalesCustomerPage);
  }
  
  myProfile(){
	  this.navCtrl.push(ProfileUserPage);
  }
  
  myHelp(){
	  this.navCtrl.push(BantuanPage);
  }
  
  create_cart_oc(){
	  this.navCtrl.push(BelanjaOcPage);
  }
  
  dashboard_transaksi(){
	   this.navCtrl.push(DsTransaksiPage);
  }
  
  dashboard_status(){
	  this.navCtrl.push(StTransaksiPage);
  }
  
  komisiList(){
	  this.navCtrl.push(KomisiUserPage);
  }
  
  perlu_kirim(){
	  this.navCtrl.push(PerluKirimPage);
  }
  
  sample_product(){
	  this.navCtrl.push(SampleProductPage);
  }
  
  totalSurveyData() {
		this.authService.getTotalLead().then((result) => {
			this.data = result;
			let cek_respon = this.data.result_status;
			
			if(cek_respon != '0'){
				this.total_survey =  this.data.result_data;
			} 
			this.greet = this.data.ucapan;
			
			//console.log(this.data.ucapan);
			
        }).catch((err) => {
            let toast = this.toastCtrl.create({
                message: err,
                duration: 2000,
                position: 'bottom'
            });
            toast.present();
        }) 
  }
  
  totalTransaksiData() {
		this.authService.getTotalTransaksi().then((result) => {
			this.data = result;
			let cek_respon = this.data.result_status;
			if(cek_respon != '0'){
				this.total_transaksi =  this.data.result_data;
			} 
        }).catch((err) => {
            let toast = this.toastCtrl.create({
                message: err,
                duration: 2000,
                position: 'bottom'
            });
            toast.present();
        }) 
   }


  totalAgendaOpen() {
		this.authService.getTotalAgendaOpen().then((result) => {
			this.data = result;
			let cek_respon = this.data.result_status;
			
			if(cek_respon != '0'){
				this.total_agenda_open =  this.data.result_data;
			} 
			
        }).catch((err) => {
            let toast = this.toastCtrl.create({
                message: err,
                duration: 2000,
                position: 'bottom'
            });
            toast.present();
        }) 
  }

  totalLeadOpen() {
		this.authService.getTotalLeadOpen().then((result) => {
			this.data = result;
			let cek_respon = this.data.result_status;
			
			if(cek_respon != '0'){
				this.total_lead_open =  this.data.result_data;
			} 
			
        }).catch((err) => {
            let toast = this.toastCtrl.create({
                message: err,
                duration: 2000,
                position: 'bottom'
            });
            toast.present();
        }) 
  }

  presentPopover(myEvent) {
    let popover = this.popoverCtrl.create(PopoverPage);
    popover.present({
      ev: myEvent
    });
  }
  
  productList() {
    this.navCtrl.push(ProductPage);
  }
  
  surveyList() {
	if(this.todo.providerUser == 'Agent'){
		this.navCtrl.push(SurveyPage);
	} 
	else if(this.todo.providerUser == 'Sales'){
		this.navCtrl.push(SurveyPage);
	}
	else {
		//console.log("cannot acess");
		this.presentAlert('Anda Tidak Bisa Mengakses Module Ini');
	}
  }
  
  performaList(){
	  this.navCtrl.push(PerformaPage);
  }
  
  agendaList(){
	  this.navCtrl.push(AgendaPage);
  }
  
  leadList(){
	  this.navCtrl.push(LeadPage);
  }
  
  approvalList(){
	  this.navCtrl.push(ApprovalAgentPage);
  }
  
  presentAlert(alt) {
    let alert = this.alertCtrl.create({
      title: 'Peringatan',
      message: alt,
      buttons: [ {
          text: 'Ok'
        }]
    });

    alert.present();
  }
  
  notificationList(){
	  this.navCtrl.push(NotificationsPage);
  }
  
  doRefresh(refresher) {
		//console.log('Begin async operation', refresher);
        this.navCtrl.setRoot(this.navCtrl.getActive().component);
		setTimeout(() => {
		  //console.log('Async operation has ended');
		  refresher.complete();
		}, 2000);
	  }
	  

}
