import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, LoadingController, ToastController, AlertController } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service/auth-service';
import { AgendaPage } from '../../pages/agenda/agenda';

/**
 * Generated class for the AgendaDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-agenda-detail',
  templateUrl: 'agenda-detail.html',
})
export class AgendaDetailPage {
	
  regData = { status_folowup:'', agenda_id:''}	
  public detail_item: any; 
  todo:any = {};
  data:any = {};
  loading: any;	

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public authService: AuthService, public loadingCtrl: LoadingController, private toastCtrl: ToastController, public alertCtrl: AlertController) {
	this.todo.userId   = localStorage.getItem('agent_id');
	this.todo.agendaId = navParams.get('agendaId');
	this.regData.agenda_id = navParams.get('agendaId');
	this.dataDetail();
  }
  
  dataDetail() {
		this.showLoader();
		this.authService.getDetailAgenda(this.todo).then((result) => {
			this.data = result;
			let cek_respon = this.data.result_status;
			if(cek_respon != '0'){
				this.detail_item = this.data.result_data;
				//console.log(this.detail_item);
			} 
			this.loading.dismiss();             
        }).catch((err) => {
			this.loading.dismiss();
            let toast = this.toastCtrl.create({
                message: err,
                duration: 2000,
                position: 'bottom'
            });
            toast.present();
        }) 
   }
   
   simpan_detail(){
		this.showLoader();
		this.authService.save_detail_agenda(this.regData).then((result) => {
		  this.data = result;
		  let cek_respon = this.data.sts_data;
		  
		  if(cek_respon =='Sucess'){
			  this.navCtrl.setRoot(AgendaPage);
		  }
		  
		  this.loading.dismiss();
		  let toast = this.toastCtrl.create({
			message: this.data.message,
			duration: 3000,
			position: "bottom"
		  });
		  toast.present();
		
		  //this.navCtrl.pop();
		}).catch((err) => {
			this.loading.dismiss();
			let toast = this.toastCtrl.create({
				message: err,
				duration: 2000,
				position: 'bottom'
			});
			toast.present();
		})  
		
		//console.log(this.regData);
   }
   
   showLoader(){
		this.loading = this.loadingCtrl.create({
			content: 'Loading...'
		});

		this.loading.present();
	}

    presentToast(msg) {
		let toast = this.toastCtrl.create({
			  message: msg,
			  duration: 3000,
			  position: 'bottom',
			  dismissOnPageChange: true
		});

		toast.onDidDismiss(() => {
		  console.log('Dismissed toast');
		});

		toast.present();
    }
	
	goBack(){
	  this.navCtrl.push(AgendaPage);
    }

 

}
