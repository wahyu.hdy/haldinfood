import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController, ModalController } from 'ionic-angular';
//import { Product } from '../../entities/product';
import { OrdersPage } from '../orders/orders';
import { AuthService } from '../../providers/auth-service/auth-service';
import { CartModalPage } from '../cart-modal/cart-modal';
import { BehaviorSubject } from 'rxjs';
import { DetailProductPage } from '../../pages/detail-product/detail-product';
import { HomePage } from '../../pages/home/home';

/**
 * Generated class for the ProductPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-product',
  templateUrl: 'product.html',
})
export class ProductPage {

  //public produtos: Product[] = [];
  
  public data_item: any;  
  
  todo:any = {};
  data:any = {};
  loading: any;
  
  cart = [];
  products = [];
  cartItemCount: BehaviorSubject<number>;
 
  @ViewChild('cart', {read: ElementRef})fab: ElementRef;
   
    constructor(public navCtrl: NavController, public navParams: NavParams, public authService: AuthService, public loadingCtrl: LoadingController, private toastCtrl: ToastController, public modalCtrl: ModalController) {
		this.todo.userId = localStorage.getItem('agent_id');
		this.todo.price_type_user = localStorage.getItem('price_type_user');
		this.dataProduct();
    }
	
	ngOnInit() {
		this.cart = this.authService.getCart();
		this.cartItemCount = this.authService.getCartItemCount();
	}
	

    dataProduct() {	
		this.showLoader();
		this.authService.getListProduct(this.todo).then((result) => {
			this.data = result;
			let cek_respon = this.data.result_status;
			//console.log(cek_respon);
			if(cek_respon != '0'){
				//console.log(JSON.stringify(this.data));
				this.data_item = this.data.result_data;
				
			} 
			this.loading.dismiss();
             
        }).catch((err) => {
			this.loading.dismiss();
            let toast = this.toastCtrl.create({
                message: err,
                duration: 2000,
                position: 'bottom'
            });
            toast.present();
        })
	}
	
	detail_product(product_id){
		/* const productDetailModal = this.modalCtrl.create(DetailProductPage, { productId: product_id });
		productDetailModal.onDidDismiss(data => {
		});
		productDetailModal.present(); */
		
		this.navCtrl.push(DetailProductPage, {'productId': product_id})
	}
  
	finishOrder() {
		this.navCtrl.push(OrdersPage);
	}
	
	addToCart(product) {
		this.authService.addProduct(product);
		//this.animateCSS('tada');
		
		//console.log(product);
	}
	 
	async openCart() {
		this.navCtrl.push(CartModalPage);
	}
	 
	/* animateCSS(animationName, keepAnimated = false) {
		const node = this.fab.nativeElement;
		node.classList.add('animated', animationName)
		
		function handleAnimationEnd() {
		  if (!keepAnimated) {
			node.classList.remove('animated', animationName);
		  }
		  node.removeEventListener('animationend', handleAnimationEnd)
		}
		node.addEventListener('animationend', handleAnimationEnd)
	} */
	
	showLoader(){
		this.loading = this.loadingCtrl.create({
			content: 'Loading...'
		});

		this.loading.present();
	}

    presentToast(msg) {
		let toast = this.toastCtrl.create({
			  message: msg,
			  duration: 3000,
			  position: 'bottom',
			  dismissOnPageChange: true
		});

		toast.onDidDismiss(() => {
		  console.log('Dismissed toast');
		});

		toast.present();
    }
	
	goBack(){
		 this.navCtrl.push(HomePage);
	}
	
	doRefresh(refresher) {
		//console.log('Begin async operation', refresher);
        this.navCtrl.setRoot(this.navCtrl.getActive().component);
		setTimeout(() => {
		  //console.log('Async operation has ended');
		  refresher.complete();
		}, 2000);
	  }

}
