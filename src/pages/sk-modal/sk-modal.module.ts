import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SkModalPage } from './sk-modal';

@NgModule({
  declarations: [
    SkModalPage,
  ],
  imports: [
    IonicPageModule.forChild(SkModalPage),
  ],
})
export class SkModalPageModule {}
