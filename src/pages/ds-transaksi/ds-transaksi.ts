import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, Platform } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service/auth-service';
import { DsDetailTransaksiPage } from '../ds-detail-transaksi/ds-detail-transaksi';

/**
 * Generated class for the DsTransaksiPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-ds-transaksi',
  templateUrl: 'ds-transaksi.html',
})
export class DsTransaksiPage {
	
  data:any = {};  
  todo:any = {};
  public data_item: any;	
  tab1Root = DsDetailTransaksiPage;
  tab2Root = DsDetailTransaksiPage;
  tab3Root = DsDetailTransaksiPage;
  


  constructor(public navCtrl: NavController, public navParams: NavParams, public authService: AuthService, private toastCtrl: ToastController, public plt: Platform) {
	   this.todo.userId         = localStorage.getItem('agent_id');
	   this.todo.providerUser   = localStorage.getItem('provider_user');
	   this.OnLoadTransaksi();
  }

   
   OnLoadTransaksi(){
	   this.authService.getDashTransaksi(this.todo).then((result) => {
			this.data = result;
			let cek_respon = this.data.result_status;
			if(cek_respon != '0'){
				this.data_item =  this.data.result_data;
			} 
			
			
        }).catch((err) => {
            let toast = this.toastCtrl.create({
                message: err,
                duration: 2000,
                position: 'bottom'
            });
            toast.present();
        }) 
   }
   
   detail_dash_trans(order_id){
	   this.navCtrl.push(DsDetailTransaksiPage, {'order_id': order_id});
   }
  
   /*copyInputMessage(inputElement){
    inputElement.select();
    document.execCommand('copy');
    inputElement.setSelectionRange(0, 0);
  }*/

}
