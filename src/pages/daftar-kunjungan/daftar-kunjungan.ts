import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service/auth-service';

/**
 * Generated class for the DaftarKunjunganPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-daftar-kunjungan',
  templateUrl: 'daftar-kunjungan.html',
})
export class DaftarKunjunganPage {
	
   todo:any = {};
    data:any = {};
   public data_item: any; 
 loading: any;   

  constructor(public navCtrl: NavController, public navParams: NavParams, public authService: AuthService, public loadingCtrl: LoadingController, private toastCtrl: ToastController) {
	 this.todo.userId   = localStorage.getItem('agent_id');
	 this.dataKunjungan();
  }

  /* ionViewDidLoad() {
    console.log('ionViewDidLoad DaftarKunjunganPage');
  } */
     
	dataKunjungan() {	
		this.showLoader();
		this.authService.getDataKunjungan(this.todo).then((result) => {
			this.data = result;
			let cek_respon = this.data.result_status;
			console.log(cek_respon);
			if(cek_respon != '0'){
				//console.log(JSON.stringify(this.data));
				this.data_item = this.data.result_data;
			} 
			this.loading.dismiss();             
        }).catch((err) => {
			this.loading.dismiss();
            let toast = this.toastCtrl.create({
                message: err,
                duration: 2000,
                position: 'bottom'
            });
            toast.present();
        })
	}
	
  
    showLoader(){
		this.loading = this.loadingCtrl.create({
			content: 'Loading...'
		});

		this.loading.present();
	}

    presentToast(msg) {
		let toast = this.toastCtrl.create({
			  message: msg,
			  duration: 3000,
			  position: 'bottom',
			  dismissOnPageChange: true
		});

		toast.onDidDismiss(() => {
		  console.log('Dismissed toast');
		});

		toast.present();
    }

}
