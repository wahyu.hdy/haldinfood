import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController, ModalController } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service/auth-service';
import { DetailPerformaPage } from '../detail-performa/detail-performa';
import { HomePage } from '../home/home';

/**
 * Generated class for the PerformaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-performa',
  templateUrl: 'performa.html',
})
export class PerformaPage {
	
  public data_item: any;	
  public total_performa_user: number;
  
  todo:any = {};
  data:any = {};
  loading: any;	
  
 

  constructor(public navCtrl: NavController, public navParams: NavParams, public authService: AuthService, public loadingCtrl: LoadingController, private toastCtrl: ToastController, public modalCtrl: ModalController) {
	this.todo.userId   = localStorage.getItem('agent_id');
	this.todo.providerUser   = localStorage.getItem('provider_user');
	this.dataPerforma();
	//console.log(localStorage.getItem('provider_user'));
  }
  
  dataPerforma() {	
		this.showLoader();
		this.authService.getListPerforma(this.todo).then((result) => {
			this.data = result;
			let cek_respon = this.data.result_status;
			//console.log(cek_respon);
			if(cek_respon != '0'){
				//console.log(JSON.stringify(this.data));
				this.data_item = this.data.result_data;
				this.total_performa_user = this.data.result_total;
			} 
			this.loading.dismiss();
            /* let toast = this.toastCtrl.create({
                message: this.data.message,
                duration: 3000,
                position: "bottom"
            });
            toast.present(); */
             
        }).catch((err) => {
			this.loading.dismiss();
            let toast = this.toastCtrl.create({
                message: err,
                duration: 2000,
                position: 'bottom'
            });
            toast.present();
        })
	}
	
	detail_performa(id){
		const profileModal = this.modalCtrl.create(DetailPerformaPage, { detailId: id });
		profileModal.onDidDismiss(data => {
		  //console.log(data);
		   //this.madalDismissData = JSON.stringify(data);
		});
		profileModal.present();
	}
	
	

   showLoader(){
		this.loading = this.loadingCtrl.create({
			content: 'Loading...'
		});

		this.loading.present();
	}

    presentToast(msg) {
		let toast = this.toastCtrl.create({
			  message: msg,
			  duration: 3000,
			  position: 'bottom',
			  dismissOnPageChange: true
		});

		toast.onDidDismiss(() => {
		  console.log('Dismissed toast');
		});

		toast.present();
    }
	
	 goBack(){
		  this.navCtrl.push(HomePage);
	  }

}
