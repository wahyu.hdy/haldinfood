import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController, ModalController, AlertController } from 'ionic-angular';
import { AddSurveyPage } from '../add-survey/add-survey';
import { AuthService } from '../../providers/auth-service/auth-service';
import { DetailOutletPage } from '../../pages/detail-outlet/detail-outlet';
import { Geolocation } from '@ionic-native/geolocation';
import { DaftarKunjunganPage } from '../../pages/daftar-kunjungan/daftar-kunjungan';
import { HomePage } from '../../pages/home/home';

/**
 * Generated class for the SurveyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-survey',
  templateUrl: 'survey.html',
})
export class SurveyPage {
  
  public data_item: any;
  public total_survey: string;
  public total_kunjungan: string;
  
  todo:any = {};
  data:any = {};
  loading: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public authService: AuthService, public loadingCtrl: LoadingController, private toastCtrl: ToastController, public modalCtrl: ModalController, private alertCtrl: AlertController, public geolocation: Geolocation) {
	 this.todo.userId   = localStorage.getItem('agent_id');
	 this.dataSurvey();
	 this.totalSurveyData();
	 this.totalKunjunganData();
  }
  
	totalSurveyData() {
		this.authService.getTotalSurvey().then((result) => {
			this.data = result;
			let cek_respon = this.data.result_status;
			if(cek_respon != '0'){
				this.total_survey =  this.data.result_data;
			} 
        }).catch((err) => {
            let toast = this.toastCtrl.create({
                message: err,
                duration: 2000,
                position: 'bottom'
            });
            toast.present();
        }) 
    }
	
	totalKunjunganData() {
		this.authService.getTotalKunjungan().then((result) => {
			this.data = result;
			let cek_respon = this.data.result_status;
			if(cek_respon != '0'){
				this.total_kunjungan =  this.data.result_data;
			} 
        }).catch((err) => {
            let toast = this.toastCtrl.create({
                message: err,
                duration: 2000,
                position: 'bottom'
            });
            toast.present();
        }) 
    }
  
 
    dataSurvey() {	
		this.showLoader();
		this.authService.getListSurvey(this.todo).then((result) => {
			this.data = result;
			let cek_respon = this.data.result_status;
			//console.log(cek_respon);
			if(cek_respon != '0'){
				//console.log(JSON.stringify(this.data));
				this.data_item = this.data.result_data;
			} 
			this.loading.dismiss();
            /* let toast = this.toastCtrl.create({
                message: this.data.message,
                duration: 3000,
                position: "bottom"
            });
            toast.present(); */
			
			this.getLocation();
             
        }).catch((err) => {
			this.loading.dismiss();
            let toast = this.toastCtrl.create({
                message: err,
                duration: 2000,
                position: 'bottom'
            });
            toast.present();
        })
	}
	
	detail_outlet(survey_id){
		const profileModal = this.modalCtrl.create(DetailOutletPage, { surveyId: survey_id });
		profileModal.onDidDismiss(data => {
		  //console.log(data);
		   //this.madalDismissData = JSON.stringify(data);
		});
		profileModal.present();
	}
 
    survey_outlet() {
		this.navCtrl.push(AddSurveyPage);
    }
  
    showLoader(){
		this.loading = this.loadingCtrl.create({
			content: 'Loading...'
		});

		this.loading.present();
	}

    presentToast(msg) {
		let toast = this.toastCtrl.create({
			  message: msg,
			  duration: 3000,
			  position: 'bottom',
			  dismissOnPageChange: true
		});

		toast.onDidDismiss(() => {
		  console.log('Dismissed toast');
		});

		toast.present();
    }
	
	tandaiKunjungan(survey_id){
		let prompt = this.alertCtrl.create({
		  title: 'Tandai Kunjungan',
		  message: "Berikan Catatan Pada Kunjungan Ini :",
		  inputs: [
			{
			  name: 'note',
			  placeholder: 'note',
			},{
			  name: 'survey_id',
			  type: 'hidden',
			  value: survey_id
			},
		  ],
		  buttons: [
			{
			  text: 'Cancel',
			  handler: data => {
				console.log('Cancel clicked');
			  }
			},
			{
			  text: 'Save',
			  handler: data => {
				//console.log(data.note);
				//document.getElementById("isi").innerHTML = data.password;
				this.simpanTandaKunjungan(data.survey_id, data.note);
			  }
			}
		  ]
		});
		prompt.present();
	}
	
	simpanTandaKunjungan(survey_id, note) {
		this.todo.userId          = localStorage.getItem('agent_id');
		this.todo.survey_id_map   = survey_id;
		this.todo.note            = note;
		
		this.showLoader();
		this.authService.saveTandaiKunjungan(this.todo).then((result) => {
		this.data = result;
		  //let cek_respon = this.data.message
		  
		  this.loading.dismiss();
		  let toast = this.toastCtrl.create({
			message: this.data.message,
			duration: 3000,
			position: "bottom"
		  });
		  toast.present();
		
		  //this.navCtrl.pop();
		  //this.doRefresh();
		 
		}).catch((err) => {
			this.loading.dismiss();
			let toast = this.toastCtrl.create({
				message: err,
				duration: 2000,
				position: 'bottom'
			});
			toast.present();
		})  
		
		//console.log(this.regData);
		
    }
	
	
	getLocation(){
		this.geolocation.getCurrentPosition().then((res) => {
		  this.todo.location_lat  = res.coords.latitude;
		  this.todo.location_long = res.coords.longitude;
          //console.log(this.regData.location_lat);
		}).catch((error) => {
		console.log('Error getting location', error);
		}); 
	}
	
	daftarKunjungan(){
		this.navCtrl.push(DaftarKunjunganPage);
	}
	
	/* doRefresh(refresher) {
		//console.log('Begin async operation', refresher);
        this.navCtrl.setRoot(this.navCtrl.getActive().component);
		
		setTimeout(() => {
		  //console.log('Async operation has ended');
		  //refresher.complete();	         		  
		}, 2000);
	  } */
	  
	backPage(){
		this.navCtrl.push(HomePage);
	} 
  
	
}
