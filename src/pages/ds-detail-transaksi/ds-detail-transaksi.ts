import { Component } from '@angular/core';
import { NavController, NavParams, ToastController } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service/auth-service';
import { InAppBrowser  } from '@ionic-native/in-app-browser/ngx';

/**
 * Generated class for the DsDetailTransaksiPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-ds-detail-transaksi',
  templateUrl: 'ds-detail-transaksi.html',
})
export class DsDetailTransaksiPage {
	
   todo:any = {};
   data:any = {};
   public data_item: any;
   public data_cost: number;   
   total_sum:number;

  constructor(public navCtrl: NavController, public navParams: NavParams, public authService: AuthService, private toastCtrl: ToastController, private iab: InAppBrowser) {
	  this.todo.order_id = navParams.get('order_id');
	  this.OnLoadDetailTransaksi();
  }

  OnLoadDetailTransaksi(){
	   this.authService.getDashDetailTransaksi(this.todo).then((result) => {
			this.data = result;
			let cek_respon = this.data.result_status;
			if(cek_respon != '0'){
				this.data_item =  this.data.result_data;
				this.data_cost = this.data.cost;
				this.getTotal(this.data_item, this.data.cost);
			} 
			
			
        }).catch((err) => {
            let toast = this.toastCtrl.create({
                message: err,
                duration: 2000,
                position: 'bottom'
            });
            toast.present();
        }) 
   }
   
    getTotal(data_items, cost) {
		 let sum = 0;
		for (let carts of data_items) {
		  sum = sum + (carts.price*carts.quantity);
		}
		
		this.total_sum = sum + parseInt(cost);
		//console.log(this.total_sum);
    }

    opendata(){
      const url = 'https://mytalasi.com/assets/upload/invoice/01022021_4503056574.pdf';
      this.iab.create(url, '_system', 'location=yes');
    }

}
