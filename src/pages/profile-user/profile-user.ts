import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { HomePage } from '../../pages/home/home';
import { AuthService } from '../../providers/auth-service/auth-service';
import { Camera } from '@ionic-native/camera';

/**
 * Generated class for the ProfileUserPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-profile-user',
  templateUrl: 'profile-user.html',
})
export class ProfileUserPage {
	
  todo:any = {};
  data:any = {};
  public data_item: any;
  public data_address: any;
  public data_agent: any;
  
  public img:String;
  public cameraSupported:boolean;
  public photos: any;
  public base64Image: string;
  public myImageProfile:string;
  
  regData = { imageOriProfile:'', agentId:'' }
  
  loading: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public authService: AuthService, private toastCtrl: ToastController, private camera: Camera, public loadingCtrl: LoadingController) {
	   this.todo.userId  = localStorage.getItem('agent_id');
	   this.onLoadProfile();
	   this.onLoadAddress();
	   this.myImageProfile = "assets/imgs/photo-camera.png";
	   //this.regData.imageOriProfile = '';
	   this.regData.agentId  = localStorage.getItem('agent_id');
	   this.todo.providerUser   = localStorage.getItem('provider_user');
  }
  
    takePhotoProfile(){
		this.camera.getPicture({
			destinationType: this.camera.DestinationType.DATA_URL,
			targetWidth: 1000,
			targetHeight: 1000
		}).then((imageDataProfile) => {
			this.myImageProfile = "data:image/jpeg;base64," + imageDataProfile;
			this.regData.imageOriProfile = imageDataProfile;
		}, (err) => {
			console.log(err);
		}); 
		
    }
	
	 doUploadFoto() {
		console.log(this.regData);
		this.showLoader();
		this.authService.uploadFotoProfile(this.regData).then((result) => {
		  this.data = result;
		  let cek_respon = this.data.data_status;
		  
		  if(cek_respon =='Sucess'){
			  this.navCtrl.push(HomePage);
		  }
		  
		   this.loading.dismiss();
		  let toast = this.toastCtrl.create({
			message: this.data.message,
			duration: 3000,
			position: "bottom"
		  });
		  toast.present();
		
		  //this.navCtrl.pop();
		}).catch((err) => {
			this.loading.dismiss();
			let toast = this.toastCtrl.create({
				message: err,
				duration: 2000,
				position: 'bottom'
			});
			toast.present();
		}) 
    }

  
  onLoadProfile() {
		this.authService.getLoadProfile(this.todo).then((result) => {
			this.data = result;
			let cek_respon = this.data.result_status;
			if(cek_respon != '0'){
				this.data_item  = this.data.result_data;
				this.data_agent = this.data.result_agent;
			} 
        }).catch((err) => {
            let toast = this.toastCtrl.create({
                message: err,
                duration: 2000,
                position: 'bottom'
            });
            toast.present();
        }) 
  }
  
  onLoadAddress() {
		this.authService.getLoadProfileAdsress(this.todo).then((result) => {
			this.data = result;
			let cek_respon = this.data.result_status;
			if(cek_respon != '0'){
				this.data_address = this.data.result_data;
			} 
        }).catch((err) => {
            let toast = this.toastCtrl.create({
                message: err,
                duration: 2000,
                position: 'bottom'
            });
            toast.present();
        }) 
  }

  goBack(){
	 this.navCtrl.push(HomePage);
  }
  
  showLoader(){
		this.loading = this.loadingCtrl.create({
			content: 'Loading...'
		});

		this.loading.present();
	}

    presentToast(msg) {
		let toast = this.toastCtrl.create({
			  message: msg,
			  duration: 3000,
			  position: 'bottom',
			  dismissOnPageChange: true
		});

		toast.onDidDismiss(() => {
		  console.log('Dismissed toast');
		});

		toast.present();
    }

}
