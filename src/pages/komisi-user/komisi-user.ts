import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController, ModalController , ViewController  } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service/auth-service';
import { KomisiDetailPage } from '../komisi-detail/komisi-detail';

/**
 * Generated class for the KomisiUserPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-komisi-user',
  templateUrl: 'komisi-user.html',
})
export class KomisiUserPage {
	
  regData = { bulanSelected:'', tahun:''};
   todo:any = {};
  data: any;
  loading: any;	
  public data_komisi: any; 
  public data_bank: any; 

  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, private toastCtrl: ToastController, public authService: AuthService, public modalCtrl: ModalController, public viewCtrl : ViewController) {
		this.todo.userId   = localStorage.getItem('agent_id');
	    this.todo.providerUser   = localStorage.getItem('provider_user');
		this.todo.kodeAgent   = localStorage.getItem('kode_agent');
		this.getBankAgent();
  }
  
  getBankAgent(){
	    let params = { 
		  agent_id: this.todo.userId
		};
		
		this.showLoader();
		this.authService.getBankAgent(params).then((result) => {
			this.data = result;
			let cek_respon = this.data.result_status;
			if(cek_respon != '0'){
				this.data_bank =  this.data.result_data;
			} 
			this.loading.dismiss();
        }).catch((err) => {
			this.loading.dismiss();
            let toast = this.toastCtrl.create({
                message: err,
                duration: 2000,
                position: 'bottom'
            });
            toast.present();
        }) 
  }

  getKomisi() {
		 let params = { 
		  tahun_set: this.regData.tahun,
		  bulan_set: this.regData.bulanSelected,
		  kode_agent_set : this.todo.kodeAgent
		};
		
		this.showLoader();
		this.authService.getKomisiAgent(params).then((result) => {
			this.data = result;
			let cek_respon = this.data.result_status;
			if(cek_respon != '0'){
				this.data_komisi =  this.data.result_data;
			} 
			this.loading.dismiss();
        }).catch((err) => {
			this.loading.dismiss();
            let toast = this.toastCtrl.create({
                message: err,
                duration: 2000,
                position: 'bottom'
            });
            toast.present();
        }) 
   }
   
   detail_komisi(xdate){
		const profileModal = this.modalCtrl.create(KomisiDetailPage, { detailTgl: xdate });
		profileModal.onDidDismiss(data => {
		  //console.log(data);
		   //this.madalDismissData = JSON.stringify(data);
		});
		profileModal.present();
   }
   
   public closeModal(){
		this.viewCtrl.dismiss();
	}
   
   showLoader(){
		this.loading = this.loadingCtrl.create({
			content: 'Loading...'
		});

		this.loading.present();
	}

    presentToast(msg) {
		let toast = this.toastCtrl.create({
			  message: msg,
			  duration: 3000,
			  position: 'bottom',
			  dismissOnPageChange: true
		});

		toast.onDidDismiss(() => {
		  console.log('Dismissed toast');
		});

		toast.present();
    }

}
