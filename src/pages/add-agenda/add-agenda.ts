import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, LoadingController, ToastController } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service/auth-service';
import { AgendaPage } from '../agenda/agenda';

/**
 * Generated class for the AddAgendaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-add-agenda',
  templateUrl: 'add-agenda.html',
})
export class AddAgendaPage {
  loading: any;
  todo:any = {};
  data: any;	
	
  regData = { title: "", message: "", startDate: "", endDate: "", jam:"" };	

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public authService: AuthService, public loadingCtrl: LoadingController, private toastCtrl: ToastController) {
  }
  
   save_agenda() {
		this.todo.userId = localStorage.getItem('agent_id');
		
		this.showLoader();
		this.authService.save_data_agenda(this.regData).then((result) => {
		  this.data = result;
		  let cek_respon = this.data.sts_data;
		  
		  if(cek_respon =='Sucess'){
			  this.navCtrl.setRoot(AgendaPage);
		  }
		  
		  this.loading.dismiss();
		  let toast = this.toastCtrl.create({
			message: this.data.message,
			duration: 3000,
			position: "bottom"
		  });
		  toast.present();
		
		  //this.navCtrl.pop();
		}).catch((err) => {
			this.loading.dismiss();
			let toast = this.toastCtrl.create({
				message: err,
				duration: 2000,
				position: 'bottom'
			});
			toast.present();
		})  
		
		//console.log(this.regData);
    }
  
  
   dismissModal() {
    //let data = { 'foo': 'bar' };
    this.viewCtrl.dismiss();
  }
  
  showLoader(){
    this.loading = this.loadingCtrl.create({
        content: 'Save Data...'
    });

    this.loading.present();
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom',
      dismissOnPageChange: true
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }


}
