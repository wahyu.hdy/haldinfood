import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service/auth-service';
//import { HomePage } from '../home/home';
import { SurveyPage } from '../survey/survey';
import { LoginPage } from '../authentication/login/login';

/**
 * Generated class for the AktifasiOutletPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-aktifasi-outlet',
  templateUrl: 'aktifasi-outlet.html',
})
export class AktifasiOutletPage {
   loading: any;
    regData = { kode:''};
	todo:any = {};
	data: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public authService: AuthService, public loadingCtrl: LoadingController, private toastCtrl: ToastController) {
  }

  /* ionViewDidLoad() {
    console.log('ionViewDidLoad AktifasiOutletPage');
  } */
  
  kirimVerifikasi(){
	this.showLoader();
    this.authService.aktivasi(this.regData).then((result) => {
	  this.data = result;
	  let cek_respon = this.data.message;
	  
	  if(cek_respon =='Sucess'){
		  this.navCtrl.setRoot(LoginPage);
	  }
	  
      this.loading.dismiss();
	  let toast = this.toastCtrl.create({
		message: this.data.message,
		duration: 3000,
		position: "bottom"
	  });
	  toast.present();
	
      //this.navCtrl.pop();
    }).catch((err) => {
		this.loading.dismiss();
		let toast = this.toastCtrl.create({
			message: 'Kode Verifikasi Tidak Ditemukan',
			duration: 2000,
			position: 'bottom'
		});
		toast.present();
	})
  }
  
  showLoader(){
    this.loading = this.loadingCtrl.create({
        content: 'Authenticating...'
    });

    this.loading.present();
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom',
      dismissOnPageChange: true
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }
  
  backHome(){
	  this.navCtrl.setRoot(SurveyPage);
  }

}
