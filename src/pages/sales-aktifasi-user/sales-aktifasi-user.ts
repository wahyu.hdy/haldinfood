import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController, ModalController } from 'ionic-angular';
import { HomePage } from '../../pages/home/home';
import { AuthService } from '../../providers/auth-service/auth-service';

/**
 * Generated class for the SalesAktifasiUserPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-sales-aktifasi-user',
  templateUrl: 'sales-aktifasi-user.html',
})
export class SalesAktifasiUserPage {
	
  loading: any;
   todo:any = {};
   data:any = {};
   
   public data_item: any;  

  constructor(public navCtrl: NavController, public navParams: NavParams, public authService: AuthService, public loadingCtrl: LoadingController, private toastCtrl: ToastController, public modalCtrl: ModalController) {
	 this.todo.userId  = localStorage.getItem('agent_id');
	 this.dataAktifasiUser();
  }

     dataAktifasiUser() {	
		this.showLoader();
		this.authService.getListAktifasiUser(this.todo).then((result) => {
			this.data = result;
			let cek_respon = this.data.result_status;
			//console.log(cek_respon);
			if(cek_respon != '0'){
				//console.log(JSON.stringify(this.data));
				this.data_item = this.data.result_data;
				
			} 
			this.loading.dismiss();
             
        }).catch((err) => {
			this.loading.dismiss();
            let toast = this.toastCtrl.create({
                message: err,
                duration: 2000,
                position: 'bottom'
            });
            toast.present();
        })
	}
	
	detail_aktifasi(code){
		this.showLoader();
		this.authService.getAktifasiUserCode(code).then((result) => {
			this.data = result;
			let cek_respon = this.data.result_status;
			//console.log(cek_respon);
			if(cek_respon != '0'){
				//console.log(JSON.stringify(this.data));
				this.navCtrl.push(HomePage);
			} 
			this.loading.dismiss();
             
        }).catch((err) => {
			this.loading.dismiss();
            let toast = this.toastCtrl.create({
                message: err,
                duration: 2000,
                position: 'bottom'
            });
            toast.present();
        })
	}
	
	goBack(){
		 this.navCtrl.push(HomePage);
	}
	
	
	showLoader(){
		this.loading = this.loadingCtrl.create({
			content: 'Loading...'
		});

		this.loading.present();
	}

    presentToast(msg) {
		let toast = this.toastCtrl.create({
			  message: msg,
			  duration: 3000,
			  position: 'bottom',
			  dismissOnPageChange: true
		});

		toast.onDidDismiss(() => {
		  console.log('Dismissed toast');
		});

		toast.present();
    }


}
