import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, LoadingController, ToastController } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service/auth-service';

/**
 * Generated class for the DetailOutletPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-detail-outlet',
  templateUrl: 'detail-outlet.html',
})
export class DetailOutletPage {
	
	public detail_item: any; 
	todo:any = {};
	data:any = {};
	loading: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public authService: AuthService, public loadingCtrl: LoadingController, private toastCtrl: ToastController) {
	this.todo.userId   = localStorage.getItem('agent_id');
	this.todo.surveyId = navParams.get('surveyId');
	this.dataDetail();
  }
  
    dataDetail() {
		//this.detail_item = this.todo.surveyId;
		//console.log(this.todo.surveyId);
		this.showLoader();
		this.authService.getDetailSurvey(this.todo).then((result) => {
			this.data = result;
			let cek_respon = this.data.result_status;
			if(cek_respon != '0'){
				this.detail_item = this.data.result_data;
				//console.log(this.detail_item);
			} 
			this.loading.dismiss();             
        }).catch((err) => {
			this.loading.dismiss();
            let toast = this.toastCtrl.create({
                message: err,
                duration: 2000,
                position: 'bottom'
            });
            toast.present();
        }) 
    }
	
	showLoader(){
		this.loading = this.loadingCtrl.create({
			content: 'Loading...'
		});

		this.loading.present();
	}

    presentToast(msg) {
		let toast = this.toastCtrl.create({
			  message: msg,
			  duration: 3000,
			  position: 'bottom',
			  dismissOnPageChange: true
		});

		toast.onDidDismiss(() => {
		  console.log('Dismissed toast');
		});

		toast.present();
    }

 
  
   dismissModal() {
    //let data = { 'foo': 'bar' };
    this.viewCtrl.dismiss();
  }

}
