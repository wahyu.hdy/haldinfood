import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, LoadingController, ToastController } from 'ionic-angular';
import { ApprovalAgentPage } from '../../pages/approval-agent/approval-agent';
import { AuthService } from '../../providers/auth-service/auth-service';

/**
 * Generated class for the DetailApprovalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-detail-approval',
  templateUrl: 'detail-approval.html',
})
export class DetailApprovalPage {
	
  public detail_item: any; 
  todo:any = {};
  data:any = {};
  loading: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public authService: AuthService, public loadingCtrl: LoadingController, private toastCtrl: ToastController) {
	this.todo.userId      = localStorage.getItem('agent_id');
	this.todo.approval_id = navParams.get('approval_id');
	this.approvalDetail();
  }
  
    approvalDetail() {
		//this.detail_item = this.todo.surveyId;
		//console.log(this.todo.surveyId);
		this.showLoader();
		this.authService.getDetailApproval(this.todo).then((result) => {
			this.data = result;
			let cek_respon = this.data.result_status;
			if(cek_respon != '0'){
				this.detail_item = this.data.result_data;
				//console.log(this.detail_item);
			} 
			
			this.loading.dismiss();       
        }).catch((err) => {
			this.loading.dismiss();
            let toast = this.toastCtrl.create({
                message: err,
                duration: 2000,
                position: 'bottom'
            });
            toast.present();
        }) 
    }
	
	approved(id) {
		//this.detail_item = this.todo.surveyId;
		//console.log(this.todo.surveyId);
		this.showLoader();
		this.authService.approvedHsp(id).then((result) => {
			this.data = result;
			let cek_respon = this.data.result_status;
			if(cek_respon != '0'){
				 this.navCtrl.push(ApprovalAgentPage);
			} 
			this.loading.dismiss(); 
             let toast = this.toastCtrl.create({
			message: this.data.message,
			duration: 3000,
			position: "bottom"
		  });
		  toast.present();      		  
        }).catch((err) => {
			this.loading.dismiss();
            let toast = this.toastCtrl.create({
                message: err,
                duration: 2000,
                position: 'bottom'
            });
            toast.present();
        }) 
    }
	
	rejected(id){
		this.showLoader();
		this.authService.rejectedHsp(id).then((result) => {
			this.data = result;
			let cek_respon = this.data.result_status;
			if(cek_respon != '0'){
				 this.navCtrl.push(ApprovalAgentPage);
			} 
			this.loading.dismiss(); 
             let toast = this.toastCtrl.create({
			message: this.data.message,
			duration: 3000,
			position: "bottom"
		  });
		  toast.present();      		  
        }).catch((err) => {
			this.loading.dismiss();
            let toast = this.toastCtrl.create({
                message: err,
                duration: 2000,
                position: 'bottom'
            });
            toast.present();
        }) 
	}
   
   showLoader(){
		this.loading = this.loadingCtrl.create({
			content: 'Loading...'
		});

		this.loading.present();
	}

    presentToast(msg) {
		let toast = this.toastCtrl.create({
			  message: msg,
			  duration: 3000,
			  position: 'bottom',
			  dismissOnPageChange: true
		});

		toast.onDidDismiss(() => {
		  console.log('Dismissed toast');
		});

		toast.present();
    }

    goBack(){
	  this.navCtrl.push(ApprovalAgentPage);
    }

}
