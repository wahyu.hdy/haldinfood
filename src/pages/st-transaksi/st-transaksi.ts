import { Component } from '@angular/core';
import { NavController, NavParams, ToastController } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service/auth-service';
import { DsDetailTransaksiPage } from '../ds-detail-transaksi/ds-detail-transaksi';
import { ProfileUserPage } from '../profile-user/profile-user';
import { BantuanPage } from '../bantuan/bantuan';
import { HomePage } from '../../pages/home/home';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

/**
 * Generated class for the StTransaksiPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-st-transaksi',
  templateUrl: 'st-transaksi.html',
})
export class StTransaksiPage {

  data:any = {};  
  todo:any = {};
  public data_item: any;	 
  public total_pending: string;	
  public total_sent: string;
  public total_kirim: string;
  public total_complete: string;
  //resiData = { noresi:''};
  
  constructor(public navCtrl: NavController, public navParams: NavParams, public authService: AuthService, private toastCtrl: ToastController, private iab: InAppBrowser) {
	   this.todo.userId         = localStorage.getItem('agent_id');
	   this.todo.providerUser   = localStorage.getItem('provider_user');
	   this.OnLoadStatusPending();
	   this.OnLoadStatusSent();
	   this.OnLoadStatusKirim();
	   this.OnLoadStatusComplete();
  }
  
  detail_dash_trans(order_id){
	   this.navCtrl.push(DsDetailTransaksiPage, {'order_id': order_id});
	   //console.log(order_id);
  }
   
  detail_status_transaksi(set){
	  this.todo.tipe_status = set;
	  this.authService.getDataStatusByTipe(this.todo).then((result) => {
			this.data = result;
			let cek_respon = this.data.result_status;
			if(cek_respon != '0'){
				this.data_item =  this.data.result_data;
			} 
			
			
        }).catch((err) => {
            let toast = this.toastCtrl.create({
                message: err,
                duration: 2000,
                position: 'bottom'
            });
            toast.present();
        }) 
  }
  
   selesaikan_pesanan(ido){
	  this.todo.od_id = ido;
	  this.authService.selesaikanPesanan(this.todo).then((result) => {
			this.data = result;
			let cek_respon = this.data.result_status;
			if(cek_respon != '0'){
				this.navCtrl.push(HomePage);
			} 
        }).catch((err) => {
            let toast = this.toastCtrl.create({
                message: err,
                duration: 2000,
                position: 'bottom'
            });
            toast.present();
        }) 
  }
  
  OnLoadStatusComplete(){
	  this.authService.getDashTotalTransaksiComplete(this.todo).then((result) => {
			this.data = result;
			let cek_respon = this.data.result_status;
			if(cek_respon != '0'){
				this.total_complete =  this.data.result_data;
			} 
			
			
        }).catch((err) => {
            let toast = this.toastCtrl.create({
                message: err,
                duration: 2000,
                position: 'bottom'
            });
            toast.present();
        }) 
  }
  
  OnLoadStatusKirim(){
	  this.authService.getDashTotalTransaksiKirim(this.todo).then((result) => {
			this.data = result;
			let cek_respon = this.data.result_status;
			if(cek_respon != '0'){
				this.total_kirim =  this.data.result_data;
			} 
			
			
        }).catch((err) => {
            let toast = this.toastCtrl.create({
                message: err,
                duration: 2000,
                position: 'bottom'
            });
            toast.present();
        }) 
  }
  
  OnLoadStatusPending(){
	  this.authService.getDashTotalTransaksiPending(this.todo).then((result) => {
			this.data = result;
			let cek_respon = this.data.result_status;
			if(cek_respon != '0'){
				this.total_pending =  this.data.result_data;
			} 
			
			
        }).catch((err) => {
            let toast = this.toastCtrl.create({
                message: err,
                duration: 2000,
                position: 'bottom'
            });
            toast.present();
        }) 
  }
  
  OnLoadStatusSent(){
	  this.authService.getDashTotalTransaksiSent(this.todo).then((result) => {
			this.data = result;
			let cek_respon = this.data.result_status;
			if(cek_respon != '0'){
				this.total_sent =  this.data.result_data;
			} 
			
			
        }).catch((err) => {
            let toast = this.toastCtrl.create({
                message: err,
                duration: 2000,
                position: 'bottom'
            });
            toast.present();
        }) 
  }
  
  myHome(){
	  this.navCtrl.push(HomePage);
  }
 
  myProfile(){
	  this.navCtrl.push(ProfileUserPage);
  }
  
  myHelp(){
	  this.navCtrl.push(BantuanPage);
  }
  
   openWebpage(bill) {
	   //console.log(this.resiData.noresi);
	    let url_data = 'https://cekresicepat.com/?v=w1&nomor_resi='+bill;
       /*  const options: InAppBrowserOptions = {
          zoom: 'no'
        } */

        // Opening a URL and returning an InAppBrowserObject
        this.iab.create(url_data, '_blank', 'location=yes'); 

       // Inject scripts, css and more with browser.X
    }
	
	 doRefresh(refresher) {
		//console.log('Begin async operation', refresher);
        this.navCtrl.setRoot(this.navCtrl.getActive().component);
		setTimeout(() => {
		  //console.log('Async operation has ended');
		  refresher.complete();
		}, 2000);
	  }
	  
	  goBack(){
		 this.navCtrl.push(HomePage);
	}
 

}
