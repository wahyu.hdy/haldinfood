import { Component } from '@angular/core';
import { NavController, NavParams, ModalController,  LoadingController, ToastController } from 'ionic-angular';
import { Camera } from '@ionic-native/camera';
import { AuthService } from '../../providers/auth-service/auth-service';
import { AktifasiOutletPage } from '../../pages/aktifasi-outlet/aktifasi-outlet';
import { Geolocation } from '@ionic-native/geolocation';

/**
 * Generated class for the AddSurveyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-add-survey',
  templateUrl: 'add-survey.html',
})
export class AddSurveyPage {
	
  loading: any;
 regData = { kategoriSelected:'',nama_outlet:'', agent_id:'', nib:'', alamat_outlet:'', nik:'', imageOri:'', imageOriOutlet:'', imageOriNpwp:'', gelar:'', nama:'', email:'', telepon:'', password:'', password2:'', location_lat:0, location_long:0, kode_agent:'', kode_sales:'', kode_pos:'', alamat:'', kode_customer:'', nomor_induk_kr:'' };

  public img:string;
  public cameraSupported:boolean;
  public photos: any;
  public base64Image: string;
  public imageURL: any;
  public myImageKtp:string;
  public myImageNpwp:string;
  public myImageOutlet:string;
  public kategori:string;
  inputDisabled: boolean = false;
  inputDisabledKar: boolean = false;
  inputDisabledOut: boolean = false;
  inputDisabledNip: boolean = false;
  buttonDisabled: boolean = false;
  showHsh: boolean = true;

  todo:any = {};
  data: any;
  public location_lat:any;
  public location_long:any;
  label_outlet:string;
  label_alamat:string;
  label_wajib:string;
  label_nama:string;
  public data_provinsi: any; 
  public data_city: any;  
  public data_kecamatan: any;  
  public data_kelurahan: any; 


  constructor(public navCtrl: NavController, public navParams: NavParams, private camera: Camera, public modalCtrl: ModalController, public loadingCtrl: LoadingController, private toastCtrl: ToastController, public authService: AuthService, public geolocation: Geolocation) {
	this.myImageKtp = "assets/imgs/photo-camera.png"; 
    this.myImageNpwp = "assets/imgs/photo-camera.png";
	this.myImageOutlet = "assets/imgs/photo-camera.png";
    //this.regData.imageOri = '';
    //this.regData.imageOriNpwp = '';
	//this.regData.imageOriOutlet = '';
	this.label_outlet='';
	this.label_alamat='';
	//this.regData.agent_id = localStorage.getItem("agent_id");
	this.getLocation();
	this.provinceData();
  }
  
	kode_customer_set(kotaId){
		//var str = kotaId;
		//var splitted = str.split("-"); 
		this.todo.kotaid = kotaId;
		this.todo.kode_daftar_customer = this.regData.kategoriSelected;
		
        //console.log(this.regData.kategoriSelected);
		//this.regData.kode_pos = splitted[3];
		
	    //this.showLoader();
		this.authService.getKodeCustomerGenerate(this.todo).then((result) => {
			this.data = result;
			let cek_respon = this.data.result_status;
			if(cek_respon != '0'){
				this.regData.kode_customer =  this.data.kode_customer_generate;
			} 
			
			this.kecamatanData(kotaId);
			//this.loading.dismiss();             
        }).catch((err) => {
			//this.loading.dismiss();
            let toast = this.toastCtrl.create({
                message: err,
                duration: 2000,
                position: 'bottom'
            });
            toast.present();
        })  
   }

    provinceData() {
		this.showLoader();
		this.authService.getProvinceRegister().then((result) => {
			this.data = result;
			let cek_respon = this.data.result_status;
			if(cek_respon != '0'){
				this.data_provinsi =  this.data.result_data;
			} 
			this.loading.dismiss();
        }).catch((err) => {
			this.loading.dismiss();
            let toast = this.toastCtrl.create({
                message: err,
                duration: 2000,
                position: 'bottom'
            });
            toast.present();
        }) 
   }
   
   onGetCity(provinceId){
	    //this.showLoader();
		this.authService.getCityRegister(provinceId).then((result) => {
			this.data = result;
			let cek_respon = this.data.result_status;
			if(cek_respon != '0'){
				this.data_city =  this.data.result_data;
				//this.data_kurir = null;
			} 
			//this.loading.dismiss();             
        }).catch((err) => {
			//this.loading.dismiss();
            let toast = this.toastCtrl.create({
                message: err,
                duration: 2000,
                position: 'bottom'
            });
            toast.present();
        }) 
   }
   
   kecamatanData(kotaId){
	    //this.showLoader();
		this.authService.getKecamatanRegister(kotaId).then((result) => {
			this.data = result;
			let cek_respon = this.data.result_status;
			if(cek_respon != '0'){
				this.data_kecamatan =  this.data.result_data;
				//this.data_kurir = null;
			} 
			//this.loading.dismiss();             
        }).catch((err) => {
			//this.loading.dismiss();
            let toast = this.toastCtrl.create({
                message: err,
                duration: 2000,
                position: 'bottom'
            });
            toast.present();
        }) 
   }
   
   onGetKelurahan(kecamatanId){
	    //this.showLoader();
		this.authService.getKelurahanRegister(kecamatanId).then((result) => {
			this.data = result;
			let cek_respon = this.data.result_status;
			if(cek_respon != '0'){
				this.data_kelurahan =  this.data.result_data;
				//this.data_kurir = null;
			} 
			//this.loading.dismiss();             
        }).catch((err) => {
			//this.loading.dismiss();
            let toast = this.toastCtrl.create({
                message: err,
                duration: 2000,
                position: 'bottom'
            });
            toast.present();
        }) 
   }
   
    saveDataUser() {
		this.showLoader();
		this.authService.register_survey(this.regData).then((result) => {
		  this.data = result;
		  let cek_respon = this.data.message;
		  
		  if(cek_respon =='Sucess'){
			  this.navCtrl.setRoot(AktifasiOutletPage);
		  }
		  
		  this.loading.dismiss();
		  let toast = this.toastCtrl.create({
			message: this.data.message,
			duration: 3000,
			position: "bottom"
		  });
		  toast.present();
		
		  //this.navCtrl.pop();
		}).catch((err) => {
			this.loading.dismiss();
			let toast = this.toastCtrl.create({
				message: err,
				duration: 2000,
				position: 'bottom'
			});
			toast.present();
		}) 
		
		//console.log(this.regData);
    }
 

  showLoader(){
    this.loading = this.loadingCtrl.create({
        content: 'Save Data...'
    });

    this.loading.present();
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom',
      dismissOnPageChange: true
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

  takePhotoKtp(){
		this.camera.getPicture({
			destinationType: this.camera.DestinationType.DATA_URL,
			targetWidth: 1000,
			targetHeight: 1000
		}).then((imageData) => {
			this.myImageKtp = "data:image/jpeg;base64," + imageData;
			this.regData.imageOri = imageData;
		}, (err) => {
			console.log(err);
		}); 
		
  }

  takePhotoNpwp(){
		this.camera.getPicture({
			destinationType: this.camera.DestinationType.DATA_URL,
			targetWidth: 1000,
			targetHeight: 1000
		}).then((imageDataNpwp) => {
			this.myImageNpwp = "data:image/jpeg;base64," + imageDataNpwp;
			this.regData.imageOriNpwp = imageDataNpwp;
		}, (err) => {
			console.log(err);
		}); 
		
  }
  
  takePhotoOutlet(){
		this.camera.getPicture({
			destinationType: this.camera.DestinationType.DATA_URL,
			targetWidth: 1000,
			targetHeight: 1000
		}).then((imageDataOutlet) => {
			this.myImageOutlet = "data:image/jpeg;base64," + imageDataOutlet;
			this.regData.imageOriOutlet = imageDataOutlet;
		}, (err) => {
			console.log(err);
		}); 
		
  }
  
  hideOutlet(kategori){
	  //this.hideMe = false;
	   if(kategori == '1'){
		  this.inputDisabled = false;
		  this.inputDisabledKar = false;
		  this.inputDisabledOut = false;
		  this.inputDisabledNip = true;
		  this.label_outlet='Corporate';
		  this.label_alamat='Outlet/Rumah';
		  this.label_wajib='';
		  this.label_nama='Pemilik/Kontak';
		   this.regData.kode_sales ='';
		   this.showHsh = true;
	  } 
	  else if(kategori == '2'){
		  this.inputDisabled = false;
		  this.inputDisabledKar = false;
		  this.inputDisabledOut = false;
		  this.inputDisabledNip = true;
		  this.label_outlet='Corporate';
		  this.label_alamat='Outlet/Rumah';
		  this.label_wajib='*';
		  this.label_nama='Pemilik/Kontak';
		  this.regData.kode_sales ='';
		  this.showHsh = true;
	  } 
	 else if(kategori == '3'){
		  this.inputDisabled = false;
		  this.inputDisabledKar = true;
		  this.inputDisabledOut = true;
		  this.inputDisabledNip = false;
		  this.label_outlet='Corporate';
		  this.label_alamat='Rumah';
		  this.label_wajib='';
		  this.label_nama='Lengkap';
		  this.regData.kode_sales ='HSH';
		  this.buttonDisabled = true;
		  this.showHsh = false;
	  }  
	  else {
		  this.inputDisabled = true;
		  this.inputDisabledKar = true;
		  this.inputDisabledOut = true;
		  this.inputDisabledNip = true;
		  this.label_outlet='Outlet';
		  this.label_alamat='';
		  this.label_wajib='';
		  this.label_nama='Nama';
		  this.showHsh = true;
	  }
  }
  
  getLocation(){
		this.geolocation.getCurrentPosition().then((res) => {
		  this.regData.location_lat  = res.coords.latitude;
		  this.regData.location_long = res.coords.longitude;
          //console.log(this.regData.location_lat);
		}).catch((error) => {
		console.log('Error getting location', error);
		}); 
	}

}
