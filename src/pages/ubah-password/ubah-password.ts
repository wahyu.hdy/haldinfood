import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service/auth-service';
import { LoginPage } from '../authentication/login/login';

/**
 * Generated class for the UbahPasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-ubah-password',
  templateUrl: 'ubah-password.html',
})
export class UbahPasswordPage {
	
	todo:any = {};
	data:any = {};
	loading: any;
	
	 regData = { password:'', password2:'', userId:'' }

  constructor(public navCtrl: NavController, public navParams: NavParams, public authService: AuthService, private toastCtrl: ToastController, public loadingCtrl: LoadingController) {
	 this.regData.userId  = localStorage.getItem('agent_id');
  }
  
  ubahPassword() {
		//console.log(this.regData);
		this.showLoader();
		this.authService.ubah_password(this.regData).then((result) => {
		  this.data = result;
		  let cek_respon = this.data.data_status;
		  
		  if(cek_respon =='Sucess'){
			  localStorage.clear();
			  this.navCtrl.push(LoginPage);
		  }
		  
		  this.loading.dismiss();
		  let toast = this.toastCtrl.create({
			message: this.data.message,
			duration: 3000,
			position: "bottom"
		  });
		  toast.present();
		
		  //this.navCtrl.pop();
		}).catch((err) => {
			this.loading.dismiss();
			let toast = this.toastCtrl.create({
				message: err,
				duration: 2000,
				position: 'bottom'
			});
			toast.present();
		}) 
    }


  showLoader(){
		this.loading = this.loadingCtrl.create({
			content: 'Loading...'
		});

		this.loading.present();
	}

    presentToast(msg) {
		let toast = this.toastCtrl.create({
			  message: msg,
			  duration: 3000,
			  position: 'bottom',
			  dismissOnPageChange: true
		});

		toast.onDidDismiss(() => {
		  console.log('Dismissed toast');
		});

		toast.present();
    }

}
