import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service/auth-service';
import { Geolocation } from '@ionic-native/geolocation';
import { LeadPage } from '../lead/lead';

/**
 * Generated class for the AddLeadPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-add-lead',
  templateUrl: 'add-lead.html',
})
export class AddLeadPage {

  loading: any;
  todo:any = {};
  data: any;
  
  public location_lat:any;
  public location_long:any;
  public data_provinsi: any;
  public data_city: any;
  public data_kecamatan: any;

  label_outlet:string;
  label_alamat:string;
  
  regData = { nama_outlet:'', nama_kontak:'', telepon:'', email:'', alamat:'', fu_satu:'', jam_fu:'', location_lat:0, location_long:0,
  sumber_data:'', kategori_outlet:'', nama_kontak2:'', telepon2:'', sosial_media:'', jumlah_outlet:'', tanggal_po:'', produk_beli:'',
  total_rupiah:'', provinsiSelected:'', kode_agent:'', citySelected:'', kecamatanSelected:'', kode_sales:''
	}
  
  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, private toastCtrl: ToastController, public authService: AuthService, public geolocation: Geolocation) {
	//this.getLocation();
	this.provinceData();
 }
  
    saveDataLead() {
		console.log(this.regData);
		this.todo.userId = localStorage.getItem('agent_id');
		
		this.showLoader();
		this.authService.save_data_lead(this.regData).then((result) => {
		  this.data = result;
		  let cek_respon = this.data.sts_data;
		  
		  if(cek_respon =='Sucess'){
			  this.navCtrl.setRoot(LeadPage);
		  }
		  
		  this.loading.dismiss();
		  let toast = this.toastCtrl.create({
			message: this.data.message,
			duration: 3000,
			position: "bottom"
		  });
		  toast.present();
		
		  //this.navCtrl.pop();
		}).catch((err) => {
			this.loading.dismiss();
			let toast = this.toastCtrl.create({
				message: err,
				duration: 2000,
				position: 'bottom'
			});
			toast.present();
		})  
		
		//console.log(this.regData);
    }
	
	showLoader(){
    	this.loading = this.loadingCtrl.create({
    	    content: 'Save Data...'
    	});
    	this.loading.present();
  	}

  
	provinceData() {
		this.showLoader();
		this.authService.getProvinceRegister().then((result) => {
			this.data = result;
			let cek_respon = this.data.result_status;
			if(cek_respon != '0'){
				this.data_provinsi =  this.data.result_data;
			} 
			this.loading.dismiss();
        }).catch((err) => {
			this.loading.dismiss();
            let toast = this.toastCtrl.create({
                message: err,
                duration: 2000,
                position: 'bottom'
            });
            toast.present();
        }) 
   }
   
   onGetCity(provinceId){
	    //this.showLoader();
		this.authService.getCityRegister(provinceId).then((result) => {
			this.data = result;
			let cek_respon = this.data.result_status;
			if(cek_respon != '0'){
				this.data_city =  this.data.result_data;
				//this.data_kurir = null;
			} 
			//this.loading.dismiss();             
        }).catch((err) => {
			//this.loading.dismiss();
            let toast = this.toastCtrl.create({
                message: err,
                duration: 2000,
                position: 'bottom'
            });
            toast.present();
        }) 
   }
   
   kode_agent(kotaId){
	//var str = kotaId;
	//var splitted = str.split("-"); 
	this.todo.kotaid = kotaId;
	
	//console.log(this.todo);
	//this.regData.kode_pos = splitted[3];
	
	//this.showLoader();
	this.authService.getKodeAgentGenerate(this.todo).then((result) => {
		this.data = result;
		let cek_respon = this.data.result_status;
		if(cek_respon != '0'){
			this.regData.kode_agent =  this.data.kode_agent_generate;
		} 
		
		this.kecamatanData(kotaId);
		//this.loading.dismiss();             
	}).catch((err) => {
		//this.loading.dismiss();
		let toast = this.toastCtrl.create({
			message: err,
			duration: 2000,
			position: 'bottom'
		});
		toast.present();
	}) 
}

kecamatanData(kotaId){
	//this.showLoader();
	this.authService.getKecamatanRegister(kotaId).then((result) => {
		this.data = result;
		let cek_respon = this.data.result_status;
		if(cek_respon != '0'){
			this.data_kecamatan =  this.data.result_data;
			//this.data_kurir = null;
		} 
		//this.loading.dismiss();             
	}).catch((err) => {
		//this.loading.dismiss();
		let toast = this.toastCtrl.create({
			message: err,
			duration: 2000,
			position: 'bottom'
		});
		toast.present();
	}) 
}

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom',
      dismissOnPageChange: true
    });

    toast.onDidDismiss(() => {
    //   console.log('Dismissed toast');
    });

    toast.present();
  }

	
	  getLocation(){
		this.geolocation.getCurrentPosition().then((res) => {
		  this.regData.location_lat  = res.coords.latitude;
		  this.regData.location_long = res.coords.longitude;
          //console.log(this.regData.location_lat);
		}).catch((error) => {
		// console.log('Error getting location', error);
		}); 
	}



  /* ionViewDidLoad() {
    console.log('ionViewDidLoad AddLeadPage');
  } */

}
