import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, LoadingController, ToastController, AlertController  } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service/auth-service';
import { LeadPage } from '../../pages/lead/lead';
import { Camera } from '@ionic-native/camera';

/**
 * Generated class for the DetailLeadPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-detail-lead',
  templateUrl: 'detail-lead.html',
})
export class DetailLeadPage {
  regData = { imageOriOutlet:'', status_folowup:'',hasil_followup:'', lead_id:'', catatan:''}	
  public detail_item: any; 
  todo:any = {};
  data:any = {};
  loading: any;
  
  public img:String;
  public cameraSupported:boolean;
  public photos: any;
  public base64Image: string;
  public imageURL: any;
  public myImageOutlet:string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public authService: AuthService, public loadingCtrl: LoadingController, private toastCtrl: ToastController, private camera: Camera, public alertCtrl: AlertController) {
	this.todo.userId   = localStorage.getItem('agent_id');
	this.todo.leadId = navParams.get('leadId');
	this.dataDetail();
	this.myImageOutlet = "assets/imgs/photo-camera.png";
	this.regData.imageOriOutlet = '';
	this.regData.lead_id = navParams.get('leadId');
  }
  
    dataDetail() {
		this.showLoader();
		this.authService.getDetailLead(this.todo).then((result) => {
			this.data = result;
			let cek_respon = this.data.result_status;
			if(cek_respon != '0'){
				this.detail_item = this.data.result_data;
				//console.log(this.detail_item);
			} 
			this.loading.dismiss();             
        }).catch((err) => {
			this.loading.dismiss();
            let toast = this.toastCtrl.create({
                message: err,
                duration: 2000,
                position: 'bottom'
            });
            toast.present();
        }) 
    }
	
	simpan_detail(){
		this.showLoader();
		this.authService.save_detail_lead(this.regData).then((result) => {
		  this.data = result;
		  let cek_respon = this.data.sts_data;
		  
		  if(cek_respon =='Sucess'){
			  this.navCtrl.setRoot(LeadPage);
		  }
		  
		  this.loading.dismiss();
		  let toast = this.toastCtrl.create({
			message: this.data.message,
			duration: 3000,
			position: "bottom"
		  });
		  toast.present();
		
		  //this.navCtrl.pop();
		}).catch((err) => {
			this.loading.dismiss();
			let toast = this.toastCtrl.create({
				message: err,
				duration: 2000,
				position: 'bottom'
			});
			toast.present();
		})  
		
		//console.log(this.regData);
	}

    showLoader(){
		this.loading = this.loadingCtrl.create({
			content: 'Loading...'
		});

		this.loading.present();
	}

    presentToast(msg) {
		let toast = this.toastCtrl.create({
			  message: msg,
			  duration: 3000,
			  position: 'bottom',
			  dismissOnPageChange: true
		});

		toast.onDidDismiss(() => {
		  console.log('Dismissed toast');
		});

		toast.present();
    }

 
  
   dismissModal() {
    //let data = { 'foo': 'bar' };
    this.viewCtrl.dismiss();
  }
  
  goBack(){
	  this.navCtrl.push(LeadPage);
  }
  
  takePhotoOutlet(){
		this.camera.getPicture({
			destinationType: this.camera.DestinationType.DATA_URL,
			targetWidth: 1000,
			targetHeight: 1000
		}).then((imageDataOutlet) => {
			this.myImageOutlet = "data:image/jpeg;base64," + imageDataOutlet;
			this.todo.imageOriOutlet = imageDataOutlet;
		}, (err) => {
			console.log(err);
		}); 
		
  }
  
  showPrompt(id) {
    let prompt = this.alertCtrl.create({
      title: 'Folow Up',
      message: "Tambah Jadwal Follow Up",
      inputs: [
        {
          name: 'tanggal',
          placeholder: 'tanggal',
		   type: 'date'
        },{
          name: 'jam',
          placeholder: 'jam',
		   type: 'time'
        },{
          name: 'id',
		  value : id,
		  type: 'hidden'
        },
		
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Save',
          handler: data => {
            //console.log(data.tanggal, data.jam, data.id); 
			this.simpanLeadBaru(data.tanggal, data.jam, data.id);
          }
        }
      ]
    });
    prompt.present();

  }
  
  simpanLeadBaru(tanggal, jam, id) {
		this.todo.userId   = localStorage.getItem('agent_id');
		this.todo.tanggal  = tanggal;
		this.todo.jam      = jam;
		this.todo.id       = id;
		
		this.showLoader();
		this.authService.saveLeadBaru(this.todo).then((result) => {
		this.data = result;
		  //let cek_respon = this.data.message
		  
		  this.loading.dismiss();
		  let toast = this.toastCtrl.create({
			message: this.data.message,
			duration: 3000,
			position: "bottom"
		  });
		  toast.present();
		
		  //this.navCtrl.pop();
		  //this.doRefresh();
		 
		}).catch((err) => {
			this.loading.dismiss();
			let toast = this.toastCtrl.create({
				message: err,
				duration: 2000,
				position: 'bottom'
			});
			toast.present();
		})  
		
		//console.log(this.regData);
		
    }


}
