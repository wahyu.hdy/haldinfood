import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service/auth-service';
import { ProductPage } from '../../pages/product/product';
import { OrdersPage } from '../../pages/orders/orders';
import { OcSamplePage } from '../../pages/oc-sample/oc-sample';
import { SampleProductPage } from '../../pages/sample-product/sample-product';

/**
 * Generated class for the CartSamplePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-cart-sample',
  templateUrl: 'cart-sample.html',
})
export class CartSamplePage {
	
  cart = [];
  products = [];
  todo:any = {};
  
  qty_custom: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, private alertCtrl: AlertController, public authService: AuthService) {
	  this.todo.providerUser   = localStorage.getItem('provider_user');
  }

   ngOnInit() {
    this.cart = this.authService.getCart();
	
	//console.log(this.cart.length);
	
	if(this.cart.length == 0){
		this.presentAlert('Keranjang Belanja Kosong');
	}
  }
  
   presentAlert(alt) {
    let alert = this.alertCtrl.create({
      title: 'Alert',
      message: alt,
      buttons: [ {
          text: 'Ok',
          handler: () => {
			if(this.todo.providerUser == 'Sales'){
				this.navCtrl.push(SampleProductPage);
			} 
			else {
				this.navCtrl.push(ProductPage);
			}
          }
        }]
    });

    alert.present();
  }

 
  decreaseCartItem(product) {
    this.authService.decreaseProduct(product);
	
	this.cart = this.authService.getCart();
	
	if(this.cart.length == 0){
		this.presentAlert('Keranjang Belanja Kosong');
	}
  }
 
  increaseCartItem(product) {
	//for(let i = 0; i < 30; i++) {
        this.authService.addProduct(product);
	//}
  }
  
  simpanCartBaru(prod, lp){
	  for(let i = 0; i < lp; i++) {
        this.authService.addProduct(prod);
	  }
  }
 
  removeCartItem(product) {
    this.authService.removeProduct(product);
	
	this.cart = this.authService.getCart();
	
	if(this.cart.length == 0){
		this.presentAlert('Keranjang Belanja Kosong');
	}
  }
 
  getTotal() {
    return this.cart.reduce((i, j) => i + j.price * j.amount, 0);
  }
 
  close() {
    //this.modalCtrl.dismiss();
  }
 
  async checkout() {
	this.navCtrl.push(OrdersPage);
  }
  
  async create_oc() {
	this.navCtrl.push(OcSamplePage);
  }
  
  showPromptCart(p) {
    let prompt = this.alertCtrl.create({
      title: 'Jumlah QTY',
      message: "Masukan Jumlah QTY",
      inputs: [
        {
          name: 'qty_custom_set',
          placeholder: 'QTY',
		   type: 'text'
        },{
          name: 'p',
		  value : p,
		  type: 'hidden'
        },
		
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Save',
          handler: data => {
            //console.log(data.p, data.jam, data.id); 
			this.simpanCartBaru(data.p, data.qty_custom_set);
          }
        }
      ]
    });
    prompt.present();

  }

}
