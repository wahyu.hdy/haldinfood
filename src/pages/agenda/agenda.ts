import { Component } from '@angular/core';
import { NavController, NavParams, Platform, ModalController, LoadingController, ToastController } from 'ionic-angular';
import { Calendar } from '@ionic-native/calendar';
import { AddAgendaPage } from '../../pages/add-agenda/add-agenda';
import { AuthService } from '../../providers/auth-service/auth-service';
import { HomePage } from '../../pages/home/home';
import { AgendaDetailPage } from '../../pages/agenda-detail/agenda-detail';
import { ProfileUserPage } from '../profile-user/profile-user';
import { BantuanPage } from '../bantuan/bantuan';

/**
 * Generated class for the AgendaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-agenda',
  templateUrl: 'agenda.html',
})


export class AgendaPage {  
  inputDisabled: boolean = false;
  disview: boolean = false;
  public data_item: any;
  public data_default: any;
  todo:any = {};
  data:any = {};
  loading: any;	
  tg: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public calendar: Calendar, public plt: Platform, public modalCtrl: ModalController, public loadingCtrl: LoadingController, private toastCtrl: ToastController, public authService: AuthService) {
	 this.todo.userId   = localStorage.getItem('agent_id');
  }
  
    ngOnInit() {
	    this.inputDisabled = true;
		this.dateSelectedDefault();	 
    }
	
	dateSelectedDefault(){
		//console.log(date);
		const dates = new Date();
        const today = dates.toISOString().substring(0, 10);
		this.todo.tgl_agenda = today;
		
		//this.showLoader();
		this.authService.getListAgendaDefault(this.todo).then((result) => {
			this.data = result;
			let cek_respon = this.data.result_status;
			//console.log(cek_respon);
			if(cek_respon != '0'){
				this.data_default = this.data.result_data;
			} 
	        this.todo.tgl_pilih = this.data.tanggal_view;
           
        }).catch((err) => {
			//this.loading.dismiss();
            let toast = this.toastCtrl.create({
                message: err,
                duration: 2000,
                position: 'bottom'
            });
            toast.present();
        })
	}

	dateSelected(date){
		this.todo.tgl_agenda = date;
		//console.log(this.todo.tgl_agenda);
		//this.showLoader();
		this.authService.getListAgenda(this.todo).then((result) => {
			this.data = result;
			let cek_respon = this.data.result_status;
			//console.log(cek_respon);
			if(cek_respon != '0'){
				this.data_item = this.data.result_data;
				this.disview = true;
			} else {
				this.data_item = null;
				this.disview = true;
			}
	        this.todo.tgl_pilih = this.data.tanggal_view;
			//this.loading.dismiss();
			//this.todo.cek_data = this.data_item.length;
           
        }).catch((err) => {
			//this.loading.dismiss();
            let toast = this.toastCtrl.create({
                message: err,
                duration: 2000,
                position: 'bottom'
            });
            toast.present();
        })
	}
	 
	show_calendar(){
		 this.inputDisabled = false;
	}
 
	close_calendar(){
		 this.inputDisabled = true;
	}
 
	add_agenda(){
		const profileModal = this.modalCtrl.create(AddAgendaPage);
		profileModal.onDidDismiss(data => {
		  //console.log(data);
		   //this.madalDismissData = JSON.stringify(data);
		});
		profileModal.present();
	}
	
	detail_agenda(agenda_id){
		const profileModal = this.modalCtrl.create(AgendaDetailPage, { agendaId: agenda_id });
		profileModal.onDidDismiss(data => {
		  //console.log(data);
		   //this.madalDismissData = JSON.stringify(data);
		});
		profileModal.present();
	}
	
	showLoader(){
		this.loading = this.loadingCtrl.create({
			content: 'Save Data...'
		});

		this.loading.present();
	  }

	presentToast(msg) {
		let toast = this.toastCtrl.create({
		  message: msg,
		  duration: 3000,
		  position: 'bottom',
		  dismissOnPageChange: true
		});

		toast.onDidDismiss(() => {
		  console.log('Dismissed toast');
		});

		toast.present();
	}
 
    goBack(){
		 this.navCtrl.push(HomePage);
	}
	
	 myProfile(){
	  this.navCtrl.push(ProfileUserPage);
	  }
	  
	  myHelp(){
		  this.navCtrl.push(BantuanPage);
	  }
	
 

}
