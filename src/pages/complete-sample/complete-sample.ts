import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { HomePage } from '../../pages/home/home';
import { AuthService } from '../../providers/auth-service/auth-service';

/**
 * Generated class for the CompleteSamplePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-complete-sample',
  templateUrl: 'complete-sample.html',
})
export class CompleteSamplePage {
	
	todo:any = {};
  payment_nicepay = [];  
  cart = [];
   loading: any;
   data:any = {};

  constructor(public navCtrl: NavController, public navParams: NavParams,  public loadingCtrl: LoadingController, private toastCtrl: ToastController, public authService: AuthService) {
  }

  closePage(){
	  this.showLoader();
	  this.cart = this.authService.getCart();
	  this.authService.removeAllCartProduct(0, this.cart.length);
	  this.navCtrl.push(HomePage);
	  this.loading.dismiss();
  }
  
  
  showLoader(){
		this.loading = this.loadingCtrl.create({
			content: 'Loading...'
		});

		this.loading.present();
  }

  presentToast(msg) {
		let toast = this.toastCtrl.create({
			  message: msg,
			  duration: 3000,
			  position: 'bottom',
			  dismissOnPageChange: true
		});

		toast.onDidDismiss(() => {
		  console.log('Dismissed toast');
		});

		toast.present();
   }

}
