import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, LoadingController, ToastController } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service/auth-service';
import { DomSanitizer } from '@angular/platform-browser';
import { ProductPage } from '../../pages/product/product';
import { CompleteOrderPage } from '../../pages/complete-order/complete-order';

/**
 * Generated class for the OrdersPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-orders',
  templateUrl: 'orders.html',
})
export class OrdersPage {
  
  cart = [];
  products = [];
  completePayment = [];
  public data_provinsi: any;
  public data_origin: any;  
  public data_city: any; 
  public data_kecamatan: any; 
  public data_kelurahan: any;
  public data_cost: any; 
  //public data_kurir: any; 
  public data_customer: any;
  public data_set_no: any;
  
  citySelected: string;
  kecSelected: string;
  kelSelected: string;
  total_berat: number;
  total_belanja: number;
  tipe_user:string;
  kurirSelected: string;
  paketSelected: string;
  agent_id: number;
  customer: string;
  nama_penerima: string;
  customerx: any;
  provinsiSelected: string;
  originSelected: string;
  inputDisabled: boolean = false;
  inputDisabledShip: boolean = false;
  inputDisabledGpaket: boolean = false;
  stockHiden: boolean = false;

  inputDisabledCibitung: boolean = false;
  inputDisabledBella: boolean = false;
  inputDisabledYogya: boolean = false;
  inputDisabledKendari: boolean = false;
  inputDisabledSemarang: boolean = false;
  inputDisabledSolo: boolean = false;
  inputDisabledSurabaya: boolean = false;
  inputDisabledBali: boolean = false;
  inputDisabledBandung: boolean = false;
  inputDisabledMedan: boolean = false;
  inputDisabledBatam: boolean = false;
  inputDisabledPekan: boolean = false;
  inputDisabledPadang: boolean = false;
  inputDisabledPalembang: boolean = false;
  inputDisabledBanjarmasin: boolean = false;
  inputDisabledSamarinda: boolean = false;
  inputDisabledMakassar: boolean = false;

  kartuku:string;
  province_id_tab:string;
  city_id_tab:string;
  kec_id_tab:string;
  kel_id_tab:string;
  bank_set:string;
  pay_methode:string;
  card_metode:string;
  ov_amt:string;
  ov_tixId:string;
  ov_mctoken:string;
  ov_time:string;
  
  data:any = {};
  loading: any;
  alamat: string;
  pos: string;
  
  todo:any = {};
  gelar:string;
  telepon:string;
  paymentMethod:string;
  tanggal_result:string;
  jam_result:string;
  tanggal_pay:string;

  constructor(public navCtrl: NavController, public navParams: NavParams, private alertCtrl: AlertController, public authService: AuthService, public loadingCtrl: LoadingController, private toastCtrl: ToastController, public sanitizer: DomSanitizer) {
	  this.todo.providerUser   = localStorage.getItem('provider_user');
	  this.inputDisabledGpaket = true;
	  this.inputDisabledCibitung 	= true;
	  this.inputDisabledBella 		= true;
	  this.inputDisabledYogya 		= true;
	  this.inputDisabledKendari 	= true;
	  this.inputDisabledSemarang 	= true;
	  this.inputDisabledSolo 		= true;
	  this.inputDisabledBali 		= true;
	  this.inputDisabledBandung 	= true;
	  this.inputDisabledSurabaya 	= true;
	  this.inputDisabledMedan 		= true;
	  this.inputDisabledBatam		= true;
	  this.inputDisabledPekan		= true;
	  this.inputDisabledPadang		= true;
	  this.inputDisabledPalembang 	= true;
	  this.inputDisabledBanjarmasin = true;
	  this.inputDisabledSamarinda 	= true;
	  this.inputDisabledMakassar	= true;
  }

  ngOnInit() {
    this.cart = this.authService.getCart();
	//console.log(this.cart);
	this.provinceData();
	this.customerData();
	this.originData();
	//this.onGetKurir();
	
	if(this.cart.length == 0){
		this.presentAlert('Keranjang Belanja Kosong');
	}
	
	this.total_berat = this.getWeight();
	this.total_belanja = this.getTotal();
	this.tipe_user = this.todo.providerUser;
	this.hideOutlet();
	
	this.todo.id_agent  = '0&'+localStorage.getItem('agent_id');
	//this.getnoHp(this.todo.id_agent);
	this.customerAddress();
    //console.log(this.total_berat);
  }
  
  customerAddress() {
		//this.showLoader();
		this.authService.getCustomerAddress().then((result) => {
		
			this.data = result;
			let cek_respon = this.data.result_status;
			// console.log(this.data);
			if(cek_respon != '0'){
				this.province_id_tab =  this.data.result_data;
				this.city_id_tab     =  this.data.city_id;
				this.kec_id_tab     =  this.data.kec_id;
				this.kel_id_tab     =  this.data.kel_id;
				this.onGetCity2(this.province_id_tab);
				this.kecamatanData(this.city_id_tab);
				this.onGetKelurahan(this.kec_id_tab);
			} 
			//console.log(this.city_id_tab);
			//this.loading.dismiss();             
        }).catch((err) => {
			//this.loading.dismiss();
            let toast = this.toastCtrl.create({
                message: err,
                duration: 2000,
                position: 'bottom'
            });
            toast.present();
        }) 
   }
  
  
   customerData() {
		//this.showLoader();
		this.authService.getCustomer().then((result) => {
			this.data = result;
			// console.log(this.data);
			let cek_respon = this.data.result_status;
			// console.log(cek_respon);
			if(cek_respon != '0'){
				this.data_customer =  this.data.result_data;
				// console.log(this.data_customer)
				this.telepon =  this.data_customer[0]['phone_number'];
				this.customer = this.data_customer[0]['gelar']+'&'+this.data_customer[0]['id']+'&'+this.data_customer[0]['customer_name']+'&'+this.data_customer[0]['email'];
			    this.alamat = this.data_customer[0]['alamat_kirim'];
				this.pos = this.data_customer[0]['postal_code'];
				this.provinsiSelected = this.data_customer[0]['province_id'];
				this.citySelected = this.data_customer[0]['city_id'];
				this.kecSelected = this.data_customer[0]['kec'];
				this.kelSelected = this.data_customer[0]['kel'];
				this.bank_set = this.data_customer[0]['virtual_card'];
				this.paymentMethod = this.data_customer[0]['virtual_card'];				
			} 
			//this.loading.dismiss();   
        }).catch((err) => {
			//this.loading.dismiss();
            let toast = this.toastCtrl.create({
                message: err,
                duration: 2000,
                position: 'bottom'
            });
            toast.present();
        }) 
   }


   
   getnoHp(customer_id){
		let customer_data_set = customer_id.split("&");
		this.todo.customer_id = customer_data_set[1];
		
		//console.log(this.todo.customer_id);
	    //this.showLoader();
		this.authService.getPhoneNumber(this.todo).then((result) => {
			this.data = result;
			console.log(this.data.result_data);
			this.data_set_no =  this.data.result_data;
			this.telepon =  this.data_set_no[0]['phone_number'];
			this.customer = this.data_set_no[0]['gelar']+'&'+this.data_set_no[0]['id']+'&'+this.data_set_no[0]['customer_name']+'&'+this.data_set_no[0]['email'];
			this.alamat = this.data_set_no[0]['alamat_kirim'];
			this.pos = this.data_set_no[0]['postal_code'];
			this.provinsiSelected = this.data_set_no[0]['province_id']+'-'+this.data_set_no[0]['province'];
			this.citySelected = this.data_set_no[0]['city_id']+'-'+this.data_set_no[0]['city']+'-'+this.data_set_no[0]['postal_code'];
			this.bank_set = this.data_set_no[0]['virtual_card'];
			this.paymentMethod = this.data_set_no[0]['virtual_card'];
			this.todo.set_cost = '';
			//this.loading.dismiss();             
        }).catch((err) => {
			//this.loading.dismiss();
            let toast = this.toastCtrl.create({
                message: err,
                duration: 2000,
                position: 'bottom'
            });
            toast.present();
        }) 
   }
   
   originData() {
		//this.showLoader();
		this.authService.getOriginPoint().then((result) => {
			this.data = result;
			let cek_respon = this.data.result_status;
			if(cek_respon != '0'){
				console.log(this.data.result_data);
				if(this.todo.providerUser == 'Karyawan'){ 
					this.data_origin = [this.data.result_data[4],this.data.result_data[3]];
				} else {
					this.data_origin =  this.data.result_data;
				}
			} 
			//this.loading.dismiss();
        }).catch((err) => {
			//this.loading.dismiss();
            let toast = this.toastCtrl.create({
                message: err,
                duration: 2000,
                position: 'bottom'
            });
            toast.present();
        }) 
   }
   
   onGetStock(pointId){
	if(pointId =='182'){
		this.inputDisabledKendari 		= false;
		this.inputDisabledBali 			= true;
		this.inputDisabledYogya 		= true;
		this.inputDisabledBandung 		= true;
		this.inputDisabledBella 		= true;
		this.inputDisabledCibitung 		= true;
		this.inputDisabledSemarang 		= true;
		this.inputDisabledSolo 			= true;
		this.inputDisabledSurabaya 		= true;
		this.inputDisabledMedan 		= true;
		this.inputDisabledBatam 		= true;
		this.inputDisabledPekan 		= true;
		this.inputDisabledPadang 		= true;
		this.inputDisabledPalembang 	= true;
		this.inputDisabledBanjarmasin 	= true;
		this.inputDisabledSamarinda		= true;
		this.inputDisabledMakassar		= true;
	} else if(pointId =='114'){
		this.inputDisabledBali 			= false;
		this.inputDisabledKendari 		= true;
		this.inputDisabledYogya 		= true;
		this.inputDisabledBandung 		= true;
		this.inputDisabledBella 		= true;
		this.inputDisabledCibitung 		= true;
		this.inputDisabledSemarang 		= true;
		this.inputDisabledSolo 			= true;
		this.inputDisabledSurabaya 		= true;
		this.inputDisabledMedan 		= true;
		this.inputDisabledBatam 		= true;
		this.inputDisabledPekan 		= true;
		this.inputDisabledPadang 		= true;
		this.inputDisabledPalembang 	= true;
		this.inputDisabledBanjarmasin 	= true;
		this.inputDisabledSamarinda		= true;
		this.inputDisabledMakassar		= true;
	} else if(pointId =='39'){
		this.inputDisabledYogya 		= false;
		this.inputDisabledBali 			= true;
		this.inputDisabledKendari 		= true;
		this.inputDisabledBandung 		= true;
		this.inputDisabledBella 		= true;
		this.inputDisabledCibitung 		= true;
		this.inputDisabledSemarang 		= true;
		this.inputDisabledSolo 			= true;
		this.inputDisabledSurabaya 		= true;
		this.inputDisabledMedan 		= true;
		this.inputDisabledBatam 		= true;
		this.inputDisabledPekan 		= true;
		this.inputDisabledPadang 		= true;
		this.inputDisabledPalembang 	= true;
		this.inputDisabledBanjarmasin 	= true;
		this.inputDisabledSamarinda		= true;
		this.inputDisabledMakassar		= true;
	} else if(pointId =='23'){
		this.inputDisabledBandung 		= false;
		this.inputDisabledYogya 		= true;
		this.inputDisabledBali 			= true;
		this.inputDisabledKendari 		= true;
		this.inputDisabledBella 		= true;
		this.inputDisabledCibitung 		= true;
		this.inputDisabledSemarang 		= true;
		this.inputDisabledSolo 			= true;
		this.inputDisabledSurabaya 		= true;
		this.inputDisabledMedan 		= true;
		this.inputDisabledBatam 		= true;
		this.inputDisabledPekan 		= true;
		this.inputDisabledPadang 		= true;
		this.inputDisabledPalembang 	= true;
		this.inputDisabledBanjarmasin 	= true;
		this.inputDisabledSamarinda		= true;
		this.inputDisabledMakassar		= true;
	} else if(pointId =='155'){
		this.inputDisabledBella 		= false;
		this.inputDisabledBandung 		= true;
		this.inputDisabledYogya 		= true;
		this.inputDisabledBali 			= true;
		this.inputDisabledKendari 		= true;
		this.inputDisabledCibitung 		= true;
		this.inputDisabledSemarang 		= true;
		this.inputDisabledSolo 			= true;
		this.inputDisabledSurabaya 		= true;
		this.inputDisabledMedan 		= true;
		this.inputDisabledBatam 		= true;
		this.inputDisabledPekan 		= true;
		this.inputDisabledPadang 		= true;
		this.inputDisabledPalembang 	= true;
		this.inputDisabledBanjarmasin 	= true;
		this.inputDisabledSamarinda		= true;
		this.inputDisabledMakassar		= true;
	} else if(pointId =='54'){
		this.inputDisabledCibitung 		= false;
		this.inputDisabledBella 		= true;
		this.inputDisabledBandung 		= true;
		this.inputDisabledYogya 		= true;
		this.inputDisabledBali 			= true;
		this.inputDisabledKendari 		= true;
		this.inputDisabledSemarang 		= true;
		this.inputDisabledSolo 			= true;
		this.inputDisabledSurabaya 		= true;
		this.inputDisabledMedan 		= true;
		this.inputDisabledBatam 		= true;
		this.inputDisabledPekan 		= true;
		this.inputDisabledPadang 		= true;
		this.inputDisabledPalembang 	= true;
		this.inputDisabledBanjarmasin 	= true;
		this.inputDisabledSamarinda		= true;
		this.inputDisabledMakassar		= true;
	} else if(pointId =='399'){
		this.inputDisabledCibitung 		= true;
		this.inputDisabledBella 		= true;
		this.inputDisabledBandung 		= true;
		this.inputDisabledYogya 		= true;
		this.inputDisabledBali 			= true;
		this.inputDisabledKendari 		= true;
		this.inputDisabledSemarang 		= false;
		this.inputDisabledSolo 			= true;
		this.inputDisabledSurabaya 		= true;
		this.inputDisabledMedan 		= true;
		this.inputDisabledBatam 		= true;
		this.inputDisabledPekan 		= true;
		this.inputDisabledPadang 		= true;
		this.inputDisabledPalembang 	= true;
		this.inputDisabledBanjarmasin 	= true;
		this.inputDisabledSamarinda		= true;
		this.inputDisabledMakassar		= true;
	} else if(pointId =='445'){
		this.inputDisabledCibitung 		= true;
		this.inputDisabledBella 		= true;
		this.inputDisabledBandung 		= true;
		this.inputDisabledYogya 		= true;
		this.inputDisabledBali 			= true;
		this.inputDisabledKendari 		= true;
		this.inputDisabledSemarang 		= true;
		this.inputDisabledSolo 			= false;
		this.inputDisabledSurabaya 		= true;
		this.inputDisabledMedan 		= true;
		this.inputDisabledBatam 		= true;
		this.inputDisabledPekan 		= true;
		this.inputDisabledPadang 		= true;
		this.inputDisabledPalembang 	= true;
		this.inputDisabledBanjarmasin 	= true;
		this.inputDisabledSamarinda		= true;
		this.inputDisabledMakassar		= true;
	} else if(pointId =='444'){
		this.inputDisabledCibitung 		= true;
		this.inputDisabledBella 		= true;
		this.inputDisabledBandung 		= true;
		this.inputDisabledYogya 		= true;
		this.inputDisabledBali 			= true;
		this.inputDisabledKendari 		= true;
		this.inputDisabledSemarang 		= true;
		this.inputDisabledSolo 			= true;
		this.inputDisabledSurabaya 		= false;
		this.inputDisabledMedan 		= true;
		this.inputDisabledBatam 		= true;
		this.inputDisabledPekan 		= true;
		this.inputDisabledPadang 		= true;
		this.inputDisabledPalembang 	= true;
		this.inputDisabledBanjarmasin	= true;
		this.inputDisabledSamarinda		= true;
		this.inputDisabledMakassar		= true;
	} else if(pointId =='278'){
		this.inputDisabledCibitung 		= true;
		this.inputDisabledBella 		= true;
		this.inputDisabledBandung 		= true;
		this.inputDisabledYogya 		= true;
		this.inputDisabledBali 			= true;
		this.inputDisabledKendari 		= true;
		this.inputDisabledSemarang 		= true;
		this.inputDisabledSolo 			= true;
		this.inputDisabledSurabaya 		= true;
		this.inputDisabledMedan 		= false;
		this.inputDisabledBatam 		= true;
		this.inputDisabledPekan 		= true;
		this.inputDisabledPadang 		= true;
		this.inputDisabledPalembang 	= true;
		this.inputDisabledBanjarmasin 	= true;
		this.inputDisabledSamarinda		= true;
		this.inputDisabledMakassar		= true;
	} else if(pointId =='48'){
		this.inputDisabledCibitung 		= true;
		this.inputDisabledBella 		= true;
		this.inputDisabledBandung 		= true;
		this.inputDisabledYogya 		= true;
		this.inputDisabledBali 			= true;
		this.inputDisabledKendari 		= true;
		this.inputDisabledSemarang 		= true;
		this.inputDisabledSolo 			= true;
		this.inputDisabledSurabaya 		= true;
		this.inputDisabledMedan 		= true;
		this.inputDisabledBatam 		= false;
		this.inputDisabledPekan 		= true;
		this.inputDisabledPadang 		= true;
		this.inputDisabledPalembang 	= true;
		this.inputDisabledBanjarmasin 	= true;
		this.inputDisabledSamarinda		= true;
		this.inputDisabledMakassar		= true;
	} else if(pointId =='350'){
		this.inputDisabledCibitung 		= true;
		this.inputDisabledBella 		= true;
		this.inputDisabledBandung 		= true;
		this.inputDisabledYogya 		= true;
		this.inputDisabledBali 			= true;
		this.inputDisabledKendari 		= true;
		this.inputDisabledSemarang 		= true;
		this.inputDisabledSolo 			= true;
		this.inputDisabledSurabaya 		= true;
		this.inputDisabledMedan 		= true;
		this.inputDisabledBatam 		= true;
		this.inputDisabledPekan 		= false;
		this.inputDisabledPadang 		= true;
		this.inputDisabledPalembang 	= true;
		this.inputDisabledBanjarmasin 	= true;
		this.inputDisabledSamarinda		= true;
		this.inputDisabledMakassar		= true;
	} else if(pointId =='318'){
		this.inputDisabledCibitung 		= true;
		this.inputDisabledBella 		= true;
		this.inputDisabledBandung 		= true;
		this.inputDisabledYogya 		= true;
		this.inputDisabledBali 			= true;
		this.inputDisabledKendari 		= true;
		this.inputDisabledSemarang 		= true;
		this.inputDisabledSolo 			= true;
		this.inputDisabledSurabaya 		= true;
		this.inputDisabledMedan 		= true;
		this.inputDisabledBatam 		= true;
		this.inputDisabledPekan 		= true;
		this.inputDisabledPadang 		= false;
		this.inputDisabledPalembang 	= true;
		this.inputDisabledBanjarmasin 	= true;
		this.inputDisabledSamarinda		= true;
		this.inputDisabledMakassar		= true;
	} else if(pointId =='327'){
		this.inputDisabledCibitung 		= true;
		this.inputDisabledBella 		= true;
		this.inputDisabledBandung 		= true;
		this.inputDisabledYogya 		= true;
		this.inputDisabledBali 			= true;
		this.inputDisabledKendari 		= true;
		this.inputDisabledSemarang 		= true;
		this.inputDisabledSolo 			= true;
		this.inputDisabledSurabaya 		= true;
		this.inputDisabledMedan 		= true;
		this.inputDisabledBatam 		= true;
		this.inputDisabledPekan 		= true;
		this.inputDisabledPadang 		= true;
		this.inputDisabledPalembang 	= false;
		this.inputDisabledBanjarmasin 	= true;
		this.inputDisabledSamarinda		= true;
		this.inputDisabledMakassar		= true;
	} else if(pointId =='36'){
		this.inputDisabledCibitung 		= true;
		this.inputDisabledBella 		= true;
		this.inputDisabledBandung 		= true;
		this.inputDisabledYogya 		= true;
		this.inputDisabledBali 			= true;
		this.inputDisabledKendari 		= true;
		this.inputDisabledSemarang 		= true;
		this.inputDisabledSolo 			= true;
		this.inputDisabledSurabaya 		= true;
		this.inputDisabledMedan 		= true;
		this.inputDisabledBatam 		= true;
		this.inputDisabledPekan 		= true;
		this.inputDisabledPadang 		= true;
		this.inputDisabledPalembang 	= true;
		this.inputDisabledBanjarmasin 	= false;
		this.inputDisabledSamarinda		= true;
		this.inputDisabledMakassar		= true;
	} else if(pointId =='387'){
		this.inputDisabledCibitung 		= true;
		this.inputDisabledBella 		= true;
		this.inputDisabledBandung 		= true;
		this.inputDisabledYogya 		= true;
		this.inputDisabledBali 			= true;
		this.inputDisabledKendari 		= true;
		this.inputDisabledSemarang 		= true;
		this.inputDisabledSolo 			= true;
		this.inputDisabledSurabaya 		= true;
		this.inputDisabledMedan 		= true;
		this.inputDisabledBatam 		= true;
		this.inputDisabledPekan 		= true;
		this.inputDisabledPadang 		= true;
		this.inputDisabledPalembang 	= true;
		this.inputDisabledBanjarmasin 	= true;
		this.inputDisabledSamarinda		= false;
		this.inputDisabledMakassar		= true;
	} else if(pointId =='254'){
		this.inputDisabledCibitung 		= true;
		this.inputDisabledBella 		= true;
		this.inputDisabledBandung 		= true;
		this.inputDisabledYogya 		= true;
		this.inputDisabledBali 			= true;
		this.inputDisabledKendari 		= true;
		this.inputDisabledSemarang 		= true;
		this.inputDisabledSolo 			= true;
		this.inputDisabledSurabaya 		= true;
		this.inputDisabledMedan 		= true;
		this.inputDisabledBatam 		= true;
		this.inputDisabledPekan 		= true;
		this.inputDisabledPadang 		= true;
		this.inputDisabledPalembang 	= true;
		this.inputDisabledBanjarmasin 	= true;
		this.inputDisabledSamarinda		= true;
		this.inputDisabledMakassar		= false;
	}
	
	this.cart = this.authService.getCart();
	
	let params = { 
	  belanja: this.cart,
	  pointsetid : pointId
	};
	
	//console.log(params);
	
	this.authService.getOriginStock(params).then((result) => {
		this.data = result;
		let cek_respon = this.data.result_data;
		let cek_sts    = this.data.result_status;
		let cek_over   = this.data.result_over;
		
		if(cek_respon == '0'){
			let toast = this.toastCtrl.create({
				message: cek_sts+' Stock Tidak Tersedia',
				duration: 2000,
				position: 'bottom'
			});
			toast.present();
			this.stockHiden = true;
		} else {
			this.stockHiden = false;
		}
		
		if(cek_over == '1'){
			let toast = this.toastCtrl.create({
				message: 'Sisa '+cek_sts+' Di stock point ini hanya '+cek_respon,
				duration: 2000,
				position: 'bottom'
			});
			toast.present();
			this.stockHiden = true;
		}  else {
			this.stockHiden = false;
		}
		
	}).catch((err) => {
		let toast = this.toastCtrl.create({
			message: err,
			duration: 2000,
			position: 'bottom'
		});
		toast.present();
	})   
}
  
   provinceData() {
		this.showLoader();
		this.authService.getProvinceRegister().then((result) => {
			this.data = result;
			let cek_respon = this.data.result_status;
			if(cek_respon != '0'){
				this.data_provinsi =  this.data.result_data;
			} 
			this.loading.dismiss();
        }).catch((err) => {
			this.loading.dismiss();
            let toast = this.toastCtrl.create({
                message: err,
                duration: 2000,
                position: 'bottom'
            });
            toast.present();
        }) 
   }
   
   
   onGetCity(provinceId){
	    //this.showLoader();
		this.authService.getCity(provinceId).then((result) => {
			this.data = result;
			let cek_respon = this.data.result_status;
			if(cek_respon != '0'){
				this.data_city =  this.data.result_data;
				//this.data_kurir = null;
				this.kurirSelected = null;
				this.data_cost = null;
				this.paketSelected = null;
				
				//console.log(this.data_city);
			} 
			//this.loading.dismiss();             
        }).catch((err) => {
			//this.loading.dismiss();
            let toast = this.toastCtrl.create({
                message: err,
                duration: 2000,
                position: 'bottom'
            });
            toast.present();
        }) 
   }

   onGetCity2(provinceId){
	//this.showLoader();
	this.authService.getCityRegister(provinceId).then((result) => {
		this.data = result;
		let cek_respon = this.data.result_status;
		if(cek_respon != '0'){
			this.data_city =  this.data.result_data;
			//this.data_kurir = null;
		} 
		//this.loading.dismiss();             
	}).catch((err) => {
		//this.loading.dismiss();
		let toast = this.toastCtrl.create({
			message: err,
			duration: 2000,
			position: 'bottom'
		});
		toast.present();
	}) 
}

kecamatanData(kotaId){
	//this.showLoader();
	this.authService.getKecamatanRegister(kotaId).then((result) => {
		this.data = result;
		let cek_respon = this.data.result_status;
		if(cek_respon != '0'){
			this.data_kecamatan =  this.data.result_data;
			//this.data_kurir = null;
		} 
		//this.loading.dismiss();             
	}).catch((err) => {
		//this.loading.dismiss();
		let toast = this.toastCtrl.create({
			message: err,
			duration: 2000,
			position: 'bottom'
		});
		toast.present();
	}) 
}

onGetKelurahan(kecamatanId){
	//this.showLoader();
	this.authService.getKelurahanRegister(kecamatanId).then((result) => {
		this.data = result;
		let cek_respon = this.data.result_status;
		if(cek_respon != '0'){
			this.data_kelurahan =  this.data.result_data;
			//this.data_kurir = null;
		} 
		//this.loading.dismiss();             
	}).catch((err) => {
		//this.loading.dismiss();
		let toast = this.toastCtrl.create({
			message: err,
			duration: 2000,
			position: 'bottom'
		});
		toast.present();
	}) 
}
   
   showCostPaket($event){
	    //console.log(costPaket);
		let trg = $event.target.value;
		let price = trg.split(",");
		let price_set = price[5];
		//console.log(firstName);
		if(price_set != null){
			this.todo.set_cost = price_set;
		} 
		else {
			this.todo.set_cost = 0;
		}
		this.inputDisabledShip = true;
		this.inputDisabledGpaket = false;
   }
   
   gantipaket(){
	   this.inputDisabledShip = false;
	   this.inputDisabledGpaket = true;
   }
   
  /*  onGetKurir(){
	    //this.showLoader();
		this.authService.getKurir().then((result) => {
			this.data = result;
			//let cek_respon = this.data.result_status;
			//if(cek_respon != '0'){
				this.data_kurir =  this.data.result_data;
			//} 
			//this.loading.dismiss();             
        }).catch((err) => {
			//this.loading.dismiss();
            let toast = this.toastCtrl.create({
                message: err,
                duration: 2000,
                position: 'bottom'
            });
            toast.present();
        }) 
   } */
   
   getPaket(kurirId){
	    this.todo.kurirId     = kurirId;
		this.todo.cityId      = this.citySelected;
		this.todo.totalWeight = this.total_berat;
		this.todo.originset   = this.originSelected;
		
	    this.showLoader();
		this.authService.getCostShipping(this.todo).then((result) => {
			this.data = result;
			let cek_respon = this.data.result_status;
			if(cek_respon != '0'){
				// console.log(this.data.result_data);
				this.data_cost =  this.data.result_data;
				// console.log(this.data_cost);
			} 
			this.loading.dismiss();             
        }).catch((err) => {
			this.loading.dismiss();
            let toast = this.toastCtrl.create({
                message: err,
                duration: 2000,
                position: 'bottom'
            });
            toast.present();
        }) 
   }
   
   saveTransaksi(){
	    this.cart = this.authService.getCart();		
	
		 let params = { 
		  belanja: this.cart,
		  berat: this.total_berat,
		  total_bayar : this.total_belanja,
		  customer_id : this.customer,
		  cus_telepon : this.telepon,
		  cus_nama_penerima : this.nama_penerima,
		  cus_alamat : this.alamat,
		  cus_provinsi : this.provinsiSelected,
		  cus_stock_point : this.originSelected,
		  cus_city : this.citySelected,
		
		  cus_kec : this.kecSelected,
		  cus_kel : this.kelSelected,
		  cus_pos : this.pos,


		  cus_kurir : this.kurirSelected,
		  cus_paket : this.paketSelected,
		  cus_paymethod : this.paymentMethod,
		  agent_id  : localStorage.getItem('agent_id'),
		  tipe_user : this.tipe_user
		  //kartu_save : this.kartuku
		};
		
		//console.log(params);
		 
	    this.showLoader();
		this.authService.saveTransaksiUser(params).then((result) => {
			this.data = result;
			this.completePayment = this.data.result_data;
			this.tanggal_result  = this.data.tanggal_order;
			this.jam_result      = this.data.jam_order;
			this.tanggal_pay     = this.data.tanggal_pay;
			this.pay_methode     = this.data.pay_metode;
			this.card_metode     = this.data.card_metode;
			this.navCtrl.push(CompleteOrderPage, {'completePayment': this.completePayment, 'tanggal_order' : this.tanggal_result, 'jam_order' :  this.jam_result, 'tanggal_pay' : this.tanggal_pay, 'pay_methode' : this.pay_methode, 'ov_amt' : this.data.amt, 'ov_tixId' : this.data.txId, 'ov_mctoken' : this.data.mc_token, 'ov_time' : this.data.timeStamp, 'card_metode' : this.data.card_metode});
			this.loading.dismiss();             
        }).catch((err) => {
			this.loading.dismiss();
            let toast = this.toastCtrl.create({
                message: err,
                duration: 2000,
                position: 'bottom'
            });
            toast.present();
        })  
	   
   }
  
   presentAlert(alt) {
    let alert = this.alertCtrl.create({
      title: 'Alert',
      message: alt,
      buttons: [ {
          text: 'Ok',
          handler: () => {
            this.navCtrl.push(ProductPage);
          }
        }]
    });

    alert.present();
  }
  
  getCustomerDefault(){
	  //console.log(this.customerData());
  }
  
  
  getTotal() {
    return this.cart.reduce((i, j) => i + j.price * j.amount, 0);
  }
  
  getWeight() {
    return this.cart.reduce((i, j) => i + j.weight * j.amount, 0);
  }
  
  showLoader(){
		this.loading = this.loadingCtrl.create({
			content: 'Loading...'
		});

		this.loading.present();
  }

  presentToast(msg) {
		let toast = this.toastCtrl.create({
			  message: msg,
			  duration: 3000,
			  position: 'bottom',
			  dismissOnPageChange: true
		});

		toast.onDidDismiss(() => {
		  console.log('Dismissed toast');
		});

		toast.present();
   }
   
    hideOutlet(){
	  	/* if(pilihBayar ==''){
		  if(this.todo.providerUser == 'Sales'){
			  this.inputDisabled = true;
		  } 
		  else {
			  this.inputDisabled = false;
		  }
		}
		else 
		{
			if(pilihBayar == '2'){
				  this.inputDisabled = false;
			  } 
			  else {
				  this.inputDisabled = true;
			}
		} */
		if(this.todo.providerUser == 'Sales'){
		   this.inputDisabled = true;
	    } 
	    else {
		   this.inputDisabled = false;
	    }
    } 
	
	doRefresh(refresher) {
		//console.log('Begin async operation', refresher);
        this.navCtrl.setRoot(this.navCtrl.getActive().component);
		setTimeout(() => {
		  //console.log('Async operation has ended');
		  refresher.complete();
		}, 2000);
	  }
  
    goBack(){
		 this.navCtrl.push(ProductPage);
	}
    
	closeWin(){
		console.log('hello');
	}
}
