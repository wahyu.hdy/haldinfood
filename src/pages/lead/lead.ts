import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController, ModalController } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service/auth-service';
import { Geolocation } from '@ionic-native/geolocation';
import { AddLeadPage } from '../add-lead/add-lead';
import { DetailLeadPage } from '../detail-lead/detail-lead';
import { HomePage } from '../../pages/home/home';
import { ProfileUserPage } from '../profile-user/profile-user';
import { BantuanPage } from '../bantuan/bantuan';

/**
 * Generated class for the LeadPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-lead',
  templateUrl: 'lead.html',
})
export class LeadPage {
	
  public data_item: any;	
  public total_lead: string;
  
  todo:any = {};
  data:any = {};
  loading: any;	

    constructor(public navCtrl: NavController, public navParams: NavParams, public authService: AuthService, public loadingCtrl: LoadingController, private toastCtrl: ToastController, public modalCtrl: ModalController, public geolocation: Geolocation) {
		this.todo.userId   = localStorage.getItem('agent_id');
		this.dataLead();
		this.totalLeadData();
    }
	
	totalLeadData() {
		this.authService.getTotalLead().then((result) => {
			this.data = result;
			let cek_respon = this.data.result_status;
			if(cek_respon != '0'){
				this.total_lead =  this.data.result_data;
			} 
        }).catch((err) => {
            let toast = this.toastCtrl.create({
                message: err,
                duration: 2000,
                position: 'bottom'
            });
            toast.present();
        }) 
    }
  
    dataLead() {	
		this.showLoader();
		this.authService.getListLead(this.todo).then((result) => {
			this.data = result;
			let cek_respon = this.data.result_status;
			//console.log(cek_respon);
			if(cek_respon != '0'){
				//console.log(JSON.stringify(this.data));
				this.data_item = this.data.result_data;
			} 
			this.loading.dismiss();
            /* let toast = this.toastCtrl.create({
                message: this.data.message,
                duration: 3000,
                position: "bottom"
            });
            toast.present(); */
             
        }).catch((err) => {
			this.loading.dismiss();
            let toast = this.toastCtrl.create({
                message: err,
                duration: 2000,
                position: 'bottom'
            });
            toast.present();
        })
	}
	
	showLoader(){
		this.loading = this.loadingCtrl.create({
			content: 'Loading...'
		});

		this.loading.present();
	}

    presentToast(msg) {
		let toast = this.toastCtrl.create({
			  message: msg,
			  duration: 3000,
			  position: 'bottom',
			  dismissOnPageChange: true
		});

		toast.onDidDismiss(() => {
		  console.log('Dismissed toast');
		});

		toast.present();
    }
	
	detail_outlet(lead_id){
		const profileModal = this.modalCtrl.create(DetailLeadPage, { leadId: lead_id });
		profileModal.onDidDismiss(data => {
		  //console.log(data);
		   //this.madalDismissData = JSON.stringify(data);
		});
		profileModal.present();
	}
  
    lead_outlet(){
	  this.navCtrl.push(AddLeadPage);
    }
	
	goBack(){
		 this.navCtrl.push(HomePage);
	}
	
	myProfile(){
	  this.navCtrl.push(ProfileUserPage);
  }
  
  myHelp(){
	  this.navCtrl.push(BantuanPage);
  }

}
