import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service/auth-service';
import { InAppBrowser  } from '@ionic-native/in-app-browser/ngx';
import { HomePage } from '../../pages/home/home';

/**
 * Generated class for the PerluKirimPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-perlu-kirim',
  templateUrl: 'perlu-kirim.html',
})
export class PerluKirimPage {
	
  public data_item: any;	
  
  todo:any = {};
  data:any = {};
  loading: any;	
  

  constructor(public navCtrl: NavController, public navParams: NavParams, public authService: AuthService, public loadingCtrl: LoadingController, private toastCtrl: ToastController, private iab: InAppBrowser) {
	  this.todo.userId   = localStorage.getItem('agent_id');
	  this.todo.providerUser   = localStorage.getItem('provider_user');
	  this.todo.kode_sales      = localStorage.getItem('kode_sales');
	  this.dataSales();
  }
  
   prosesKirim(id){
	   this.showLoader();
		this.authService.saveKirimPaket(id).then((result) => {
			this.data = result;
			let cek_respon = this.data.result_status;
			//console.log(cek_respon);
			if(cek_respon != '0'){
				//console.log(JSON.stringify(this.data));
				//this.data_item = this.data.result_data;
				//window.location.reload();
				this.navCtrl.setRoot(this.navCtrl.getActive().component);
			} 
			this.loading.dismiss();
            /* let toast = this.toastCtrl.create({
                message: this.data.message,
                duration: 3000,
                position: "bottom"
            });
            toast.present(); */
             
        }).catch((err) => {
			this.loading.dismiss();
            let toast = this.toastCtrl.create({
                message: err,
                duration: 2000,
                position: 'bottom'
            });
            toast.present();
        })
   }

   dataSales() {	
		this.showLoader();
		this.authService.getListPerluKirim(this.todo).then((result) => {
			this.data = result;
			let cek_respon = this.data.result_status;
			//console.log(cek_respon);
			if(cek_respon != '0'){
				//console.log(JSON.stringify(this.data));
				this.data_item = this.data.result_data;
			} 
			this.loading.dismiss();
            /* let toast = this.toastCtrl.create({
                message: this.data.message,
                duration: 3000,
                position: "bottom"
            });
            toast.present(); */
             
        }).catch((err) => {
			this.loading.dismiss();
            let toast = this.toastCtrl.create({
                message: err,
                duration: 2000,
                position: 'bottom'
            });
            toast.present();
        })
	}
	
	 showLoader(){
		this.loading = this.loadingCtrl.create({
			content: 'Loading...'
		});

		this.loading.present();
	}

    presentToast(msg) {
		let toast = this.toastCtrl.create({
			  message: msg,
			  duration: 3000,
			  position: 'bottom',
			  dismissOnPageChange: true
		});

		toast.onDidDismiss(() => {
		  console.log('Dismissed toast');
		});

		toast.present();
    }
	
	openDocPage(id) {
	   //console.log(this.resiData.noresi);
	   //let url_data = 'https://apps.haldinfoods.com/api-haldin-agent/api/openDoc';
       /*  const options: InAppBrowserOptions = {
          zoom: 'no'
        } */

        // Opening a URL and returning an InAppBrowserObject
        //this.iab.create(url_data, this.options); 
		this.iab.create("http://apps.haldinfoods.com/api-haldin-agent/api/pdf_oc/"+id, '_system', 'location=yes');

       // Inject scripts, css and more with browser.X
    }
	
	goBack(){
	  this.navCtrl.push(HomePage);
    }
  
}
