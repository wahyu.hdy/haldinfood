import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController  } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service/auth-service';
import { PerformaPage } from '../../pages/performa/performa';

/**
 * Generated class for the DetailPerformaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-detail-performa',
  templateUrl: 'detail-performa.html',
})
export class DetailPerformaPage {
	
  public detail_item: any;	
  public total_performa_user: number;
   public nama_customer: string;	
  
  todo:any = {};
  data:any = {};
  loading: any;	

  constructor(public navCtrl: NavController, public navParams: NavParams, public authService: AuthService, public loadingCtrl: LoadingController, private toastCtrl: ToastController) {
	this.todo.detailId = navParams.get('detailId');
	this.dataDetailPerforma();
  }
  
  dataDetailPerforma() {
		this.showLoader();
		this.authService.getDetailPerforma(this.todo).then((result) => {
			this.data = result;
			let cek_respon = this.data.result_status;
			if(cek_respon != '0'){
				this.detail_item = this.data.result_data;
				this.nama_customer = this.data.result_nama;
				//console.log(this.detail_item);
			} 
			this.loading.dismiss();             
        }).catch((err) => {
			this.loading.dismiss();
            let toast = this.toastCtrl.create({
                message: err,
                duration: 2000,
                position: 'bottom'
            });
            toast.present();
        }) 
    }

  showLoader(){
		this.loading = this.loadingCtrl.create({
			content: 'Loading...'
		});

		this.loading.present();
	}

    presentToast(msg) {
		let toast = this.toastCtrl.create({
			  message: msg,
			  duration: 3000,
			  position: 'bottom',
			  dismissOnPageChange: true
		});

		toast.onDidDismiss(() => {
		  console.log('Dismissed toast');
		});

		toast.present();
    }
  
  
     goBack(){
		  this.navCtrl.push(PerformaPage);
	  }



}
