import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController} from 'ionic-angular';
import { AuthService } from '../../providers/auth-service/auth-service';
import {LoginPage} from '../authentication/login/login';

/**
 * Generated class for the LupaPasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-lupa-password',
  templateUrl: 'lupa-password.html',
})
export class LupaPasswordPage {
	
  loading: any;	
  loginData = { email:'' };
   data: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public authService: AuthService, public loadingCtrl: LoadingController, private toastCtrl: ToastController) {
  }

  forgotPassword(){
	this.showLoader();
    this.authService.forgotPassword(this.loginData).then((result) => {
      this.loading.dismiss();
      this.data = result;
	  let cek_respon = this.data.status_data;
	  
		  if(cek_respon == 1){
			  this.navCtrl.setRoot(LoginPage);
		  }
		  
		  let toast = this.toastCtrl.create({
			message: this.data.message,
			duration: 3000,
			position: "bottom"
		  });
		  toast.present();
    }, (err) => {
	  this.loading.dismiss();
	  let toast = this.toastCtrl.create({
		message: err,
		duration: 3000,
		position: "bottom"
	  });
	  toast.present();
    });
  }
  
   showLoader(){
    this.loading = this.loadingCtrl.create({
        content: 'Authenticating...'
    });

    this.loading.present();
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'top',
      dismissOnPageChange: true
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

}
