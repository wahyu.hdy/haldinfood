import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController, ViewController } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service/auth-service';
import { KomisiUserPage } from '../../pages/komisi-user/komisi-user';

/**
 * Generated class for the KomisiDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-komisi-detail',
  templateUrl: 'komisi-detail.html',
})
export class KomisiDetailPage {
	
 todo:any = {};
 loading: any;	
  data:any = {};
 public detail_komisi: any; 
 
  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, private toastCtrl: ToastController, public authService: AuthService, public viewCtrl : ViewController) {
	  this.todo.detailTgl = navParams.get('detailTgl');
	  this.todo.kodeAgent   = localStorage.getItem('kode_agent');
	  this.getDetailKomisi();
  }
  
  getDetailKomisi(){
	    let params = { 
		  tgl_set: this.todo.detailTgl,
		  kode_agent_set : this.todo.kodeAgent
		};
		
		this.showLoader();
		this.authService.getDetailKomisiAgent(params).then((result) => {
			this.data = result;
			let cek_respon = this.data.result_status;
			if(cek_respon != '0'){
				this.detail_komisi =  this.data.result_data;
			} 
			this.loading.dismiss();
        }).catch((err) => {
			this.loading.dismiss();
            let toast = this.toastCtrl.create({
                message: err,
                duration: 2000,
                position: 'bottom'
            });
            toast.present();
        })  
		
  }
  
  showLoader(){
		this.loading = this.loadingCtrl.create({
			content: 'Loading...'
		});

		this.loading.present();
	}

    presentToast(msg) {
		let toast = this.toastCtrl.create({
			  message: msg,
			  duration: 3000,
			  position: 'bottom',
			  dismissOnPageChange: true
		});

		toast.onDidDismiss(() => {
		  console.log('Dismissed toast');
		});

		toast.present();
    }
	
	 goBack(){
		  this.navCtrl.push(KomisiUserPage);
	  }

      public closeModal(){
		this.viewCtrl.dismiss();
	}
   

}
