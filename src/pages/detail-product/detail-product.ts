import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams, ViewController, LoadingController, ToastController } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service/auth-service';
import { CartModalPage } from '../cart-modal/cart-modal';
import { BehaviorSubject } from 'rxjs';

/**
 * Generated class for the DetailProductPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-detail-product',
  templateUrl: 'detail-product.html',
})
export class DetailProductPage {
	
  public detail_item: any; 
  public detail_image: any; 
  todo:any = {};
  data:any = {};
  loading: any;
  
  cart = [];
  products = [];
  cartItemCount: BehaviorSubject<number>;
  
  @ViewChild('cart', {read: ElementRef})fab: ElementRef;
	
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public authService: AuthService, public loadingCtrl: LoadingController, private toastCtrl: ToastController) {
	this.todo.userId   = localStorage.getItem('agent_id');
	this.todo.productId = navParams.get('productId');
	this.productDetail();
	//this.imageDetail();
  }

    ngOnInit() {
	  this.cart = this.authService.getCart();
	  this.cartItemCount = this.authService.getCartItemCount();
    }

  
    productDetail() {
		//this.detail_item = this.todo.surveyId;
		//console.log(this.todo.surveyId);
		this.showLoader();
		this.authService.getDetailProduct(this.todo).then((result) => {
			this.data = result;
			let cek_respon = this.data.result_status;
			if(cek_respon != '0'){
				this.detail_item = this.data.result_data;
				this.detail_image = this.data.result_images;
				//console.log(this.detail_item);
			} 
			this.loading.dismiss();             
        }).catch((err) => {
			this.loading.dismiss();
            let toast = this.toastCtrl.create({
                message: err,
                duration: 2000,
                position: 'bottom'
            });
            toast.present();
        }) 
   }
   
    addToCart(product) {
		this.authService.addProduct(product);
		this.animateCSS('tada');
	}
	 
	async openCart() {
		this.navCtrl.push(CartModalPage);
	}
	 
	animateCSS(animationName, keepAnimated = false) {
		const node = this.fab.nativeElement;
		node.classList.add('animated', animationName)
		
		//https://github.com/daneden/animate.css
		function handleAnimationEnd() {
		  if (!keepAnimated) {
			node.classList.remove('animated', animationName);
		  }
		  node.removeEventListener('animationend', handleAnimationEnd)
		}
		node.addEventListener('animationend', handleAnimationEnd)
	}
   
   showLoader(){
		this.loading = this.loadingCtrl.create({
			content: 'Loading...'
		});

		this.loading.present();
	}

    presentToast(msg) {
		let toast = this.toastCtrl.create({
			  message: msg,
			  duration: 3000,
			  position: 'bottom',
			  dismissOnPageChange: true
		});

		toast.onDidDismiss(() => {
		  console.log('Dismissed toast');
		});

		toast.present();
    }

 
  
   dismissModal() {
    //let data = { 'foo': 'bar' };
    this.viewCtrl.dismiss();
  }
	

}
