import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController, ModalController } from 'ionic-angular';
import { HomePage } from '../../pages/home/home';
import { AuthService } from '../../providers/auth-service/auth-service';
import { DetailApprovalPage } from '../../pages/detail-approval/detail-approval';

/**
 * Generated class for the ApprovalAgentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-approval-agent',
  templateUrl: 'approval-agent.html',
})
export class ApprovalAgentPage {
	
   loading: any;
   todo:any = {};
   data:any = {};
   
   public total_approval: string;   
   public data_item: any;  

    constructor(public navCtrl: NavController, public navParams: NavParams, public authService: AuthService, public loadingCtrl: LoadingController, private toastCtrl: ToastController, public modalCtrl: ModalController) {
	  this.todo.userId  = localStorage.getItem('agent_id');
	  this.dataApproval();
	  this.totalApprovalData();
    }
  
    dataApproval() {	
		this.showLoader();
		this.authService.getListApproval(this.todo).then((result) => {
			this.data = result;
			let cek_respon = this.data.result_status;
			//console.log(cek_respon);
			if(cek_respon != '0'){
				//console.log(JSON.stringify(this.data));
				this.data_item = this.data.result_data;
				
			} 
			this.loading.dismiss();
             
        }).catch((err) => {
			this.loading.dismiss();
            let toast = this.toastCtrl.create({
                message: err,
                duration: 2000,
                position: 'bottom'
            });
            toast.present();
        })
	}
	
	totalApprovalData() {
		this.authService.getTotalApproval(this.todo).then((result) => {
			this.data = result;
			let cek_respon = this.data.result_status;
			if(cek_respon != '0'){
				this.total_approval =  this.data.result_data;
			} 
			
        }).catch((err) => {
            let toast = this.toastCtrl.create({
                message: err,
                duration: 2000,
                position: 'bottom'
            });
            toast.present();
        }) 
    }
	
	detail_approval(id){
		this.navCtrl.push(DetailApprovalPage, {'approval_id': id})
	}

  
    goBack(){
		 this.navCtrl.push(HomePage);
	}
	
	showLoader(){
		this.loading = this.loadingCtrl.create({
			content: 'Loading...'
		});

		this.loading.present();
	}

    presentToast(msg) {
		let toast = this.toastCtrl.create({
			  message: msg,
			  duration: 3000,
			  position: 'bottom',
			  dismissOnPageChange: true
		});

		toast.onDidDismiss(() => {
		  console.log('Dismissed toast');
		});

		toast.present();
    }


}
