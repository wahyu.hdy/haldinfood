import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController, ModalController } from 'ionic-angular';
import { AuthService } from '../../../providers/auth-service/auth-service';
//import { SkModalPage } from '../../../pages/sk-modal/sk-modal';
import { Camera } from '@ionic-native/camera';
import { AktifasiPage } from '../../../pages/aktifasi/aktifasi';
//import { PolicyIndexPage } from '../../../pages/policies/policy-index/policy-index';
import { LoginPage } from '../login/login';
import { InAppBrowser  } from '@ionic-native/in-app-browser/ngx';

@Component({
  selector: 'page-register',
  templateUrl: 'register.html'
})
export class RegisterPage {
  loading: any;
  regData = { imageOri:'',imageOriNpwp:'', imageOriRek:'', gelar:'', nama:'', email:'', telepon:'', password:'', password2:'', nik:'', npwp:'', nama_bank:'', kantor_cabang:'', nomor_rekening:'', nama_pemilik:'', kode_sales:'', kode_agent:'', kode_pos:'', kode_tipe_user:'', alamat:'' };

  public img:String;
  public cameraSupported:boolean;
  public checkedbtn :boolean;
  public photos: any;
  public base64Image: string;
  public imageURL: any;
  public myImageKtp:string;
  public myImageNpwp:string;
  public myImageRek:string;
  public data_provinsi: any; 
  public data_city: any;  
  public data_kecamatan: any;  
  public data_kelurahan: any; 

  todo:any = {};
  data: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public authService: AuthService, public loadingCtrl: LoadingController, private toastCtrl: ToastController, public modalCtrl: ModalController, private camera: Camera, private iab: InAppBrowser) {
    this.myImageKtp = "assets/imgs/photo-camera.png"; 
    this.myImageNpwp = "assets/imgs/photo-camera.png";
	this.myImageRek = "assets/imgs/photo-camera.png";
    //this.regData.imageOri = '';
    //this.regData.imageOriNpwp = '';
	//this.kode_agent();
	this.provinceData();
	this.todo.kode_set_pack = navParams.get('kode_set');
	this.regData.kode_tipe_user = navParams.get('kode_set');
	this.checkedbtn = true;
  }
  
	kode_agent(kotaId){
		//var str = kotaId;
		//var splitted = str.split("-"); 
		this.todo.kotaid = kotaId;
		
        //console.log(this.todo);
		//this.regData.kode_pos = splitted[3];
		
	    //this.showLoader();
		this.authService.getKodeAgentGenerate(this.todo).then((result) => {
			this.data = result;
			let cek_respon = this.data.result_status;
			if(cek_respon != '0'){
				this.regData.kode_agent =  this.data.kode_agent_generate;
			} 
			
			this.kecamatanData(kotaId);
			//this.loading.dismiss();             
        }).catch((err) => {
			//this.loading.dismiss();
            let toast = this.toastCtrl.create({
                message: err,
                duration: 2000,
                position: 'bottom'
            });
            toast.present();
        }) 
   }
   
	provinceData() {
		this.showLoader();
		this.authService.getProvinceRegister().then((result) => {
			this.data = result;
			let cek_respon = this.data.result_status;
			if(cek_respon != '0'){
				this.data_provinsi =  this.data.result_data;
			} 
			this.loading.dismiss();
        }).catch((err) => {
			this.loading.dismiss();
            let toast = this.toastCtrl.create({
                message: err,
                duration: 2000,
                position: 'bottom'
            });
            toast.present();
        }) 
   }
   
   onGetCity(provinceId){
	//this.showLoader();
	this.authService.getCityRegister(provinceId).then((result) => {
		this.data = result;
		let cek_respon = this.data.result_status;
		if(cek_respon != '0'){
			this.data_city =  this.data.result_data;
			//this.data_kurir = null;
		} 
		//this.loading.dismiss();             
	}).catch((err) => {
		//this.loading.dismiss();
		let toast = this.toastCtrl.create({
			message: err,
			duration: 2000,
			position: 'bottom'
		});
		toast.present();
	}) 
}
   
   kecamatanData(kotaId){
	    //this.showLoader();
		this.authService.getKecamatanRegister(kotaId).then((result) => {
			this.data = result;
			let cek_respon = this.data.result_status;
			if(cek_respon != '0'){
				this.data_kecamatan =  this.data.result_data;
				//this.data_kurir = null;
			} 
			//this.loading.dismiss();             
        }).catch((err) => {
			//this.loading.dismiss();
            let toast = this.toastCtrl.create({
                message: err,
                duration: 2000,
                position: 'bottom'
            });
            toast.present();
        }) 
   }
   
   onGetKelurahan(kecamatanId){
	    //this.showLoader();
		this.authService.getKelurahanRegister(kecamatanId).then((result) => {
			this.data = result;
			let cek_respon = this.data.result_status;
			if(cek_respon != '0'){
				this.data_kelurahan =  this.data.result_data;
				//this.data_kurir = null;
			} 
			//this.loading.dismiss();             
        }).catch((err) => {
			//this.loading.dismiss();
            let toast = this.toastCtrl.create({
                message: err,
                duration: 2000,
                position: 'bottom'
            });
            toast.present();
        }) 
   }
 
    doSignup() {
		console.log(this.regData);
		this.showLoader();
		this.authService.register(this.regData).then((result) => {
		  this.data = result;
		  let cek_respon = this.data.data_status;
		  
		  if(cek_respon =='Sucess'){
			  this.navCtrl.setRoot(LoginPage);
		  }
		  
		  this.loading.dismiss();
		  let toast = this.toastCtrl.create({
			message: this.data.message,
			duration: 3000,
			position: "bottom"
		  });
		  toast.present();
		
		  //this.navCtrl.pop();
		}).catch((err) => {
			this.loading.dismiss();
			let toast = this.toastCtrl.create({
				message: err,
				duration: 2000,
				position: 'bottom'
			});
			toast.present();
		}) 
    }

	showLoader(){
		this.loading = this.loadingCtrl.create({
			content: 'Loading...'
		});

		this.loading.present();
	}

    presentToast(msg) {
		let toast = this.toastCtrl.create({
			  message: msg,
			  duration: 3000,
			  position: 'bottom',
			  dismissOnPageChange: true
		});

		toast.onDidDismiss(() => {
		  console.log('Dismissed toast');
		});

		toast.present();
    }

	termcondition() {
	   /*  const profileModal = this.modalCtrl.create(SkModalPage, { userId: 8675309 });
		profileModal.onDidDismiss(data => {
		  console.log(data);
		   this.madalDismissData = JSON.stringify(data);
		});
		profileModal.present(); */
		this.navCtrl.setRoot(AktifasiPage);
	}

    takePhotoKtp(){
		this.camera.getPicture({
			destinationType: this.camera.DestinationType.DATA_URL,
			targetWidth: 1000,
			targetHeight: 1000
		}).then((imageData) => {
			this.myImageKtp = "data:image/jpeg;base64," + imageData;
			this.regData.imageOri = imageData;
		}, (err) => {
			console.log(err);
		}); 
		
    }

    takePhotoNpwp(){
		this.camera.getPicture({
			destinationType: this.camera.DestinationType.DATA_URL,
			targetWidth: 1000,
			targetHeight: 1000
		}).then((imageDataNpwp) => {
			this.myImageNpwp = "data:image/jpeg;base64," + imageDataNpwp;
			this.regData.imageOriNpwp = imageDataNpwp;
		}, (err) => {
			console.log(err);
		}); 
		
    }
	
	takePhotoRek(){
		this.camera.getPicture({
			destinationType: this.camera.DestinationType.DATA_URL,
			targetWidth: 1000,
			targetHeight: 1000
		}).then((imageDataRek) => {
			this.myImageRek = "data:image/jpeg;base64," + imageDataRek;
			this.regData.imageOriRek = imageDataRek;
		}, (err) => {
			console.log(err);
		}); 
		
    }
	
	changeEvent(event) {
	   if (event.value) {
		  this.checkedbtn = false;
		  //this.navCtrl.push(PolicyIndexPage);
		  this.openDocPage();
		}else{
		  this.checkedbtn = true;
		}   
	  }
	  
	  
	  openDocPage() {
	   //console.log(this.resiData.noresi);
	   //let url_data = 'https://www.haldinfoods.com/api-haldin-agent/api/openDoc';
       /*  const options: InAppBrowserOptions = {
          zoom: 'no'
        } */

        // Opening a URL and returning an InAppBrowserObject
        //this.iab.create(url_data, this.options); 
		this.iab.create("http://apps.haldinfoods.com/api-haldin-agent/doc/PersetujuanMitraHSP.pdf", '_system', 'location=yes');

       // Inject scripts, css and more with browser.X
    }

}
