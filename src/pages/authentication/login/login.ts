import { Component } from '@angular/core';
import { NavController, LoadingController, ToastController } from 'ionic-angular';
import { AuthService } from '../../../providers/auth-service/auth-service';
//import { RegisterPage } from '../../register/register';
import { HomePage } from '../../home/home';
import { RegisterIndexPage } from '../../register-index/register-index';
import { LupaPasswordPage } from '../../lupa-password/lupa-password';
import { AktifasiOutletPage } from '../../aktifasi-outlet/aktifasi-outlet';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {

  loading: any;
  loginData = { username:'', password:'' };
  data: any;
  
  public showPassword: boolean = false;

   public rootPage;
  constructor(public navCtrl: NavController, public authService: AuthService, public loadingCtrl: LoadingController, private toastCtrl: ToastController) {
	  let statusLogin = localStorage.getItem("isLogin");
		if(statusLogin =='true') { 
			 this.navCtrl.setRoot(HomePage);
        }
		else {
			this.rootPage = LoginPage;
		} 
  }
  
  onPasswordToggle(): void {
    this.showPassword = !this.showPassword;
  }

  doLogin() {
    this.showLoader();
    this.authService.login(this.loginData).then((result) => {
      this.loading.dismiss();
      this.data = result;
	  let cek_respon = this.data.status_data;
	  
		  if(cek_respon == 1){
			  localStorage.setItem("isLogin", "true");
			  localStorage.setItem('token', this.data.access_token);
			  localStorage.setItem('agent_id', this.data.agent_id);
			  localStorage.setItem('title_user', this.data.title_user);
			  localStorage.setItem('email_user', this.data.email_user);
			  localStorage.setItem('nama_user', this.data.nama_user);
			  localStorage.setItem('telpon_user', this.data.telpon_user);
			  localStorage.setItem('provider_user', this.data.provider);
			  localStorage.setItem('kode_sales', this.data.kode_sales);
			  localStorage.setItem('kode_agent', this.data.kode_agent);
			  localStorage.setItem('price_type_user', this.data.price_type_user);
			  this.navCtrl.setRoot(HomePage);
		  }
		  else{
			  this.navCtrl.setRoot(LoginPage);
		  }
		  
		  
		  let toast = this.toastCtrl.create({
			message: this.data.message,
			duration: 3000,
			position: "bottom"
		  });
		  toast.present();
    }, (err) => {
	  this.loading.dismiss();
	  let toast = this.toastCtrl.create({
		message: err,
		duration: 3000,
		position: "bottom"
	  });
	  toast.present();
    });
  } 

  register() {
    this.navCtrl.push(RegisterIndexPage);
  }
  
  lupa_password() {
    this.navCtrl.push(LupaPasswordPage);
  }
  
  public logOut() {
	localStorage.clear();
  }
  
  public aktifasiAfterRegister() {
	this.navCtrl.push(AktifasiOutletPage);
  }

  showLoader(){
    this.loading = this.loadingCtrl.create({
        content: 'Authenticating...'
    });

    this.loading.present();
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'top',
      dismissOnPageChange: true
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

}
