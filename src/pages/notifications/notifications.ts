import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController, ModalController } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service/auth-service';

/**
 * Generated class for the NotificationsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-notifications',
  templateUrl: 'notifications.html',
})
export class NotificationsPage {
	
  public data_item: any;  
  
  todo:any = {};
  data:any = {};
  loading: any;
	
  products = [];
  
  public event: any;
  public events: any[] = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public authService: AuthService, public loadingCtrl: LoadingController, private toastCtrl: ToastController, public modalCtrl: ModalController) {
	  this.todo.userId = localStorage.getItem('agent_id');
	  this.todo.price_type_user = localStorage.getItem('price_type_user');
	   this.event = "today";
  }

   
   
  
   showLoader(){
		this.loading = this.loadingCtrl.create({
			content: 'Loading...'
		});

		this.loading.present();
	}

    presentToast(msg) {
		let toast = this.toastCtrl.create({
			  message: msg,
			  duration: 3000,
			  position: 'bottom',
			  dismissOnPageChange: true
		});

		toast.onDidDismiss(() => {
		  console.log('Dismissed toast');
		});

		toast.present();
    }

}
