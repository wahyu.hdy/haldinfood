import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {LoginPage} from '../authentication/login/login';
import {RegisterPage} from '../authentication/register/register';
import {AddSurveyPage } from '../add-survey/add-survey';

/**
 * Generated class for the RegisterIndexPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-register-index',
  templateUrl: 'register-index.html',
})
export class RegisterIndexPage {

  page1: any = LoginPage;
  page2: any = RegisterPage;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
	 
  }

  register_agent(kode_set) {
	this.navCtrl.push(RegisterPage, {'kode_set': kode_set})
  }
  
  register_customer() {
    this.navCtrl.push(AddSurveyPage);
  }

}
