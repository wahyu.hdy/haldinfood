import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { SuperTabsModule } from 'ionic2-super-tabs';
import { HttpModule } from '@angular/http';
import { Camera } from '@ionic-native/camera';
import { Geolocation } from '@ionic-native/geolocation';
import { Calendar } from '@ionic-native/calendar';
import { DatePickerModule } from 'ionic-calendar-date-picker';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
//import { File } from '@ionic-native/file/ngx';
import { FCM } from '@ionic-native/fcm/ngx';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { HighlightPage } from '../pages/home/child/highlight/highlight';
import { TaobaoCollectionPage } from '../pages/home/child/taobao-collection/taobao-collection';
import { ElectronicPage } from '../pages/home/child/electronic/electronic';
import {FashionPage} from '../pages/home/child/fashion/fashion';
import {HomeLivingPage} from '../pages/home/child/home-living/home-living';
import {PetShopPage} from '../pages/home/child/pet-shop/pet-shop';
import {HealthBeautyPage} from '../pages/home/child/health-beauty/health-beauty';
import {BabyToysPage} from '../pages/home/child/baby-toys/baby-toys';
import {SportTravelPage} from '../pages/home/child/sport-travel/sport-travel';
import {MotorsPage} from '../pages/home/child/motors/motors';
import {ShopByBrandsPage} from '../pages/home/child/shop-by-brands/shop-by-brands';
import { TabsPage } from '../pages/navigator/tabs/tabs';
import { MenuPage } from '../pages/navigator/menu/menu';
import { PopoverPage } from '../pages/components/popover/popover';
import { AuthService } from '../providers/auth-service/auth-service';
import { AuthIndexPage } from '../pages/authentication/auth-index/auth-index';
import { LoginPage } from '../pages/authentication/login/login';
import { RegisterPage } from '../pages/authentication/register/register';
import { NotificationsPage } from '../pages/notifications/notifications';
import { WishlistPage } from '../pages/wishlist/wishlist';
import { OrdersPage } from '../pages/orders/orders';
import { OcSalesPage } from '../pages/oc-sales/oc-sales';
import { BelanjaOcPage } from '../pages/belanja-oc/belanja-oc';
import { SettingsPage } from '../pages/settings/settings';
import { PolicyIndexPage } from '../pages/policies/policy-index/policy-index';
import { PrivacyPolicyPage } from '../pages/policies/privacy-policy/privacy-policy';
import { TermsConditionsPage } from '../pages/policies/terms-conditions/terms-conditions';
//import { SkModalPageModule } from '../pages/sk-modal/sk-modal';
import { ProvidersCartProvider } from '../providers/providers-cart/providers-cart';
import { CartModalPage } from '../pages/cart-modal/cart-modal';
import { ProductPage } from '../pages/product/product';
import { PricePipe } from '../pipes/price';
import { SurveyPage } from '../pages/survey/survey';
import { TopupPage } from '../pages/topup/topup';
import { AddSurveyPage } from '../pages/add-survey/add-survey';
import { AktifasiPage } from '../pages/aktifasi/aktifasi';
import { AktifasiOutletPage } from '../pages/aktifasi-outlet/aktifasi-outlet';
import { DetailOutletPage } from '../pages/detail-outlet/detail-outlet';
import { SearchPipe } from '../pipes/search/search';
import { DetailProductPage } from '../pages/detail-product/detail-product';
import { CompleteOrderPage } from '../pages/complete-order/complete-order';
import { DaftarKunjunganPage } from '../pages/daftar-kunjungan/daftar-kunjungan';
import { RegisterIndexPage } from '../pages/register-index/register-index';
import { LupaPasswordPage } from '../pages/lupa-password/lupa-password';
import { LeadPage } from '../pages/lead/lead';
import { AddLeadPage } from '../pages/add-lead/add-lead';
import { DetailLeadPage } from '../pages/detail-lead/detail-lead';
import { AgendaPage } from '../pages/agenda/agenda';
import { AddAgendaPage } from '../pages/add-agenda/add-agenda';
import { DsTransaksiPage } from '../pages/ds-transaksi/ds-transaksi';
import { HelpPage } from '../pages/help/help';
import { DsDetailTransaksiPage } from '../pages/ds-detail-transaksi/ds-detail-transaksi';
import { StTransaksiPage } from '../pages/st-transaksi/st-transaksi';
import { StDetailTransaksiPage } from '../pages/st-detail-transaksi/st-detail-transaksi';
import { ProfileUserPage } from '../pages/profile-user/profile-user';
import { BantuanPage } from '../pages/bantuan/bantuan';
import { ApprovalAgentPage } from '../pages/approval-agent/approval-agent';
import { DetailApprovalPage } from '../pages/detail-approval/detail-approval';
import { AgendaDetailPage } from '../pages/agenda-detail/agenda-detail';
import { UbahPasswordPage } from '../pages/ubah-password/ubah-password';
import { PerformaPage } from '../pages/performa/performa';
import { DetailPerformaPage } from '../pages/detail-performa/detail-performa';
import { SalesCustomerPage } from '../pages/sales-customer/sales-customer';
import { SalesAktifasiUserPage } from '../pages/sales-aktifasi-user/sales-aktifasi-user';
import { KomisiUserPage } from '../pages/komisi-user/komisi-user';
import { KomisiDetailPage } from '../pages/komisi-detail/komisi-detail';
import { PerluKirimPage } from '../pages/perlu-kirim/perlu-kirim';
import { SampleProductPage } from '../pages/sample-product/sample-product';
import { CartSamplePage } from '../pages/cart-sample/cart-sample';
import { OcSamplePage } from '../pages/oc-sample/oc-sample';
import { CompleteSamplePage } from '../pages/complete-sample/complete-sample';
import { Firebase } from '@ionic-native/firebase';
import { LocalNotifications} from '@ionic-native/local-notifications/ngx'

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    HighlightPage,
    TaobaoCollectionPage,
    ElectronicPage,
    FashionPage,
    HomeLivingPage,
    PetShopPage,
    HealthBeautyPage,
    BabyToysPage,
    SportTravelPage,
    MotorsPage,
    ShopByBrandsPage,
    TabsPage,
    MenuPage,
    PopoverPage,
    AuthIndexPage,
    LoginPage,
    RegisterPage,
    NotificationsPage,
    WishlistPage,
    OrdersPage,
	OcSalesPage,
	BelanjaOcPage,
    SettingsPage,
    PolicyIndexPage,
    PrivacyPolicyPage,
    TermsConditionsPage,
    ProductPage,
	PricePipe,
	SurveyPage,
	TopupPage,
	AddSurveyPage,
	AktifasiPage,
	AktifasiOutletPage,
	DetailOutletPage,
	SearchPipe,
	DetailProductPage,
	CompleteOrderPage,
	DaftarKunjunganPage,
	RegisterIndexPage,
	LeadPage,
	AddLeadPage,
	DetailLeadPage,
	AgendaPage,
	AddAgendaPage,
	HelpPage,
	DsTransaksiPage,
	DsDetailTransaksiPage,
	StTransaksiPage,
	StDetailTransaksiPage,
	ProfileUserPage,
	BantuanPage,
	LupaPasswordPage,
	ApprovalAgentPage,
	DetailApprovalPage,
	AgendaDetailPage,
	UbahPasswordPage,
	PerformaPage,
	DetailPerformaPage,
	SalesCustomerPage,
	SalesAktifasiUserPage,
	KomisiUserPage,
	KomisiDetailPage,
	PerluKirimPage,
	SampleProductPage,
	CartModalPage,
	CartSamplePage,
	OcSamplePage,
	CompleteSamplePage,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    SuperTabsModule.forRoot(),
    HttpModule,
    //SkModalPageModule,
    //CartModalPageModule,
	DatePickerModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    HighlightPage,
    TaobaoCollectionPage,
    ElectronicPage,
    FashionPage,
    HomeLivingPage,
    PetShopPage,
    HealthBeautyPage,
    BabyToysPage,
    SportTravelPage,
    MotorsPage,
    ShopByBrandsPage,
    TabsPage,
    MenuPage,
    PopoverPage,
    AuthIndexPage,
    LoginPage,
    RegisterPage,
    NotificationsPage,
    WishlistPage,
    OrdersPage,
	OcSalesPage,
	BelanjaOcPage,
    SettingsPage,
    PolicyIndexPage,
    PrivacyPolicyPage,
    TermsConditionsPage,
	ProductPage,
	SurveyPage,
	TopupPage,
	AddSurveyPage,
	AktifasiPage,
	AktifasiOutletPage,
	DetailOutletPage,
	DetailProductPage,
	CompleteOrderPage,
	DaftarKunjunganPage,
	RegisterIndexPage,
	LeadPage,
	AddLeadPage,
	DetailLeadPage,
	AgendaPage,
	AddAgendaPage,
	DsTransaksiPage,
	DsDetailTransaksiPage,
	StTransaksiPage,
	StDetailTransaksiPage,
	ProfileUserPage,
	BantuanPage,
	LupaPasswordPage,
	ApprovalAgentPage,
	DetailApprovalPage,
	AgendaDetailPage,
	UbahPasswordPage,
	PerformaPage,
	DetailPerformaPage,
	SalesCustomerPage,
	SalesAktifasiUserPage,
	KomisiUserPage,
	KomisiDetailPage,
	PerluKirimPage,
	SampleProductPage,
	CartModalPage,
	CartSamplePage,
	OcSamplePage,
	CompleteSamplePage,
  ],
  providers: [
    Calendar,
    StatusBar,
	Geolocation,
    SplashScreen,
    Camera,
	Firebase,
	FCM,
	InAppBrowser,
    //File,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
	AuthService,
	LocalNotifications,
    ProvidersCartProvider  
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {}
