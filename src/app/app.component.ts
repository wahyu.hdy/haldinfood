import { Component, ViewChild } from '@angular/core';
import { Platform, NavController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

//import { HomePage } from '../pages/home/home';
//import { AgendaPage } from '../pages/agenda/agenda';
//import { LeadPage } from '../pages/lead/lead';
import { LoginPage } from '../pages/authentication/login/login';
import { Firebase } from '@ionic-native/firebase';
import { AuthService } from '../providers/auth-service/auth-service';
//import { FCM } from '@ionic-native/fcm/ngx';
//import { LocalNotifications } from '@ionic-native/local-notifications/ngx';

//import { isCordovaAvailable } from '../common/is-cordova-available';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild('myNav') navCtrl: NavController;
  rootPage:any = LoginPage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, firebase: Firebase, public authService: AuthService) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
	  
	  //fcm.getToken().then(token => 
	  //  console.log(token)
	  //).catch(err=> console.log(err));
     
    //var $this = this;
    
    /*this.fcm.onNotification().subscribe(data => {
      if (data.wasTapped) {
        this.navCtrl.push(AgendaPage);
      } else {
        this.navCtrl.push(AgendaPage);
      }
    });*/
    
	});
	
  }
 
 
	
}

