export class Product {
  constructor(
    public name: string,
    public price: number,
    public icon?: string,
    public img?: string,
    private _qtt: number = 0,
  ) {}

  set qtt(val: any) { this._qtt = Math.max(0, parseInt(val) || 0) }
  get qtt() { return this._qtt }
}